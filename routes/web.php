<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('passport.index');
});

//Test Route
Route::get('/dashboard', function () {
    return view('dashboard.index');
});

Route::group([
    'prefix' => 'passport',
    'as' => 'Passport::'
],function(){
    Route::get('/api',[
        'as' => 'API',
        'uses' => 'PassportCtrl@api'
    ]);
    Route::get('search',[
        'as' => 'Search',
        'uses' => 'PassportCtrl@search'
    ]);
    Route::get('add',[
        'as' => 'Add',
        'uses' => 'PassportCtrl@add'
    ]);
    Route::get('edit',[
        'as' => 'Edit',
        'uses' => 'PassportCtrl@edit'
    ]);
    Route::post('save',[
        'as' => 'Save',
        'uses' => 'PassportCtrl@save'
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/schedule', 'HomeController@schedule');


Route::group(['prefix'=>'administrator', 'middleware'=>['auth']], function () {
	// Route::resource('home', 'HomeController@index');
    //file_management
    Route::group([
        'middleware' => ['role:admin|arsip|pegawai'],
        'prefix' => 'filemanagement',
        'as' => 'FileManagement::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'FilemanagementController@index'
        ]);
        Route::group([
            'prefix' => 'file',
            'as' => 'File::'
        ],function(){
            Route::get('/api',[
                'as' => 'API',
                'uses' => 'FilemanagementController@api_file'
            ]);
            Route::get('upload',[
                'as' => 'Upload',
                'uses' => 'FilemanagementController@upload_page'
            ]);
            Route::post('upload',[
                'as' => 'Upload',
                'uses' => 'FilemanagementController@upload_file'
            ]);
            Route::get('download/{file}',[
                'as' => 'Download',
                'uses' => 'FilemanagementController@download_file'
            ]);
            Route::post('/delete', [
                'as' => 'Delete',
                'uses' => 'FilemanagementController@delete_file'
            ]);
        });
        Route::group([
            'prefix' => 'folder',
            'as' => 'Folder::'
        ],function(){
            Route::get('add',[
                'as' => 'Add',
                'uses' => 'FilemanagementController@add_folder'
            ]);
            Route::post('save',[
                'as' => 'Save',
                'uses' => 'FilemanagementController@save_folder'
            ]);
            Route::get('edit',[
                'as' => 'Edit',
                'uses' => 'FilemanagementController@edit_folder'
            ]);
            Route::get('/delete_page', [
                'as' => 'DeletePage',
                'uses' => 'FilemanagementController@delete_folder_page'
            ]);
            Route::post('/delete', [
                'as' => 'Delete',
                'uses' => 'FilemanagementController@delete_folder'
            ]);
        });
    });

    Route::group([
        'middleware' => ['role:admin|arsip|pegawai'],
        'prefix' => 'schedules',
        'as' => 'Schedule::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'SchedulesController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'SchedulesController@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'SchedulesController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'SchedulesController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'SchedulesController@destroy'
        ]);
        Route::get('report/{id?}',[
            'as' => 'Report',
            'uses' => 'SchedulesController@report'
        ]);
    });

    Route::group([
        'middleware' => ['role:admin|arsip|pegawai'],
        'prefix' => 'disposition',
        'as' => 'Disposition::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'DispositionCtrl@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'DispositionCtrl@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'DispositionCtrl@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'DispositionCtrl@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'DispositionCtrl@destroy'
        ]);
        Route::get('report/{id?}',[
            'as' => 'Report',
            'uses' => 'DispositionCtrl@report'
        ]);
    });

    //OfficialtaskController
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'official-task',
        'as' => 'OfficialTask::'
    ],function(){
        Route::any('api',[
            'as' => 'API',
            'uses' => 'OfficialtaskController@api'
        ]);
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'OfficialtaskController@index'
        ]);
        Route::get('/normatif',[
            'as' => 'IndexNormatif',
            'uses' => 'OfficialtaskController@index_nofmatif'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'OfficialtaskController@add'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'OfficialtaskController@edit'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'OfficialtaskController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'OfficialtaskController@destroy'
        ]);
        Route::get('report/{id?}',[
            'as' => 'Report',
            'uses' => 'OfficialtaskController@report'
        ]);
    });

    //SettingOfficial
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'setting-official-task',
        'as' => 'SettingOfficial::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'SettingperjalanandinasController@index'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'SettingperjalanandinasController@save'
        ]);

        Route::get('add-place',[
            'as' => 'AddPlace',
            'uses' => 'SettingperjalanandinasController@add_place'
        ]);
        Route::get('edit-place',[
            'as' => 'EditPlace',
            'uses' => 'SettingperjalanandinasController@edit_place'
        ]);
        Route::post('save-save',[
            'as' => 'SavePlace',
            'uses' => 'SettingperjalanandinasController@save_place'
        ]);
        Route::post('/delete', [
            'as' => 'DeletePlace',
            'uses' => 'SettingperjalanandinasController@destroy_place'
        ]);
    });

//	Route::resource('rapatluarkantor', 'RapatluarkantorController');
//	Route::resource('uangharian', 'UangharianController');
//	Route::resource('uangrapatluarkantor', 'UangrapatluarkantorController');

    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'tiket-pesawat',
        'as' => 'TiketPesawat::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'TikepesawatController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'TikepesawatController@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'TikepesawatController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'TikepesawatController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'TikepesawatController@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'penginapan',
        'as' => 'Penginapan::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'PenginapanController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'PenginapanController@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'PenginapanController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'PenginapanController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'PenginapanController@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'taxi',
        'as' => 'Taxi::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'TaxiController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'TaxiController@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'TaxiController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'TaxiController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'TaxiController@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'sewa-kendaraan',
        'as' => 'VehicleRent::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'SewakendaraanController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'SewakendaraanController@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'SewakendaraanController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'SewakendaraanController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'SewakendaraanController@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'insentif-meeting',
        'as' => 'InsentifMeeting::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'InsentifMeetingCtrl@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'InsentifMeetingCtrl@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'InsentifMeetingCtrl@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'InsentifMeetingCtrl@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'InsentifMeetingCtrl@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'kode-akun-kategori',
        'as' => 'KodeAkunKategori::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'KodeAkunKategoriCtrl@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'KodeAkunKategoriCtrl@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'KodeAkunKategoriCtrl@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'KodeAkunKategoriCtrl@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'KodeAkunKategoriCtrl@destroy'
        ]);
    });
    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'kode-akun',
        'as' => 'KodeAkun::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'KodeAkunCtrl@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'KodeAkunCtrl@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'KodeAkunCtrl@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'KodeAkunCtrl@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'KodeAkunCtrl@destroy'
        ]);
    });

    Route::group([
        'middleware' => ['role:admin|keuangan'],
        'prefix' => 'biaya-perjalanan-dinas',
        'as' => 'BiayaPerjalananDinas::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'BiayaPerjalananDinasCtrl@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'BiayaPerjalananDinasCtrl@add_page'
        ]);
        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'BiayaPerjalananDinasCtrl@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'BiayaPerjalananDinasCtrl@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'BiayaPerjalananDinasCtrl@destroy'
        ]);
    });
    Route::group([
        'prefix' => 'pegawai',
        'as' => 'Pegawai::'
    ],function(){
        Route::get('/',[
            'as' => 'Index',
            'uses' => 'PegawaiController@index'
        ]);
        Route::get('add',[
            'as' => 'Add',
            'uses' => 'PegawaiController@add_page'
        ]);

        Route::get('edit',[
            'as' => 'Edit',
            'uses' => 'PegawaiController@edit_page'
        ]);
        Route::post('save',[
            'as' => 'Save',
            'uses' => 'PegawaiController@save'
        ]);
        Route::post('/delete', [
            'as' => 'Delete',
            'uses' => 'PegawaiController@destroy'
        ]);
        Route::any('api',[
            'as' => 'API',
            'uses' => 'PegawaiController@api'
        ]);

        //jabatan
        Route::group([
            'prefix' => 'jabatan',
            'as' => 'Jabatan::'
        ],function(){
            Route::get('add',[
                'as' => 'Add',
                'uses' => 'PegawaiController@add_jabatan_page'
            ]);
            Route::get('edit',[
                'as' => 'Edit',
                'uses' => 'PegawaiController@edit_jabatan_page'
            ]);
            Route::post('save',[
                'as' => 'Save',
                'uses' => 'PegawaiController@save_jabatan'
            ]);
            Route::post('/delete', [
                'as' => 'Delete',
                'uses' => 'PegawaiController@destroy_jabatan'
            ]);
        });

        //golongan
        Route::group([
            'prefix' => 'golongan',
            'as' => 'Golongan::'
        ],function(){
            Route::get('add',[
                'as' => 'Add',
                'uses' => 'PegawaiController@add_golongan_page'
            ]);
            Route::get('edit',[
                'as' => 'Edit',
                'uses' => 'PegawaiController@edit_golongan_page'
            ]);
            Route::post('save',[
                'as' => 'Save',
                'uses' => 'PegawaiController@save_golongan'
            ]);
            Route::post('/delete', [
                'as' => 'Delete',
                'uses' => 'PegawaiController@destroy_golongan'
            ]);
        });
    });
});