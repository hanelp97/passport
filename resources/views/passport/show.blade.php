@extends('layouts.nosidebar-app')

@section('content')
    <passport-show></passport-show>
    <template id="passportShow">
        <div class="container-page">
            <div class="">
                <div class="col-lg-12">
                    <h3>Data Passport <u>{{request()->get('no_passport')}}</u></h3>
                    <p>
                        Dibawah ini adalah detail passport anda
                    </p>
                </div>
                <br>
            </div> <!-- /row -->
        </div> <!-- /container full -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail Passport</div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nomor Paspor</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->no_paspor}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">NIK</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->nik}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nama Lengkap</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->nama_lengkap}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tempat Lahir</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->tempat_lahir}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        @if($data_passport->tanggal_lahir)
                                            {{\Carbon\Carbon::parse($data_passport->tanggal_lahir)->format('d')}}
                                            {{\App\Helpers\main_helper::indonesiaDate(\Carbon\Carbon::parse($data_passport->tanggal_lahir)->format('n'),'month')}}
                                            {{\Carbon\Carbon::parse($data_passport->tanggal_lahir)->format('Y')}}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jenis Kelamin</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{strtoupper($data_passport->jenis_kelamin) == 'L' ? 'Laki-laki' : 'Perempuan'}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Status Pernikahan</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->status_pernikahan == 'S' ? 'Sudah Menikah' : 'Belum Menikah'}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat Domisili (Bahasa Inggris)</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->alamat_domisili_bahasa_inggris}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat Domisili (Bahasa Cina)</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->alamat_domisili_bahasa_china}}
                                    </p>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-3 control-label">Jenis Disabilitas</label>--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--<p class="form-control-static">--}}
                                        {{--{{$data_passport->jenis_disabilitas}}--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Metode Pemungutan Suara</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->metode_pemungutan_suara}}
                                    </p>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-3 control-label">Metode Pemungutan Suara</label>--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--<p class="form-control-static">--}}
                                        {{--{{$data_passport->metode_pemungutan_suara}}--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="{{url('passport/edit?no_passport='.request()->no_passport)}}" class="btn btn-primary"><i class="fa fa-pencil"></i> &nbsp; Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
@endsection

@section('scripts')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
        .modal-dialog{
            width: 800px;
        }
    </style>
    <script>
        var passportShow = Vue.extend({
            template:'#passportShow',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Schedule::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            },
            ready:function () {
                $('#d1').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $('#d2').datetimepicker({
                    useCurrent: false,
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $("#d1").on("dp.change", function (e) {
                    $('#d2').data("DateTimePicker").minDate(e.date);
                });
                $("#d2").on("dp.change", function (e) {
                    $('#d1').data("DateTimePicker").maxDate(e.date);
                });

                $('#monthPicker').datetimepicker({
                    useCurrent: false,
                    format: 'M',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
            }
        });
        Vue.component('passport-show',passportShow);
    </script>
    <!-- DATETIMEPICKER-->

@endsection