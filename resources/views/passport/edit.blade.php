@extends('layouts.nosidebar-app')

@section('content')
    <passport-edit></passport-edit>
    <template id="passportEdit">
        <div class="container-page">
            <div class="">
                <div class="col-lg-12">
                    <h3>Edit Passport <u>{{request()->get('no_passport')}}</u></h3>
                    <p>
                        Isilah data yang valid
                    </p>
                </div>
                <br>
            </div> <!-- /row -->
        </div> <!-- /container full -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Passport</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="{{url('passport/save')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="no_passport" value="{{$data_passport->no_paspor}}">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nomor Paspor</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        {{$data_passport->no_paspor}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">NIK *</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm" name="nik" value="{{@$data_passport->nik}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nama Lengkap *</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control input-sm" value="{{@$data_passport->nama_lengkap}}" name="nama_lengkap" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tempat Lahir *</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm" value="{{@$data_passport->tempat_lahir}}" name="tempat_lahir" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tanggal Lahir *</label>
                                <div class="col-md-3">
                                    <div id="d2" class="input-group date">
                                        <input type="text" class="form-control input-sm" name="tanggal_lahir" value="{{\Carbon\Carbon::parse($data_passport->tanggal_lahir)->format('d-m-Y')}}" placeholder="Sampai">
                                        <span class="input-group-addon">
                  <span class="fa fa-calendar"></span>
               </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jenis Kelamin *</label>
                                <div class="col-sm-4">
                                    <select name="jenis_kelamin" class="form-control input-sm" id="" required>
                                        <option value="L" {{strtoupper($data_passport->jenis_kelamin) == 'L' ? 'selected' : ''}}>Laki-laki</option>
                                        <option value="P" {{strtoupper($data_passport->jenis_kelamin) == 'P' ? 'selected' : ''}}>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Status Pernikahan *</label>
                                <div class="col-sm-4">
                                    <select name="status_pernikahan" class="form-control input-sm" id="" required>
                                        <option value="B" {{strtoupper($data_passport->status_pernikahan) == 'B' ? 'selected' : ''}}>Belum Menikah</option>
                                        <option value="S" {{strtoupper($data_passport->status_pernikahan) == 'S' ? 'selected' : ''}}>Sudah Menikah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat Domisili (Bahasa Inggris)</label>
                                <div class="col-sm-5">
                                    <textarea name="alamat_domisili_bahasa_inggris" class="form-control input-sm" id="" cols="30" rows="10">{{@$data_passport->alamat_domisili_bahasa_inggris}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat Domisili (Bahasa Cina)</label>
                                <div class="col-sm-5">
                                    <textarea name="alamat_domisili_bahasa_china" class="form-control input-sm" id="" cols="30" rows="10">{{@$data_passport->alamat_domisili_bahasa_china}}</textarea>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-3 control-label">Jenis Disabilitas</label>--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--<p class="form-control-static">--}}
                                        {{--{{$data_passport->jenis_disabilitas}}--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Metode Pemungutan Suara *</label>
                                <div class="col-sm-4">
                                    <select name="metode_pemungutan_suara" class="form-control input-sm" id="" required>
                                        <option value="POS" {{strtoupper($data_passport->metode_pemungutan_suara) == 'POS' ? 'selected' : ''}}>POS</option>
                                        <option value="TPSLN" {{strtoupper($data_passport->metode_pemungutan_suara) == 'TPSLN' ? 'selected' : ''}}>TPSLN</option>
                                    </select>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="" class="col-sm-3 control-label">Metode Pemungutan Suara</label>--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--<p class="form-control-static">--}}
                                        {{--{{$data_passport->metode_pemungutan_suara}}--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </template>
@endsection

@section('scripts')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
        .modal-dialog{
            width: 800px;
        }
    </style>
    <script>
        var passportEdit = Vue.extend({
            template:'#passportEdit',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Schedule::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            },
            ready:function () {
                $('#d1').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $('#d2').datetimepicker({
                    useCurrent: false,
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $("#d1").on("dp.change", function (e) {
                    $('#d2').data("DateTimePicker").minDate(e.date);
                });
                $("#d2").on("dp.change", function (e) {
                    $('#d1').data("DateTimePicker").maxDate(e.date);
                });

                $('#monthPicker').datetimepicker({
                    useCurrent: false,
                    format: 'M',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
            }
        });
        Vue.component('passport-edit',passportEdit);
    </script>
    <!-- DATETIMEPICKER-->

@endsection