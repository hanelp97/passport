@extends('layouts.nosidebar-app')

@section('content')
    <index-search></index-search>
    <template id="indexSearch">
        <div class="container-full">

            <div class="row">

                <div class="col-lg-12 text-center v-center">

                    <h1>Cari Data Passport Anda</h1>
                    <p class="lead">
                        Masukkan nomor passport anda
                    </p>

                    <br>

                    <form class="col-lg-12" action="{{url('passport/search')}}">
                        <div class="input-group input-group-lg col-sm-offset-4 col-sm-4">
                            <input type="text" name="no_passport" class="center-block form-control input-lg" title="Masukan nomor passport anda" placeholder="Masukan nomor passport anda">
                            <span class="input-group-btn"><button class="btn btn-lg btn-primary" type="submit">Cari</button></span>
                        </div>
                        <br>
                        <div class="col-sm-4 col-sm-offset-4">
                            @if(request()->session()->has('error'))
                            <div class="alert alert-danger">
                                <i class="fa fa-warning"></i>
                                &nbsp;
                                {!! request()->session()->get('error') !!}
                            </div>
                            @endif
                        </div>
                    </form>
                </div>

            </div> <!-- /row -->

            <div class="row">

                <div class="col-lg-12 text-center v-center" style="font-size:39pt;">
                    <a href="#"><i class="icon-google-plus"></i></a> <a href="#"><i class="icon-facebook"></i></a>  <a href="#"><i class="icon-twitter"></i></a> <a href="#"><i class="icon-github"></i></a> <a href="#"><i class="icon-pinterest"></i></a>
                </div>

            </div>

            <br><br><br><br><br>

        </div> <!-- /container full -->
    </template>
@endsection

@section('scripts')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
        .modal-dialog{
            width: 800px;
        }
    </style>
    <script>
        var indexSearch = Vue.extend({
            template:'#indexSearch',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Schedule::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            },
            ready:function () {
                $('#d1').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $('#d2').datetimepicker({
                    useCurrent: false,
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $("#d1").on("dp.change", function (e) {
                    $('#d2').data("DateTimePicker").minDate(e.date);
                });
                $("#d2").on("dp.change", function (e) {
                    $('#d1').data("DateTimePicker").maxDate(e.date);
                });

                $('#monthPicker').datetimepicker({
                    useCurrent: false,
                    format: 'M',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
            }
        });
        Vue.component('index-search',indexSearch);
    </script>
    <!-- DATETIMEPICKER-->

@endsection