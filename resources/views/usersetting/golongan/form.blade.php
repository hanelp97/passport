<form action="{{route('Pegawai::Golongan::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Golongan *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->class : ""}}" name="class" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Jenis *</label>
            <select name="type" id="type" class="form-control" required>
                <option value="E1" {{$mode=='edit' ? ($data->type == "E1" ? "selected" : "") : ""}}>Eselon 1</option>
                <option value="E2" {{$mode=='edit' ? ($data->type == "E2" ? "selected" : "") : ""}}>Eselon 2</option>
                <option value="E3" {{$mode=='edit' ? ($data->type == "E3" ? "selected" : "") : ""}}>Eselon 3 / Gol. 4</option>
                <option value="E4" {{$mode=='edit' ? ($data->type == "E4" ? "selected" : "") : ""}}>Eselon 4 / Gol. 3</option>
                <option value="G1" {{$mode=='edit' ? ($data->type == "G1" ? "selected" : "") : ""}}>Gol 1</option>
                <option value="G2" {{$mode=='edit' ? ($data->type == "G2" ? "selected" : "") : ""}}>Gol 2</option>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
