<form action="{{route('Pegawai::Jabatan::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Jabatan *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->position : ""}}" name="position" required>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
