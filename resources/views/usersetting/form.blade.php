<form action="{{route('Pegawai::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">

            <label for="" class="control-label">Hak Akses *</label>
            <select name="role_id" class="form-control select2" id="role_id" required>
                @foreach($data_roles as $role)
                    <option value="{{$role->id}}" {{$mode=='edit' ? ($data->roles()->first()->id == $role->id?'selected':'') : ''}}>{{$role->display_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Nama Lengkap *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->name : ""}}" name="name" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Email </label>
            <input type="email" class="form-control" value="{{$mode=='edit' ? $data->email : ""}}" name="email">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Tanggal Lahir </label>
            <input type="text" class="form-control date-picker" id="tgl_lahir" value="{{$mode=='edit' ? $data->birth : ""}}" name="birth">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Username *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->username : ""}}" name="username" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Password {{$mode=='add' ?  "*" : ($mode=='edit' && $data->password == "" ? "*" : "")}}</label>
            <input type="password" class="form-control" name="password" {{$mode=='add' ?  "required" : ($mode=='edit' && $data->password == "" ? "required" : "")}}>
        </div>
        <div class="form-group">
            <label for="" class="control-label">NIP</label>
            <input type="number" class="form-control" value="{{$mode=='edit' ? $data->NIP : ""}}" name="NIP">
        </div>
        <div class="form-group">
            <label for="" class="control-label">HP</label>
            <input type="number" class="form-control" value="{{$mode=='edit' ? $data->phone : ""}}" name="phone">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Alamat</label>
            <textarea class="form-control" rows="5" name="address">{{$mode=='edit' ? $data->address : ""}}</textarea>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Jabatan</label>
            <select name="position_id" class="form-control chosen" id="position_id">
                <option value="0">None</option>
                @foreach($data_positions as $position)
                    <option value="{{$position->id}}" {{$mode=='edit' ? ($data->position_id == $position->id?'selected':'') : ''}}>{{$position->position}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Photo</label>
            <input type="file" name="photo" class="form-control">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Golongan.</label>
            <select name="class_id" class="form-control chosen" id="class_id">
                <option value="0">None</option>
                @foreach($data_classes as $classes)
                    <option value="{{$classes->id}}" {{$mode=='edit' ? ($data->class_id == $classes->id?'selected':'') : ''}}>{{$classes->class}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
<script>
    $('.chosen').chosen();
    $('#tgl_lahir').datetimepicker({
        format: 'DD-MM-YYYY',
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-crosshairs',
            clear: 'fa fa-trash'
        }
    });
</script>