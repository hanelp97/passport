@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <h3>Pengaturan User atau Pegawai
            <!-- <small>You are loged in!</small> -->
        </h3>
        @if(Session::has('success_message'))
            <div class="alert alert-success text-center">
                {{Session::get('success_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @elseif(Session::has('error_message'))
            <div class="alert alert-danger text-center">
                {{Session::get('error_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
    @endif
        <!-- START panel tab-->
        <div role="tabpanel" class="panel">
            <!-- Nav tabs-->
            <ul role="tablist" class="nav nav-tabs nav-justified">
                <li role="presentation" class="active">
                    <a href="#datapegawai" aria-controls="data" role="tab" data-toggle="tab">
                        <!-- <em class="fa fa-clock-o fa-fw"></em> -->Data Pegawai</a>
                </li>
                <li role="presentation">
                    <a href="#jabatandangolongan" aria-controls="daftar" role="tab" data-toggle="tab">
                        <!-- <em class="fa fa-money fa-fw"></em> -->Jabatan dan Golongan</a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content p0">
                <div id="datapegawai" role="tabpanel" class="tab-pane active">
                    <div class="panel-footer text-right">
                        <button @click.prevent="CreateUser()" class="btn btn-lg btn-info"><strong>Tambah Data Pegawai</strong></button>
                    </div>
                    <!-- START list group-->
                    <div class="panel b">
                        <div class="panel-body">
                            <div class="well">
                                <form action="" method="get">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label for="" class="control-label">Kata Kunci</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="keyword" class="form-control" value="{{Request::get('keyword')}}">
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary"><i class="fa fa-search"></i> &nbsp; Cari</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="datatable1" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th width="10%">Pangkat / Golongan</th>
                                        <th width="15%">Jabatan</th>
                                        <th>Email</th>
                                        <th width="10%">Hp</th>
                                        <th width="15%">Alamat</th>
                                        <th width="10%">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data_users->count() == 0)
                                        <tr>
                                            <td colspan="8">
                                                <div class="alert alert-warning text-center">
                                                    Data Empty
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($data_users as $user)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{@$user->Classification()->class ? : '-'}}</td>
                                            <td>{{@$user->Position()->position ? : '-'}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->address}}</td>
                                            <td class="text-center">
                                                <button type="button" @click.prevent="EditUser({{$user->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                @if(Auth::user()->id != $user->id)
                                                <button type="button" @click.prevent="DeleteUser({{$user->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @php(
                                            $no++
                                        )
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    Page {{$data_users->currentPage()}} of {{$data_users->lastPage()}}
                                </div>
                                <div class="col-lg-6 text-right">
                                    {{$data_users->appends([
                                         'status'=>Request::get('status'),
                                         'sortby'=>Request::get('sortby'),
                                         'order'=>Request::get('order'),
                                         'keyword'=>Request::get('keyword')
                                     ])->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END list group-->
                    <!-- <div class="panel-footer text-right"><a href="#" class="btn btn-default btn-sm">View All Activity </a>
                    </div> -->
                </div>

                <div id="jabatandangolongan" role="tabpanel" class="tab-pane">

                    <!-- START row-->
                    <div class="row">
                        <div style="clear: both;">&nbsp;</div>
                        <div class="col-lg-6">
                            <!-- START panel-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-footer text-right">
                                        <button @click.prevent="CreateClass" class="btn btn-lg btn-info"><strong>Tambah Pangkat / Golongan</strong></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <!-- START table responsive-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Pangkat / Golongan</th>
                                                <th>Jenis</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($data_classes->count() == 0)
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="alert alert-warning text-center">
                                                            Data Empty
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach($data_classes as $class)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$class->class}}</td>
                                                    <td>{{$class->type}}</td>
                                                    <td class="text-center">
                                                        <button type="button" @click.prevent="EditClass({{$class->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                        @if(Auth::user()->class_id != $class->id)
                                                            <button type="button" @click.prevent="DeleteClass({{$class->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php(
                                                    $no++
                                                )
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END table responsive-->
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- START panel-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-footer text-right">
                                        <button @click.prevent="CreatePosition" class="btn btn-lg btn-info"><strong>Tambah Jabatan</strong></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <!-- START table responsive-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jabatan</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($data_positions->count() == 0)
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="alert alert-warning text-center">
                                                            Data Empty
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach($data_positions as $position)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$position->position}}</td>
                                                    <td class="text-center">
                                                        <button type="button" @click.prevent="EditPosition({{$position->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                        @if(Auth::user()->position_id != $position->id)
                                                            <button type="button" @click.prevent="DeletePosition({{$position->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php(
                                                    $no++
                                                )
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END table responsive-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END row-->

                </div>
            </div>
        </div>
        <!-- END panel tab-->
    </template>
@endsection
@section('scripts-jakban')
    <script type="text/javascript" src="{{url('vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{url('vendor/chosen_v1.2.0/chosen.jquery.min.js')}}"></script>
    <script>
        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                CreateUser: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Pegawai',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                EditUser: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Pegawai',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                DeleteUser: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus pegawai ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Pegawai::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },

                CreatePosition: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jabatan',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Jabatan::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                EditPosition: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jabatan',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Jabatan::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                DeletePosition: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jabatan ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Pegawai::Jabatan::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },

                CreateClass: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Golongan',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Golongan::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                EditClass: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Golongan',
                        closable: false,
                        message:function () {
                            var url = '{{route('Pegawai::Golongan::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                DeleteClass: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus Golongan ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Pegawai::Golongan::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
@endsection