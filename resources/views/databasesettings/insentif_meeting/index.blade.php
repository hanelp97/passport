@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <div class="pull-right">
            <button @click.prevent="Create" class="btn btn-lg btn-primary">Tambah Data</button>
        </div>
        <h3>
            Insentif meeting
            <small>Database</small>
        </h3>
        <!-- START row-->
        <div class="row">
            <div class="col-lg-12">
                <!-- START panel-->
                <div class="panel panel-default">
                    <div class="panel-heading">Satuan Insentif meeting Perjalanan Dinas Dalam Negeri</div>
                    <div class="panel-body">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success_message')}}
                                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        @elseif(Session::has('error_message'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error_message')}}
                                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                    @endif
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Provinsi</th>
                                    <th>Satuan</th>
                                    <th>Fullboard diluar kota</th>
                                    <th>Fullboard didalam kota</th>
                                    <th>Fullday/ HalfDay didalam kota</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($data_insentif_meetings->count() == 0)
                                    <tr>
                                        <td colspan="9">
                                            <div class="alert alert-warning text-center">
                                                Data Empty
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($data_insentif_meetings as $im)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$im->Provinsi()->nama}}</td>
                                        <td>{{$im->unit}}</td>
                                        <td>Rp. {{number_format($im->fullboard_other_city)}}</td>
                                        <td>Rp. {{number_format($im->fullboard_in_city)}}</td>
                                        <td>Rp. {{number_format($im->fullday_halfday_in_city)}}</td>
                                        <td class="text-center">
                                            <button type="button" @click.prevent="Edit({{$im->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                            <button type="button" @click.prevent="Delete({{$im->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                        </td>
                                    </tr>
                                    @php(
                                        $no++
                                    )
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END table-responsive-->
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                Page {{$data_insentif_meetings->currentPage()}} of {{$data_insentif_meetings->lastPage()}}
                            </div>
                            <div class="col-lg-6 text-right">
                                {{$data_insentif_meetings->appends([
                                     'status'=>Request::get('status'),
                                     'sortby'=>Request::get('sortby'),
                                     'order'=>Request::get('order'),
                                     'q'=>Request::get('q')
                                 ])->render()}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END panel-->
            </div>
        </div>
        <!-- END row-->
    </template>
@endsection
@section('scripts-jakban')
    <script>
        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Insentif Meeting',
                        closable: false,
                        message:function () {
                            var url = '{{route('InsentifMeeting::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Insentif Meeting',
                        closable: false,
                        message:function () {
                            var url = '{{route('InsentifMeeting::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Are you want to delete data ' + id + ' ?',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('InsentifMeeting::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
@endsection