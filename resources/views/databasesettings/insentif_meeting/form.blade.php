<form action="{{route('InsentifMeeting::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Provinsi *</label>
            <select name="provinsi_id" class="form-control select2" id="provinsi_id" required>
                @foreach($master_provinsi as $provinsi)
                    <option value="{{$provinsi->id}}" {{$mode=='edit' ? ($data->provinsi_id == $provinsi->id?'selected':'') : ''}}>{{$provinsi->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Unit *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->unit : ""}}" name="unit" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Fullboard Luar Kota*</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->fullboard_other_city : 0}}" name="fullboard_other_city" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Fullboard Dalam Kota *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->fullboard_in_city : 0}}" name="fullboard_in_city" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Fullday / Halfday Dalam Kota *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->fullday_halfday_in_city : 0}}" name="fullday_halfday_in_city" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
