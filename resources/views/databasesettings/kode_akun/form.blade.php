<form action="{{route('KodeAkun::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Kode *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->code : ""}}" name="code" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Tipe *</label>
            <select name="type" class="form-control select2" id="type" required>
                <option value="A" {{$mode=='edit' ? ($data->type == "A" ?'selected':'') : ''}}>A</option>
                <option value="B" {{$mode=='edit' ? ($data->type == "B" ?'selected':'') : ''}}>B</option>
                <option value="C" {{$mode=='edit' ? ($data->type == "C" ?'selected':'') : ''}}>C</option>
                <option value="D" {{$mode=='edit' ? ($data->type == "D" ?'selected':'') : ''}}>D</option>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Kategori *</label>
            <select name="kategori_id" class="form-control select2" id="kategori_id" required>
                @foreach($kategoris as $kategori)
                    <option value="{{$kategori->id}}" {{$mode=='edit' ? ($data->kategori_id == $kategori->id?'selected':'') : ''}}>{{$kategori->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
   <div class="col-md-12">
       <div class="form-group">
           <label for="" class="control-label">Biaya *</label>
           <div class="input-group">
               <span class="input-group-addon">Rp.</span>
               <input type="number" class="form-control" value="{{$mode=='edit' ? $data->rate : 0}}" name="rate" required>
               <span class="input-group-addon">,-</span>
           </div>
       </div>
   </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
