@extends('layouts.app-jakban')

@section('content-jakban')

	<h3>Satuan Biaya Uang Harian Perjalanan Dinas dan Uang Representasi</h3>
	<!-- START row-->
    <div class="row">
       <div class="col-lg-12">
          <!-- START panel-->
          <div class="panel panel-default">
             <div class="panel-heading">Uang Harian Perjalanan Dinas Dalam Negeri</div>
             <div class="panel-body">
                <!-- START table-responsive-->
                <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover">
                      <thead>
                         <tr>
                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Satuan</th>
                            <th>Luar Kota</th>
                            <th>Dalam Kota Lebih Dari 8 Jam</th>
                            <th>DIKLAT</th>
                            <th>Aksi</th>
                         </tr>
                      </thead>
                      <tbody>
                         <tr>
                            <td>1</td>
                            <td>Aceh</td>
                            <td>OH</td>
                            <td>360.000</td>
                            <td>140.000</td>
                            <td>110.000</td>
                            <td><button type="button" class="btn btn-sm btn-default"><em class="fa fa-pencil"></em></button></td>
                         </tr>
                         <tr>
                            <td>2</td>
                            <td>Sumatera Utara</td>
                            <td>OH</td>
                            <td>370.000</td>
                            <td>150.000</td>
                            <td>110.000</td>
                            <td><button type="button" class="btn btn-sm btn-default"><em class="fa fa-pencil"></em></button></td>
                         </tr>
                      </tbody>
                   </table>
                </div>
                <!-- END table-responsive-->
             </div>

             <div class="panel-footer">
	              <div class="row">
	                 <div class="col-lg-6"></div>
	                 <div class="col-lg-6 text-right">
	                    <ul class="pagination pagination-sm">
	                       <li class="active"><a href="#">1</a>
	                       </li>
	                       <li><a href="#">2</a>
	                       </li>
	                       <li><a href="#">3</a>
	                       </li>
	                       <li><a href="#">»</a>
	                       </li>
	                    </ul>
	                 </div>
	              </div>
	          </div>
          </div>
          <!-- END panel-->
       </div>
    </div>
    <!-- END row-->

@endsection