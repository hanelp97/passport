<form action="{{route('KodeAkunKategori::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Name *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->name : ""}}" name="name" required>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
