<form action="{{route('Penginapan::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Provinsi *</label>
            <select name="provinsi_id" class="form-control select2" id="provinsi_id" required>
                @foreach($master_provinsi as $provinsi)
                    <option value="{{$provinsi->id}}" {{$mode=='edit' ? ($data->provinsi_id == $provinsi->id?'selected':'') : ''}}>{{$provinsi->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Unit *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->unit : ""}}" name="unit" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Eselon 1 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->eselon1 : 0}}" name="eselon1" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Eselon 2 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->eselon2 : 0}}" name="eselon2" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Eselon 3 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->eselon3 : 0}}" name="eselon3" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Eselon 4 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->eselon4 : 0}}" name="eselon4" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Golongan 1 / 2 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->golongan1_2 : 0}}" name="golongan1_2" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
