<form action="{{route('TiketPesawat::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Asal Kota *</label>
            <select name="city_first_id" class="form-control select2" id="city_first_id" required>
                @foreach($master_provinsi as $provinsi)
                    <optgroup label="{{$provinsi->nama}}">
                        @foreach($master_kabkota->where('provinsi_id',$provinsi->id) as $kabkota)
                            <option value="{{$kabkota->id}}" {{$mode=='edit' ? ($data->city_first_id == $kabkota->id?'selected':'') : ''}}>{{$kabkota->nama}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Tujuan Kota *</label>
            <select name="city_destination_id" class="form-control select2" id="city_destination_id" required>
                @foreach($master_provinsi as $provinsi)
                    <optgroup label="{{$provinsi->nama}}">
                        @foreach($master_kabkota->where('provinsi_id',$provinsi->id) as $kabkota)
                            <option value="{{$kabkota->id}}" {{$mode=='edit' ? ($data->city_destination_id == $kabkota->id?'selected':'') : ''}}>{{$kabkota->nama}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Biaya Tiket Bisnis *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->business : 0}}" name="business" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Biaya Tiket Ekonomi *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->economy : 0}}" name="economy" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
