<form action="{{route('VehicleRent::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Provinsi *</label>
            <select name="provinsi_id" class="form-control select2" id="provinsi_id" required>
                @foreach($master_provinsi as $provinsi)
                    <option value="{{$provinsi->id}}" {{$mode=='edit' ? ($data->provinsi_id == $provinsi->id?'selected':'') : ''}}>{{$provinsi->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Unit *</label>
            <input type="text" class="form-control" value="{{$mode=='edit' ? $data->unit : ""}}" name="unit" required>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Biaya Roda 4 *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->car : 0}}" name="car" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">BRoda 6/Bus Sedang *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->midle_bus : 0}}" name="midle_bus" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Roda 6/Bus Besar *</label>
            <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="number" class="form-control" value="{{$mode=='edit' ? $data->big_bus : 0}}" name="big_bus" required>
                <span class="input-group-addon">,-</span>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
