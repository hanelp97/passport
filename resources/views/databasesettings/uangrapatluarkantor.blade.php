@extends('layouts.app-jakban')

@section('content-jakban')

	<h3>Biaya Rapat Luar Kantor</h3>
	<!-- START row-->
    <div class="row">
       <div class="col-lg-12">
          <!-- START panel-->
          <div class="panel panel-default">
             <div class="panel-heading">Uang Harian Kegiatan Rapat / Pertemuan di Luar Kantor</div>
             <div class="panel-body">
                <!-- START table-responsive-->
                <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover">
                      <thead>
                         <tr>
                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Satuan</th>
                            <th>Fullboard Di Luar Kota</th>
                            <th>Fullboard Di Dalam Kota</th>
                            <th>Fullday/Halfday Di Dalam Kota</th>
                            <th>Aksi</th>
                         </tr>
                      </thead>
                      <tbody>
                         <tr>
                            <td>1</td>
                            <td>Aceh</td>
                            <td>OH</td>
                            <td>120.000</td>
                            <td>100.000</td>
                            <td>85.000</td>
                            <td><button type="button" class="btn btn-sm btn-default"><em class="fa fa-pencil"></em></button></td>
                         </tr>
                         <tr>
                            <td>2</td>
                            <td>Sumatera Utara</td>
                            <td>OH</td>
                            <td>130.000</td>
                            <td>110.000</td>
                            <td>95.000</td>
                            <td><button type="button" class="btn btn-sm btn-default"><em class="fa fa-pencil"></em></button></td>
                         </tr>
                      </tbody>
                   </table>
                </div>
                <!-- END table-responsive-->
             </div>

             <div class="panel-footer">
	              <div class="row">
	                 <div class="col-lg-6"></div>
	                 <div class="col-lg-6 text-right">
	                    <ul class="pagination pagination-sm">
	                       <li class="active"><a href="#">1</a>
	                       </li>
	                       <li><a href="#">2</a>
	                       </li>
	                       <li><a href="#">3</a>
	                       </li>
	                       <li><a href="#">»</a>
	                       </li>
	                    </ul>
	                 </div>
	              </div>
	          </div>
          </div>
          <!-- END panel-->
       </div>
    </div>
    <!-- END row-->

@endsection