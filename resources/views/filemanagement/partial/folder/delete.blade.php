<form action="{{route('FileManagement::Folder::Delete')}}" method="post" class="form-horizontal">
    {!! csrf_field() !!}
    <input type="hidden" name="id" value="{{$data_folder->id}}">
    <div class="col-md-12">
        Are you want to delete folder {{$data_folder->id}} with the sub folders and their files ? it is cannot be undo
        <br>
        <button type="submit" class="btn btn-danger">Delete</button>
    </div>
</form>