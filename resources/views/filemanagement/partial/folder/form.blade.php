<form action="{{route('FileManagement::Folder::Save')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data_folder->id}}">
    @else
        <input type="hidden" name="parent_id" value="{{Request::get('folder')}}">
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Folder Name</label>
            <input type="text" class="form-control" name="folder" value="{{$mode=='add'?'':$data_folder->folder}}" placeholder="Folder name" required>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>