<form action="{{route('FileManagement::File::Upload')}}" enctype="multipart/form-data" method="post" class="form-horizontal">
    {{ csrf_field() }}
    <input type="hidden" name="folder_id" value="{{Request::get('folder') ? : 0}}">
    <div class="col-md-12">
        <div class="form-group">
            <label for="" class="control-label">Folder Name</label>
            <input type="file" class="form-control" name="file" placeholder="Upload File" required>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>