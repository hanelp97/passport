@extends('layouts.app-jakban')
@section('content-jakban')
    <file-manager></file-manager>
    <template id="fileManager">
        <h3>File manager</h3>
        <div class="row">
            <div class="col-md-12">
                @if(Request::get('folder') > 0)
                    <h3>
                        Folder: {{$data_folder->folder}}
                    </h3>
                    <div>
                        <button type="button" class="btn btn-info" @click.prevent="EditFolder({{$data_folder->id}})" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-danger" @click.prevent="DeleteFolder({{$data_folder->id}})" data-toggle="tooltip" title="Delete"><i class="fa fa-eraser"></i></button>
                    </div>
                    <br>
                    <div class="clearfix"></div>
                    <ol class="breadcrumb">
                        <li><a href="{{route('FileManagement::Index')}}"><i class="fa fa-home"></i></a></li>
                        {!! Folder::Breadcrumbs($data_folder->parent_id) !!}
                        <li class="active">{{$data_folder->folder}}</li>
                    </ol>
                @endif
            </div>
            <div class="col-md-3">
                <div class="mb-lg">
                    <div class="row">
                        <div class="col-md-6">
                            <button @click.prevent="CreateFolder()" class="btn btn-success">Create Folder</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-info" @click.prevent="UploadFile()" data-toggle="tooltip" title="Edit"><i class="fa fa-upload"></i>Upload File</button>
                        </div>
                    </div>
                </div>
                <div class="panel b">
                    <div class="panel-body">
                        <strong class="text-muted">FOLDERS</strong>
                    </div>
                    <div class="list-group">
                        @if($data_folders->count() == 0)
                            <div class="list-group-item">Folder Empty</div>
                        @endif
                        @foreach($data_folders as $folder)
                            <a href="{{route('FileManagement::Index')}}?folder={{$folder->id}}" class="{{Request::get('folder') == $folder->id ? 'active':''}} list-group-item">
                                <span class="badge">{{$folder->Files()->count()}}</span>
                                <i class="fa fa-folder"></i> &nbsp;
                                <span>{{$folder->folder}}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                {{--<div class="panel b">--}}
                    {{--<div class="panel-body">--}}
                        {{--<strong class="text-muted">Type</strong>--}}
                    {{--</div>--}}
                    {{--<div class="list-group">--}}
                        {{--<a href="#" class="active list-group-item">--}}
                            {{--<span class="badge">49</span>--}}
                            {{--<span class="circle bg-white mr"></span>--}}
                            {{--<span>All</span>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<span class="badge">5</span>--}}
                            {{--<span class="circle circle-green mr"></span>--}}
                            {{--<span>Audio</span>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<span class="badge">12</span>--}}
                            {{--<span class="circle circle-danger mr"></span>--}}
                            {{--<span>Movie</span>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<span class="badge">22</span>--}}
                            {{--<span class="circle circle-warning mr"></span>--}}
                            {{--<span>Image</span>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<span class="badge">9</span>--}}
                            {{--<span class="circle circle-purple mr"></span>--}}
                            {{--<span>Code</span>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="clearfix text-sm">--}}
                            {{--<p class="pull-left">Storage</p>--}}
                            {{--<p class="pull-right">--}}
                                {{--<strong>25 GB / 100 GB</strong>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                        {{--<div class="progress progress-xs m0">--}}
                            {{--<div style="width:25%" class="progress-bar progress-bar-info">25%</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="col-md-9">
                @if(Session::has('success_message'))
                    <div class="alert alert-success text-center">
                        {{Session::get('success_message')}}
                        <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                @elseif(Session::has('error_message'))
                    <div class="alert alert-danger text-center">
                        {{Session::get('error_message')}}
                        <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                @endif
                @if($data_files->count() == 0)
                    <div class="alert alert-warning text-center">
                        Folder Empty
                    </div>
                @endif
                <div class="row">
                    @foreach($data_files as $file)
                        <div class="col-md-4 col-sm-6">
                            <div data-filter-group="file" class="panel discoverer">
                                <div class="panel-body text-center">
                                    <div class="clearfix discover">
                                        <div class="pull-left">
                                           <button class="btn btn-danger btn-xs" @click.prevent="DeleteFile({{$file->id}})"><i class="fa fa-eraser"></i></button>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{route('FileManagement::File::Download',['file'=>$file->file])}}" title="Download" class="text-muted mr-sm">
                                                <em class="fa fa-download fa-fw"></em>
                                            </a>
                                        </div>
                                    </div>
                                    <a href="#" class="file-icon ph-lg">
                                        <em class="fa fa-5x fa-file text-primary"></em>
                                    </a>
                                    <p>
                                        <small class="text-dark">{{$file->file}}</small>
                                    </p>
                                    <div class="clearfix m0 text-muted">
                                        <small class="pull-right">{{round(($file->size / 1024 / 1024) * 100) / 100 ? : 0}} MB</small>
                                        <small class="pull-left">{{date('d/m/Y',strtotime($file->created_at))}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </template>
@endsection

@section('scripts-jakban')
    <script>
        /**
         * Created by Hanel Prillian C on 10/6/2016.
         */

        var fileManager = Vue.extend({
            template:'#fileManager',
            data: function() {
                return {
                    formData:{
                        aw:123
                    }
                }
            },
            methods: {
                UploadFile: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-file',
                        title:'Upload New File',
                        closable: false,
                        message:function () {
                            var url = '{{route('FileManagement::File::Upload')}}?folder={{Request::get('folder') > 0 ? Request::get('folder'): ''}}&token='+Laravel.csrfToken;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                DeleteFile: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Are you want to delete file ' + id + ' ?',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('FileManagement::File::Delete')}}?id='+id+'&token='+Laravel.csrfToken).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        window.location.replace("filemanagement?folder="+response.json().folder_id);
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
                CreateFolder: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Add New Folder',
                        closable: false,
                        message:function () {
                            var url = '{{route('FileManagement::Folder::Add')}}?folder={{Request::get('folder') > 0 ? Request::get('folder'): ''}}&token='+Laravel.csrfToken;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                    {{--BootstrapDialog.show({--}}
                        {{--title:'Add New Folder',--}}
                        {{--closable: false,--}}
                        {{--message:$('<div></div>').load('{{route('FileManagement::Folder::Add')}}?folder={{Request::get('folder') > 0 ? Request::get('folder'): ''}}&token='+Laravel.csrfToken),--}}
                        {{--buttons:[--}}
                            {{--{--}}
                                {{--label: 'Close',--}}
                                {{--action: function(dialogRef){--}}
{{--//                                    BootstrapDialog.show({--}}
{{--//                                        message: 'Refreshing....',--}}
{{--//                                    });--}}
{{--//                                    location.reload();--}}
                                    {{--dialogRef.close();--}}
                                {{--}--}}
                            {{--}--}}
                        {{--]--}}
                    {{--});--}}
                },
                EditFolder: function(id){
                    var self = this;
                    BootstrapDialog.show({
                        title:'Edit New Folder',
                        closable: false,
                        message:$('<div></div>').load('{{route('FileManagement::Folder::Edit')}}?id='+id+'&folder={{Request::get('folder') > 0 ? Request::get('folder'): ''}}&token='+Laravel.csrfToken),
                        buttons:[
                            {
                                label: 'Close',
                                action: function(dialogRef){
//                                    BootstrapDialog.show({
//                                        message: 'Refreshing....',
//                                    });
//                                    location.reload();
                                    dialogRef.close();
                                }
                            }
                        ]
                    });
                },
                DeleteFolder: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Are you want to delete folder ' + id + ' ?',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('FileManagement::Folder::Delete')}}?id='+id+'&token='+Laravel.csrfToken).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        window.location.replace("filemanagement?folder="+response.json().parent_id);
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            }
        });
        Vue.component('file-manager',fileManager);
    </script>
@endsection