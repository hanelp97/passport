<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jadwal Kegiatan Balai Teknik Perkeretaapian Wilayah Jakarta Dan Banten</title>
    <meta name="description" content="Demo of A Free Coming Soon Bootstrap 4 Template by TemplateFlip.com."/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/report.css" rel="stylesheet">
    <style>
        .table-upper-header th {
            text-transform: uppercase;
        }

        .gradient {
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#88bfe8+0,70b0e0+100;Blue+3D+%2317 */
            background: #88bfe8; /* Old browsers */
            background: -moz-linear-gradient(top, #88bfe8 0%, #70b0e0 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #88bfe8 0%,#70b0e0 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #88bfe8 0%,#70b0e0 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#88bfe8', endColorstr='#70b0e0',GradientType=0 ); /* IE6-9 */
        }

    </style>
    <!-- fav icon -->
    <link rel="icon" type="image/x-icon" href="img/logo-single.png">
</head>
<body id="top">
<div class="site-wrapper gradient">
    <div class=" site-wrapper-inner">
        <div class="col-xs-12" style="padding: 20px">
            <div class=" clearfix col-xs-12">
                <div class="inner">
                    <h3 class="masthead-brand"><img src="{{url('/')}}/img/logo-balai-teknik-perkeretaapian.png" height="100px" width="auto"/></h3>
                    {{--<nav class="nav nav-masthead pull-right">--}}
                        {{--<a class="nav-link nav-social" href="#">JADWAL KEGIATAN </a>--}}
                    {{--</nav>--}}
                </div>
            </div>
            <div class="">
                <h3>BALAI TEKNIK PERKERETAAPIAN WILAYAH JAKARTA DAN BANTEN</h3><br/>
                <!-- START table-responsive-->
                <div class="table-responsive" style="width: 100%">
                    <table class="table table-upper-header" style="border:1px solid #301E6E">
                        <thead>
                        <tr>
                            <th style="border:1px solid #301E6E" width="3%" >
                                <span style="color:#333">
                                    No
                                </span>
                            </th>
                            <th style="border:1px solid #301E6E;" width="13%">
                                <div class="text-center" style="color:#333">
                                    Tanggal
                                </div>
                            </th>
                            <th style="border:1px solid #301E6E" width="8%">
                                <div class="text-center" style="color:#333">
                                    Pukul
                                </div>
                            </th>
                            <th style="border:1px solid #301E6E" width="20%">
                                <div class="text-center" style="color:#333">
                                    Perihal
                                </div>
                            </th>
                            <th style="border:1px solid #301E6E" width="10%">
                                <div class="text-center" style="color:#333">
                                    Dari
                                </div>
                            </th>
                            <th style="border:1px solid #301E6E" width="10%">
                                <div class="text-center" style="color:#333">
                                    Tempat
                                </div>
                            </th>
                            {{--<th style="border:1px solid #301E6E" width="10%">Disposisi</th>--}}
                            <th style="border:1px solid #301E6E" width="15%">
                                <div class="text-center" style="color:#333">
                                    Diteruskan
                                </div>
                            </th>
                            <th style="border:1px solid #301E6E" width="20%">
                                <div class="text-center" style="color:#333">
                                    Catatan
                                </div>
                            </th>
                            {{--<th style="border:1px solid #301E6E" width="10%">Klasifikasi</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if($data_schedules->count() == 0)
                            <tr>
                                <td colspan="10">
                                    <div class="alert alert-warning text-center">
                                        Data Empty
                                    </div>
                                </td>
                            </tr>
                        @endif
                        @php
                            $no = 1;
                        @endphp
                        @foreach($data_schedules as $schedule)
                            <tr class="">
                                <td style="border:1px solid #301E6E;color:#333">{{$no}}</td>
                                <td style="border:1px solid #301E6E;color:#333">
                                    {{date('d',strtotime($schedule->datetime_schedule_start))}}
                                    {{\App\Helpers\main_helper::indonesiaDate(date('n',strtotime($schedule->datetime_schedule_start)),"month")}}
                                    {{date('Y',strtotime($schedule->datetime_schedule_start))}}
                                    {{--s/d--}}
                                    {{--{{date('d',strtotime($schedule->datetime_schedule_end))}}--}}
                                    {{--{{\App\Helpers\main_helper::indonesiaDate(date('n',strtotime($schedule->datetime_schedule_end)),"month")}}--}}
                                    {{--{{date('Y',strtotime($schedule->datetime_schedule_end))}}--}}
                                </td>
                                <td style="border:1px solid #301E6E;color:#333">
                                    {{date('H:i',strtotime($schedule->datetime_schedule_start))}}
                                    {{-----}}
                                    {{--s/d--}}
                                    {{--{{date('H:i',strtotime($schedule->datetime_schedule_end))}}--}}
                                </td>
                                <td style="border:1px solid #301E6E;color:#333">{{$schedule->title}}</td>
                                <td style="border:1px solid #301E6E;color:#333">{{$schedule->from}}</td>
                                <td style="border:1px solid #301E6E;color:#333">{{$schedule->place}}</td>
                                {{--<td style="border:1px solid #301E6E;color:#333">--}}
                                    {{--@if(isset($schedule->category_id))--}}
                                        {{--<ol>--}}
                                            {{--@foreach(\App\ScheduleCategory::whereIn('id',array_map('intval',$schedule->category_id))->get() as $user)--}}
                                                {{--<li>{{$user->category_schedule}}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ol>--}}
                                    {{--@else--}}
                                        {{-----}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td style="border:1px solid #301E6E;color:#333">
                                    {{--@if(isset($schedule->forward_to_user_id))--}}
                                        {{--<ol>--}}
                                            {{--@foreach(\App\User::whereIn('id',array_map('intval',$schedule->forward_to_user_id))->get() as $user)--}}
                                                {{--<li>{{$user->name}}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ol>--}}
                                        {{--@else--}}
                                            {{-----}}
                                    {{--@endif--}}
                                    @if(isset($schedule->forward_to_forwarder))
                                        <ol>
                                            @foreach(\App\Forwarder::whereIn('id',array_map('intval',$schedule->forward_to_forwarder))->get() as $forwarder)
                                                <li>{{$forwarder->name}}</li>
                                            @endforeach
                                        </ol>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style="border:1px solid #301E6E;color:#333">{{$schedule->notes?:"-"}}</td>
                                {{--<td style="border:1px solid #301E6E;color:#333">--}}
                                    {{--@if($schedule->class == 1)--}}
                                        {{--<div class="inline wd-xxs label label-default">Biasa</div>--}}
                                    {{--@elseif($schedule->class == 2)--}}
                                        {{--<div class="inline wd-xxs label label-primary">Segera</div>--}}
                                    {{--@elseif($schedule->class == 3)--}}
                                        {{--<div class="inline wd-xxs label label-warning">Sangat Segera</div>--}}
                                    {{--@elseif($schedule->class == 4)--}}
                                        {{--<div class="inline wd-xxs label label-info">Rahasia</div>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                            </tr>
                            @php(
                                $no++
                            )
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        Page {{$data_schedules->currentPage()}} of {{$data_schedules->lastPage()}}
                    </div>
                    <div class="col-lg-6 text-right">
                        {{$data_schedules->appends([
                             'status'=>Request::get('status'),
                             'sortby'=>Request::get('sortby'),
                             'order'=>Request::get('order'),
                             'q'=>Request::get('q')
                         ])->render()}}
                    </div>
                </div>
                <!-- END table-responsive-->
            </div>
            {{--<div class="mastfoot">--}}
                {{--<div class="inner">--}}
                    {{--<p>&copy; 2017 - Balai Teknik Perkeretaapian Wilayah Jakarta dan Banten</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
</body>
</html>