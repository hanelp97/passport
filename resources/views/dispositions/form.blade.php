<form method="POST" action="{{route('Disposition::Save')}}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-horizontal">
    {{ csrf_field() }}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
    @else
    @endif
<!-- Isi Form Disposisi -->
    <div class="form-group">
        <label for="dari" class="col-md-3 control-label">No Surat </label>
        <div class="col-md-9">
            <input class="form-control" value="{{$mode=='edit' ? $data->no_surat_perintah : ""}}" name="no_surat_perintah" type="text" id="no_surat_perintah">
        </div>
    </div>
    <div class="form-group">
        <label for="tanggal_agenda" class="col-md-3 control-label">Tanggal Diterima</label>
        <div class="col-md-4">
            <div id="" class="input-group date datetimepicker">
                <input type="text" class="form-control" name="tgl_diterima" value="{{$mode=='edit' ? date('d-m-Y',strtotime($data->tgl_diterima)) : ""}}"  placeholder="Tgl Diterima">
                <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="dari" class="col-md-3 control-label">No Agenda</label>
        <div class="col-md-9">
            <input class="form-control" value="{{$mode=='edit' ? $data->no_agenda : ""}}" name="no_agenda" type="text" id="no_agenda">
        </div>
    </div>
    <div class="form-group">
        <label for="dari" class="col-md-3 control-label">Dari *</label>
        <div class="col-md-9">
            <input class="form-control" required="required" value="{{$mode=='edit' ? $data->from : ""}}" name="from" type="text" id="from">
        </div>
    </div>
    <div class="form-group">
        <label for="perihal" class="col-md-3 control-label">Perihal *</label>
        <div class="col-md-9">
            <textarea name="title" id="title" cols="30" class="form-control" rows="5" required="required">{{$mode=='edit' ? $data->title : ""}}</textarea>
            {{--<input class="form-control" required="required" value=""  name="title" type="text" id="title">--}}
        </div>
    </div>
    <div class="form-group">
        <label for="klasifikasi" class="col-md-3 control-label">Klasifikasi </label>
        <div class="col-md-9">
            <select class="form-control" id="class" name="class">
                <option selected="selected" value="0">-</option>
                <option value="1" {{$mode=='edit' ? ($data->class == 1 ? "selected" : "") : ""}}>Biasa</option>
                <option value="2" {{$mode=='edit' ? ($data->class == 2 ? "selected" : "") : ""}}>Segera</option>
                <option value="3" {{$mode=='edit' ? ($data->class == 3 ? "selected" : "") : ""}}>Sangat Segera</option>
                <option value="4" {{$mode=='edit' ? ($data->class == 4 ? "selected" : "") : ""}}>Rahasia</option>
            </select>
        </div>
    </div>
    {{--<div class="form-group">--}}
        {{--<label for="disposisi" class="col-md-3 control-label">Disposisi</label>--}}
        {{--<div class="col-md-9">--}}
            {{--<div class="row">--}}
                {{--@foreach($data_categories as $category)--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="checkbox c-checkbox">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="{{$category->id}}" name="category_id[]" {{$mode=='edit' && isset($data->category_id) ? (in_array($category->id,$data->category_id)? "checked" : "") : ""}}>--}}
                                {{--<span class="fa fa-check"></span>{{$category->category_schedule}}--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="form-group">
        <label for="catatan" class="col-md-3 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea class="form-control" name="notes" cols="30" rows="3" id="catatan">{{$mode=='edit' ? $data->notes : ""}}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="diteruskan" class="col-md-3 control-label">Diteruskan Kepada</label>
        <div class="col-md-9" style="max-height: 200px;overflow-y: scroll">
            @if($data_forwarders->count() == 0)
                <div class="alert alert-warning">
                    Data Forwarder Kosong!
                </div>
            @endif
            @foreach($data_forwarders as $forwarder)
                <div class="col-md-6">
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox" value="{{$forwarder->id}}" name="forward_to_forwarder[]" {{$mode=='edit' && isset($data->forward_to_forwarder) ? (in_array($forwarder->id,$data->forward_to_forwarder)? "checked" : "") : ""}}>
                            <span class="fa fa-check"></span>{{$forwarder->name}}
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <label for="folder" class="col-md-3 control-label">Folder *</label>
        <div class="col-md-9">
            <select class="form-control" id="folder" name="folder_id">
                @foreach($data_folders as $folder)
                    <option value="{{$folder->id}}" {{$mode=='edit' ? ($data->folder_id == $folder->id ? "selected" : "") : ""}}>{{$folder->folder}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="filelampiran" class="col-md-3 control-label">File Lampiran *</label>
        <div class="col-md-9">
            <input class="form-control filestyle" name="file" type="file" id="filelampiran">
            @if($mode=='edit' && $data->File())
                <a href="{{route('FileManagement::File::Download',['file'=>$data->File()->file])}}" title="Download" class="text-muted mr-sm">
                    {{$data->File()->permalink}}
                </a>
            @endif
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>

<script>
    $('.datetimepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-crosshairs',
            clear: 'fa fa-trash'
        }
    });
$('#datetimepicker1').datetimepicker({
    format: 'DD-MM-YYYY H:m',
    icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-crosshairs',
        clear: 'fa fa-trash'
    }
});
$('#datetimepickersampai').datetimepicker({
    useCurrent: false,
    format: 'DD-MM-YYYY H:m',
    icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-crosshairs',
        clear: 'fa fa-trash'
    }
});
$("#datetimepicker1").on("dp.change", function (e) {
    $('#datetimepickersampai').data("DateTimePicker").minDate(e.date);
});
$("#datetimepickersampai").on("dp.change", function (e) {
    $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
});
</script>