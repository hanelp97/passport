@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <div class="pull-right">
            <button @click.prevent="Create" class="btn btn-lg btn-primary">Tambah Disposisi</button>
        </div>

        <h3>Disposisi
            <small>You are loged in!</small>
        </h3>
        @if(Session::has('success_message'))
            <div class="alert alert-success text-center">
                {{Session::get('success_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @elseif(Session::has('error_message'))
            <div class="alert alert-danger text-center">
                {{Session::get('error_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
    @endif
        <!-- Table Agenda Meeting -->
        <!-- START row-->
        <div class="row">
            <div class="col-lg-12">
                <!-- START panel-->
                <div class="panel panel-default">
                    <div class="panel-heading">Agenda Pertemuan</div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Filter
                                <div class="pull-right">
                                    <button form="form-filter" class="btn btn-primary btn-xs">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="" method="get" id="form-filter">
                                    <input type="hidden" name="filter" value="true">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="tanggal_agenda" class="col-xs-12 control-label">Berdasarkan Tanggal Dibuat *</label>
                                                <div class="col-md-6">
                                                    <div id="d1" class="input-group date">
                                                        <input type="text" class="form-control" name="created_at" value="{{Request::get('created_at')}}"  placeholder="">
                                                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="month" class="col-xs-12 control-label">Berdasarkan Bulan *</label>
                                                <div class="col-md-12">
                                                    <div id="monthPicker" class="input-group date">
                                                        <input type="text" class="form-control" name="month" value="{{Request::get('month')}}"  placeholder="Bulan">
                                                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(Request::get('filter') == true)
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="month" class="col-xs-12 control-label">&nbsp;</label>
                                                    <div class="col-md-12">
                                                        <a target="_blank" href="{{route('Disposition::Report')}}?type=all&filter={{Request::get('filter')}}&datetime_schedule_start={{Request::get('datetime_schedule_start')}}&datetime_schedule_end={{Request::get('datetime_schedule_end')}}&month={{Request::get('month')}}" class="btn btn-primary"><i class="fa fa-print"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" style="min-width: 1000px">
                                <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%" class="text-center">#</th>
                                    <th width="300px">Perihal</th>
                                    <th width="300px">Dari</th>
                                    <th width="500px">Disposisi</th>
                                    <th width="15%">Diteruskan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($dispositions = $data_dispositions)
                                @if($dispositions->count() == 0)
                                    <tr>
                                        <td colspan="10">c
                                            <div class="alert alert-warning text-center">
                                                Data Empty
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($data_dispositions as $disposition)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td class="text-center">
                                            <a target="_blank" href="{{route('Disposition::Report',['id'=>$disposition->id])}}?type=unit" class="btn btn-sm btn-default"><em class="fa fa-print"></em></a>
                                            @if(isset($disposition->forward_to_user_id) && in_array(Auth::user()->id,$disposition->forward_to_user_id) || Auth::user()->roles->first()->name == 'admin')
                                                <button type="button" @click.prevent="Edit({{$disposition->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                <button type="button" @click.prevent="Delete({{$disposition->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                            @endif
                                        </td>
                                        <td>{{$disposition->title}}</td>
                                        <td>{{$disposition->from}}</td>
                                        <td>
                                            @if(isset($disposition->category_id))
                                                <ol>
                                                    @foreach(\App\ScheduleCategory::whereIn('id',array_map('intval',$disposition->category_id))->get() as $user)
                                                        <li>{{$user->category_schedule}}</li>
                                                    @endforeach
                                                </ol>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            {{--@if(isset($disposition->forward_to_user_id))--}}
                                                {{--<ol>--}}
                                                    {{--@foreach(\App\User::whereIn('id',array_map('intval',$disposition->forward_to_user_id))->whereNotIn('id',[Auth::user()->id])->get() as $user)--}}
                                                        {{--<li>{{$user->name}}</li>--}}
                                                    {{--@endforeach--}}
                                                {{--</ol>--}}
                                            {{--@else--}}
                                                {{-----}}
                                            {{--@endif--}}
                                            @if(isset($disposition->forward_to_forwarder))
                                                <ol>
                                                    @foreach(\App\Forwarder::whereIn('id',array_map('intval',$disposition->forward_to_forwarder))->get() as $forwarder)
                                                        <li>{{$forwarder->name}}</li>
                                                    @endforeach
                                                </ol>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        {{--<td>--}}
                                        {{--@if($disposition->class == 1)--}}
                                        {{--<div class="inline wd-xxs label label-default">Biasa</div>--}}
                                        {{--@elseif($disposition->class == 2)--}}
                                        {{--<div class="inline wd-xxs label label-primary">Segera</div>--}}
                                        {{--@elseif($disposition->class == 3)--}}
                                        {{--<div class="inline wd-xxs label label-warning">Sangat Segera</div>--}}
                                        {{--@elseif($disposition->class == 4)--}}
                                        {{--<div class="inline wd-xxs label label-info">Rahasia</div>--}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                        {{--@if($disposition->File())--}}
                                        {{--<a href="{{route('FileManagement::File::Download',['file'=>$disposition->File()->file])}}" title="Download" class="text-muted mr-sm">--}}
                                        {{--Download--}}
                                        {{--</a>--}}
                                        {{--@else--}}
                                        {{-----}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                    </tr>
                                    @php(
                                        $no++
                                    )
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END table-responsive-->
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                Page {{$data_dispositions->currentPage()}} of {{$data_dispositions->lastPage()}}
                            </div>
                            <div class="col-lg-6 text-right">
                                {{$data_dispositions->appends([
                                     'status'=>Request::get('status'),
                                     'sortby'=>Request::get('sortby'),
                                     'order'=>Request::get('order'),
                                     'q'=>Request::get('q')
                                 ])->render()}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END panel-->
            </div>
        </div>
        <!-- END row-->
        <!-- End Table Agenda Meeting -->
    </template>
@endsection

@section('scripts-jakban')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
       .modal-dialog {
            width: 800px;
        }
    </style>
    <script>
        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Disposisi',
                        closable: false,
                        message:function () {
                            var url = '{{route('Disposition::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Disposisi',
                        closable: false,
                        message:function () {
                            var url = '{{route('Disposition::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Disposition::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },

            },
            ready:function () {
                $('#d1').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });

                $('#monthPicker').datetimepicker({
                    useCurrent: false,
                    format: 'M',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
    <!-- DATETIMEPICKER-->

@endsection