@extends('layouts.app-jakban-print')

@section('content-jakban')
    <div class="row">
        <div class="col-xs-2 text-center">
            <img src="{{url('/')}}/img/logokemenhub.png" width="50px" style="padding-top: 10px">
        </div>
        <div class="col-xs-8 text-center">
            <h4>
                <strong>
                    KEPALA BALAI TEKNIK PERKERETAAPIAN WILAYAH JAKARTA DAN BANTEN
                </strong>
            </h4>
            <H2>
                <strong>
                    LEMBAR DISPOSISI
                </strong>
            </H2>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <table class="table" width="100%" border="1">
            <thead>
            <tr>
                <td colspan="4"><b>A. Surat Diterima</b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="20%">Dari</td>
                <td width="80%" colspan="3">{{$data->from}}</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>
                    {{date('d',strtotime($data->created_at))}}
                    {{\App\Helpers\main_helper::indonesiaDate(date('n',strtotime($data->created_at)),"month")}}
                    {{date('Y',strtotime($data->created_at))}}
                </td>
                <td width="15%">
                    Nomor Surat
                </td>
                <td width="25%">
                    {{$data->no_surat_perintah}}
                </td>
            </tr>
            <tr>
                <td>Perihal</td>
                <td colspan="3" style="height: 40px">
                    {{$data->title}}
                </td>
            </tr>
            <tr>
                <td>Diterima Tanggal</td>
                <td>
                    @if($data->tgl_diterima)
                        {{date('d',strtotime($data->tgl_diterima))}}
                        {{\App\Helpers\main_helper::indonesiaDate(date('n',strtotime($data->tgl_diterima)),"month")}}
                        {{date('Y',strtotime($data->tgl_diterima))}}
                    @endif
                </td>
                <td width="15%">
                    No. Agenda
                </td>
                <td>
                    {{$data->no_agenda}}
                </td>
            </tr>
            </tbody>
            <thead>
            <tr>
                <td colspan="4"><b>B. Diteruskan Kepada</b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4">
                    @foreach($data_forwarders as $forwarder)
                        <div class="col-xs-12">
                            <div class="square pull-left">
                                {!! isset($data->forward_to_forwarder) ? (in_array($forwarder->id,$data->forward_to_forwarder)? "<strong>V</strong>" : "") : "" !!}
                            </div>
                            <div class="" style="padding-top:7px">
                                {{$forwarder->name}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                    <div class="col-xs-12">
                        <div class="square pull-left">&nbsp;</div>
                        <div class="" style="padding-top:7px">
                            ..........................................................................................................
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="square pull-left">&nbsp;</div>
                        <div class="" style="padding-top:7px">
                            ..........................................................................................................
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {{--@if(isset($data->forward_to_user_id))--}}
                    {{--@foreach(\App\User::whereIn('id',array_map('intval',$data->forward_to_user_id))->get() as $user)--}}
                    {{--@foreach(\App\User::get() as $user)--}}
                    {{--<div class="col-xs-4">--}}
                    {{--<div class="square pull-left">&nbsp;</div>--}}
                    {{--<input type="checkbox" checked disabled>--}}
                    {{--<div class="pull-left" style="padding-top:7px">--}}
                    {{--{{$user->name}}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                    {{--@else--}}
                    {{-----}}
                    {{--@endif--}}
                </td>
            </tr>
            </tbody>
            <thead>
            <tr>
                <td><b>C. Klasifikasi Surat</b></td>
                <td colspan="3">
                    <div class="col-xs-3">
                        <div class="square pull-left">
                            {!! $data->class == 1 ? "<strong>V</strong>" : ""!!}
                        </div>
                        {{--                        <input type="checkbox" {{ $data->class == 1 ? 'checked' : ''}} disabled>--}}
                        <div class="" style="padding-top:7px">
                            Biasa
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="square pull-left">
                            {!! $data->class == 2 ? "<strong>V</strong>" : ""!!}
                        </div>
                        {{--                        <input type="checkbox" {{ $data->class == 2 ? 'checked' : ''}} disabled>--}}
                        <div class="" style="padding-top:7px">
                            Segera
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="square pull-left">
                            {!! $data->class == 3 ? "<strong>V</strong>" : ""!!}
                        </div>
                        {{--                        <input type="checkbox" {{ $data->class == 3 ? 'checked' : ''}} disabled>--}}
                        <div class="" style="padding-top:7px">
                            Sangat Segera
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="square pull-left">
                            {!! $data->class == 4 ? "<strong>V</strong>" : ""!!}
                        </div>
                        {{--                        <input type="checkbox" {{ $data->class == 4 ? 'checked' : ''}} disabled>--}}
                        <div class="" style="padding-top:7px">
                            Rahasia
                        </div>

                    </div>
                </td>
            </tr>
            </thead>
            <thead>
            <tr>
                <td colspan="4"><b>D. Disposisi</b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4">
                    @foreach(\App\ScheduleCategory::all() as $cat)
                        <div class="col-xs-6">
                            <div class="square pull-left">&nbsp;</div>
                            {{--<input type="checkbox" checked disabled>--}}
                            <div class="pull-left" style="padding-top:7px">
                                {{$cat->category_schedule}}
                            </div>
{{--                            <input type="checkbox" {{isset($data->category_id) && in_array($cat->id,array_map('intval',$data->category_id)) ? 'checked' : ''}} disabled>--}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <b>Catatan</b> <br>
                    <div style="min-height: 150px;position: relative">
                        {{--{{$data->notes}}--}}

                        <span style="position: absolute;bottom:0px;right:20px;text-align: right;font-size:14pt;">
                            {{@ucfirst(\App\User::find(Setting::name('kepala_balai'))->name)}}
                        </span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="text-center" style="padding-top:5px">
            Dengan Cara Cermat dan Tujuan Pasti, Kita Bangun Perkeretaapian Nasional
        </div>
        {{--<div class="row clearfix">--}}
            {{--<div class="col-xs-4 pull-right">--}}
                {{--<table>--}}
                    {{--<tr>--}}
                        {{--<td>--}}
                            {{--: Jakarta--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>--}}
                            {{--Pada Tanggal--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--: ..... {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--</table>--}}
                {{--<strong><u>{{ucfirst(\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->name)}}</u></strong><br>--}}
                {{--{{\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->NIP}}--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@endsection