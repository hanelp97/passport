@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <div class="pull-right">
            <button @click.prevent="Create" class="btn btn-lg btn-primary">Tambah Jadwal</button>
        </div>

        <h3>Jadwal
            <small>Kelola Jadwal</small>
        </h3>
        @if(Session::has('success_message'))
            <div class="alert alert-success text-center">
                {{Session::get('success_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @elseif(Session::has('error_message'))
            <div class="alert alert-danger text-center">
                {{Session::get('error_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
         @endif
        <!-- Table Agenda Meeting -->
        <!-- START row-->
        <div class="row">
            <div class="col-lg-12">
                <!-- START panel-->
                <div class="panel panel-default">
                    <div class="panel-heading">Agenda Pertemuan</div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Filter
                                <div class="pull-right">
                                    <button form="form-filter" class="btn btn-primary btn-xs">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="" method="get" id="form-filter">
                                    <input type="hidden" name="filter" value="true">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="tanggal_agenda" class="col-xs-12 control-label">Berdasarkan Tanggal Agenda *</label>
                                                <div class="col-md-6">
                                                    <div id="d1" class="input-group date">
                                                        <input type="text" class="form-control" name="datetime_schedule_start" value="{{Request::get('datetime_schedule_start')}}"  placeholder="Dari">
                                                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="d2" class="input-group date">
                                                        <input type="text" class="form-control" name="datetime_schedule_end" value="{{Request::get('datetime_schedule_end')}}" placeholder="Sampai">
                                                        <span class="input-group-addon">
                  <span class="fa fa-calendar"></span>
               </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="month" class="col-xs-12 control-label">Berdasarkan Bulan *</label>
                                                <div class="col-md-12">
                                                    <div id="monthPicker" class="input-group date">
                                                        <input type="text" class="form-control" name="month" value="{{Request::get('month')}}"  placeholder="Bulan">
                                                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(Request::get('filter') == true)
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="month" class="col-xs-12 control-label">&nbsp;</label>
                                                    <div class="col-md-12">
                                                        <a target="_blank" href="{{route('Schedule::Report')}}?type=all&filter={{Request::get('filter')}}&datetime_schedule_start={{Request::get('datetime_schedule_start')}}&datetime_schedule_end={{Request::get('datetime_schedule_end')}}&month={{Request::get('month')}}" class="btn btn-primary"><i class="fa fa-print"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- START table-responsive-->
                        <div class="table-responsive" style="overflow: scroll;">
                            <table class="table table-striped table-bordered table-hover table-upper-header" style="min-width:1500px">
                                <thead>
                                <tr>
                                    <th width="50px">No</th>
                                    <th width="10%" class="text-center">#</th>
                                    <th style="min-width:200px">Tanggal Agenda</th>
                                    <th style="min-width:130px">Pukul</th>
                                    <th width="20%">Perihal</th>
                                    <th width="10%">Dari</th>
                                    <th width="15%">Diteruskan</th>
                                    <th width="15%">Catatan</th>
                                    {{--<th width="15%">Disposisi</th>--}}
                                    {{--<th width="10%">Catatan</th>--}}
                                    {{--<th width="10%">Klasifikasi</th>--}}
                                    {{--<th width="10%">File Lampiran</th>--}}
                                    {{--<th width="6%">Aksi</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @if($data_schedules->count() == 0)
                                    <tr>
                                        <td colspan="10">
                                            <div class="alert alert-warning text-center">
                                                Data Empty
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($data_schedules as $schedule)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td class="text-center">
                                            <a target="_blank" href="{{route('Schedule::Report',['id'=>$schedule->id])}}?type=unit" class="btn btn-sm btn-default"><em class="fa fa-print"></em></a>
                                            @if(isset($schedule->forward_to_user_id) && in_array(Auth::user()->id,$schedule->forward_to_user_id) || Auth::user()->roles->first()->name == 'admin')
                                                <button type="button" @click.prevent="Edit({{$schedule->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                <button type="button" @click.prevent="Delete({{$schedule->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                            @endif
                                        </td>
                                        <td>
                                            {{date('d',strtotime($schedule->datetime_schedule_start))}}
                                            {{\App\Helpers\main_helper::indonesiaDate(date('n',strtotime($schedule->datetime_schedule_start)),"month")}}
                                            {{date('Y',strtotime($schedule->datetime_schedule_start))}}
                                        </td>
                                        <td>{{date('H:i',strtotime($schedule->datetime_schedule_start))}}
                                            {{--s/d--}}
                                            {{--{{date('H:i',strtotime($schedule->datetime_schedule_end))}}--}}
                                        </td>
                                        <td>{{$schedule->title}}</td>
                                        <td>{{$schedule->from}}</td>
                                        <td>
                                            {{--@if(isset($schedule->forward_to_user_id))--}}
                                            {{--<ol>--}}
                                            {{--@foreach(\App\User::whereIn('id',array_map('intval',$schedule->forward_to_user_id))->whereNotIn('id',[Auth::user()->id])->get() as $user)--}}
                                            {{--<li>{{$user->name}}</li>--}}
                                            {{--@endforeach--}}
                                            {{--</ol>--}}
                                            {{--@else--}}
                                            {{-----}}
                                            {{--@endif--}}
                                            @if(isset($schedule->forward_to_forwarder))
                                                <ol>
                                                    @foreach(\App\Forwarder::whereIn('id',array_map('intval',$schedule->forward_to_forwarder))->get() as $forwarder)
                                                        <li>{{$forwarder->name}}</li>
                                                    @endforeach
                                                </ol>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{$schedule->notes}}</td>
                                        {{--<td>--}}
                                            {{--@if(isset($schedule->category_id))--}}
                                                {{--<ol>--}}
                                                    {{--@foreach(\App\ScheduleCategory::whereIn('id',array_map('intval',$schedule->category_id))->get() as $user)--}}
                                                        {{--<li>{{$user->category_schedule}}</li>--}}
                                                    {{--@endforeach--}}
                                                {{--</ol>--}}
                                            {{--@else--}}
                                                {{-----}}
                                            {{--@endif--}}
                                        {{--</td>--}}
                                        {{--<td>{{$schedule->notes?:"-"}}</td>--}}
                                        {{--<td>--}}
                                        {{--@if($schedule->class == 1)--}}
                                        {{--<div class="inline wd-xxs label label-default">Biasa</div>--}}
                                        {{--@elseif($schedule->class == 2)--}}
                                        {{--<div class="inline wd-xxs label label-primary">Segera</div>--}}
                                        {{--@elseif($schedule->class == 3)--}}
                                        {{--<div class="inline wd-xxs label label-warning">Sangat Segera</div>--}}
                                        {{--@elseif($schedule->class == 4)--}}
                                        {{--<div class="inline wd-xxs label label-info">Rahasia</div>--}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                        {{--@if($schedule->File())--}}
                                        {{--<a href="{{route('FileManagement::File::Download',['file'=>$schedule->File()->file])}}" title="Download" class="text-muted mr-sm">--}}
                                        {{--{{$schedule->File()->permalink}}--}}
                                        {{--</a>--}}
                                        {{--@else--}}
                                        {{-----}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                    </tr>

                                    @php($no++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END table-responsive-->
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                Page {{$data_schedules->currentPage()}} of {{$data_schedules->lastPage()}}
                            </div>
                            <div class="col-lg-6 text-right">
                                {{$data_schedules->appends([
                                     'status'=>Request::get('status'),
                                     'sortby'=>Request::get('sortby'),
                                     'order'=>Request::get('order'),
                                     'q'=>Request::get('q')
                                 ])->render()}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END panel-->
            </div>
        </div>
        <!-- END row-->
        <!-- End Table Agenda Meeting -->
    </template>
@endsection

@section('scripts-jakban')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
        .modal-dialog{
            width: 800px;
        }
    </style>
    <script>
        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('Schedule::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('Schedule::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            },
            ready:function () {
                $('#d1').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $('#d2').datetimepicker({
                    useCurrent: false,
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $("#d1").on("dp.change", function (e) {
                    $('#d2').data("DateTimePicker").minDate(e.date);
                });
                $("#d2").on("dp.change", function (e) {
                    $('#d1').data("DateTimePicker").maxDate(e.date);
                });

                $('#monthPicker').datetimepicker({
                    useCurrent: false,
                    format: 'M',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
    <!-- DATETIMEPICKER-->

@endsection