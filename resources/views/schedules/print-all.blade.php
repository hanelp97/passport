@extends('layouts.app-jakban-print')

@section('content-jakban')
    <div class="row">
        <div class="col-xs-2 text-center">
            <img src="{{url('/')}}/img/logokemenhub.png" width="50px" style="padding-top: 20px">
        </div>
        <div class="col-xs-10">
            <h4>
                <strong>
                    KEPALA BALAI KERETAAPIAN WILAYAH JAKARTA DAN BANTEN
                </strong>
            </h4>
            <H2>
                <strong>
                    Daftar Jadwal
                </strong>
            </H2>
            @if(Request::get('month') != "")
                Bulan {{\App\Helpers\main_helper::indonesiaDate(Request::get('month'),'month')}}
            @elseif(Request::get('datetime_schedule_start') != "" && Request::get('datetime_schedule_end') != "")
               Tanggal Agenda {{date('d-m-Y',strtotime(Request::get('datetime_schedule_start')))}} s/d {{date('d-m-Y',strtotime(Request::get('datetime_schedule_end')))}}
            @endif
            {{date('Y')}}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <br>
            <table border="1" class="table">
                <thead>
                <tr>
                    <th width="3%">No</th>
                    <th width="10%">Tanggal Agenda</th>
                    <th width="20%">Perihal</th>
                    <th width="10%">Dari</th>
                    <th width="10%">Disposisi</th>
                    <th width="15%">Diteruskan</th>
                    <th width="10%">Catatan</th>
                    <th width="10%">Klasifikasi</th>
                </tr>
                </thead>
                <tbody>
                @if($data_schedules->count() == 0)
                    <tr>
                        <td colspan="10">
                            <div class="alert alert-warning text-center">
                                Data Empty
                            </div>
                        </td>
                    </tr>
                @endif
                @php
                    $no = 1;
                @endphp
                @foreach($data_schedules as $schedule)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{date('d/m/Y',strtotime($schedule->datetime_schedule_start))}} s/d {{date('d/m/Y',strtotime($schedule->datetime_schedule_end))}}</td>
                        <td>{{$schedule->title}}</td>
                        <td>{{$schedule->from}}</td>
                        <td>
                            @if(isset($schedule->category_id))
                                <ol>
                                    @foreach(\App\ScheduleCategory::whereIn('id',array_map('intval',$schedule->category_id))->get() as $user)
                                        <li>{{$user->category_schedule}}</li>
                                    @endforeach
                                </ol>
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if(isset($schedule->forward_to_user_id))
                                <ol>
                                    @foreach(\App\User::whereIn('id',array_map('intval',$schedule->forward_to_user_id))->get() as $user)
                                        <li>{{$user->name}}</li>
                                    @endforeach
                                </ol>
                            @else
                                -
                            @endif
                        </td>
                        <td>{{$schedule->notes?:"-"}}</td>
                        <td>
                            @if($schedule->class == 1)
                                <div class="inline wd-xxs label label-default">Biasa</div>
                            @elseif($schedule->class == 2)
                                <div class="inline wd-xxs label label-primary">Segera</div>
                            @elseif($schedule->class == 3)
                                <div class="inline wd-xxs label label-warning">Sangat Segera</div>
                            @elseif($schedule->class == 4)
                                <div class="inline wd-xxs label label-info">Rahasia</div>
                            @endif
                        </td>
                    </tr>
                    @php(
                        $no++
                    )
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection