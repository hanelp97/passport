<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="{{url('vendor/fontawesome/css/font-awesome.min.css')}}">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="{{url('/vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- ANIMATE.CSS-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/animate.css/animate.min.css">
    <!-- WHIRL (spinners)-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/whirl/dist/whirl.css">
    <!-- =============== PAGE VENDOR STYLES ===============-->
    <!-- TAGS INPUT-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <!-- SLIDER CTRL-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css">
    <!-- CHOSEN-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/chosen_v1.2.0/chosen.min.css">
    <!-- DATETIMEPICKER-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <!-- COLORPICKER-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css">
    <!-- SELECT2-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/select2/dist/css/select2.css">
    <link rel="stylesheet" href="{{url('/')}}/vendor/select2-bootstrap-theme/dist/select2-bootstrap.css">
    <!-- WEATHER ICONS-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/weather-icons/css/weather-icons.min.css">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="{{url('/')}}/css/app-modify.css" id="maincss">
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap-dialog.min.css" id="maincss">
    <!-- fav icon -->
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/img/logo-single.png">
    <!-- FULLCALENDAR-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/fullcalendar/dist/fullcalendar.css">

    <style>
        .table-upper-header th {
            text-transform: uppercase;
        }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
@yield('modal-dialog')
<div class="wrapper">
    <!-- top navbar-->
    <header class="topnavbar-wrapper">
        <!-- START Top Navbar-->
        <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a href="#/" class="navbar-brand">
                    <div class="brand-logo">
                        <img src="{{url('/')}}/img/logo.png" alt="App Logo" class="img-responsive">
                    </div>
                    <div class="brand-logo-collapsed">
                        <img src="{{url('/')}}/img/logo-single.png" alt="App Logo" class="img-responsive">
                    </div>
                </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
                <!-- START Left navbar-->
                <ul class="nav navbar-nav">
                    <li>
                        <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                        <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                            <em class="fa fa-navicon"></em>
                        </a>
                        <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                        <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                            <em class="fa fa-navicon"></em>
                        </a>
                    </li>
                </ul>
                <!-- END Left navbar-->
                <!-- START Right Navbar-->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Fullscreen (only desktops)-->
                    <li class="visible-lg">
                        <a href="#" data-toggle-fullscreen="">
                            <em class="fa fa-expand"></em>
                        </a>
                    </li>

                    <!-- START Offsidebar button-->
                    @if (Auth::check())
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <em class="icon-notebook"></em> Log Out
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                @endif
                <!-- END Offsidebar menu-->
                </ul>
                <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
        </nav>
    </header>
    <!-- END Top Navbar-->

    <!-- sidebar-->
    <aside class="aside">
        <!-- START Sidebar (left)-->
        <div class="aside-inner">
            <nav data-sidebar-anyclick-close="" class="sidebar">
                <!-- START sidebar nav-->
                <ul class="nav">
                    <!-- START user info-->
                    <li class="has-user-block">
                        <div id="user-block" class="expand">
                            <div class="item user-block">
                                <!-- User picture-->
                                <div class="user-block-picture">
                                    <div class="user-block-status">
                                        <img src="{{Auth::user()->photo ? url('upload/file/'.Auth::user()->photo) :  url('img/logo-single.png')}}" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                                        <div class="circle circle-success circle-lg"></div>
                                    </div>
                                </div>
                                <!-- Name and Job-->
                                @if (Auth::check())
                                    <div class="user-block-info">
                                        <span class="user-block-name">Hello, {{ Auth::user()->name }}</span>
                                        <!-- <span class="user-block-role"></span> -->
                                    </div>
                                @endif
                            </div>
                        </div>
                    </li>
                    <!-- END user info-->
                    <!-- Iterates over all sidebar items-->
                    <li class="nav-heading ">
                        <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                    </li>
                    <li class=" ">
                        <a href="{{ url('/home') }}" title="Dashboard">
                            <!-- <div class="pull-right label label-info">3</div> -->
                            <em class="icon-speedometer"></em>
                            <span data-localize="sidebar.nav.DASHBOARD">Dashboard</span>
                        </a>
                    </li>
                    @role(['admin','arsip','pegawai'])
                    <li class=" ">
                        <a href="{{ route('FileManagement::Index') }}" title="Kelola File">
                            <div class="pull-right label label-success">{{\App\File::all()->count()}} File</div>
                            <em class="icon-folder-alt"></em>
                            <span data-localize="sidebar.nav.FILEMANAGEMENT">Kelola File</span>
                        </a>
                    </li>
                    @endrole

                    @role(['admin','arsip','pegawai'])
                    <li class=" ">
                        <a href="{{ route('Schedule::Index') }}" title="Schedules">
                            {{--<div class="pull-right label label-info">30 Waiting</div>--}}
                            <em class="icon-calendar"></em>
                            <span data-localize="sidebar.nav.Schedules">Schedules</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ route('Disposition::Index') }}" title="Disposisi">
                            {{--<div class="pull-right label label-info">30 Waiting</div>--}}
                            <em class="icon-calendar"></em>
                            <span data-localize="sidebar.nav.Schedules">Disposisi</span>
                        </a>
                    </li>
                    @endrole

                    @role(['admin','keuangan'])
                    <li class=" ">
                        <a href="{{ route('OfficialTask::Index') }}" title="Perjalanan Dinas">
                            <!-- <div class="pull-right label label-info">30 File</div> -->
                            <em class="icon-briefcase"></em>
                            <span data-localize="sidebar.nav.Perjalanan Dinas">Perjalanan Dinas</span>
                        </a>
                    </li>
                    <li class="nav-heading ">
                        <span data-localize="sidebar.heading.SETTINGS">Settings</span>
                    </li>
                    <li class=" ">
                        <a href="#datasettings" title="Database Perjalanan Dinas" data-toggle="collapse">
                            <!-- <div class="pull-right label label-success">30</div> -->
                            <em class="icon-book-open"></em>
                            <span data-localize="sidebar.nav.DATASETTINGS">Database Perjalanan Dinas</span>
                        </a>
                        <ul id="datasettings" class="nav sidebar-subnav collapse">
                            <li class="sidebar-subnav-header">Data Settings</li>
                            <li class=" ">
                                <a href="{{ route('KodeAkunKategori::Index') }}" title="Kode Akun Kategori">
                                    <span>Kode Akun Kategori</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('KodeAkun::Index') }}" title="Kode Akun">
                                    <span>Kode Akun</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('BiayaPerjalananDinas::Index') }}" title="Biaya Perjalanan Dinas">
                                    <span>Biaya Perjalanan Dinas</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('TiketPesawat::Index') }}" title="Biaya Tiket Pesawat">
                                    <span>Biaya Tiket Pesawat</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('Penginapan::Index') }}" title="Biaya Penginapan">
                                    <span>Biaya Penginapan</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('Taxi::Index') }}" title="Biaya Taksi">
                                    <span>Biaya Taksi</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('VehicleRent::Index') }}" title="Biaya Sewa Kendaraan">
                                    <span>Biaya Sewa Kendaraan</span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ route('InsentifMeeting::Index') }}" title="Insentif Rapat">
                                    <span>Insentif Rapat</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endrole


                    @role(['admin'])
                    <li class="nav-heading ">
                        <span data-localize="sidebar.heading.SETTINGS">Settings User / Pegawai</span>
                    </li>
                    <li class=" ">
                        <a href="{{ route('Pegawai::Index') }}" title="User / Pegawai">
                            <em class="icon-user"></em>
                            <span data-localize="sidebar.nav.PEGAWAI">User / Pegawai</span>
                        </a>
                    </li>
                    @endrole
                </ul>
                <!-- END sidebar nav-->
            </nav>
        </div>
        <!-- END Sidebar (left)-->
    </aside>

    <!-- Main section-->
    <section>
        <!-- Page content-->
        <div class="content-wrapper">
            @yield('content')
        </div>
    </section>
    <!-- End Section -->

    <!-- Page footer-->
    <footer>
        <span>&copy; 2017 - {{ config('app.name', 'Balai Teknik') }}</span>
    </footer>
    <!-- End Page Footer -->
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="{{url('/')}}/vendor/modernizr/modernizr.custom.js"></script>
<!-- MATCHMEDIA POLYFILL-->
<script src="{{url('/')}}/vendor/matchMedia/matchMedia.js"></script>
<!-- JQUERY-->
<script src="{{url('/')}}/vendor/jquery/dist/jquery.js"></script>
<!-- BOOTSTRAP-->
<script src="{{url('/')}}/vendor/bootstrap/dist/js/bootstrap.js"></script>
<!-- STORAGE API-->
<script src="{{url('/')}}/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
<!-- JQUERY EASING-->
<script src="{{url('/')}}/vendor/jquery.easing/js/jquery.easing.js"></script>
<!-- ANIMO-->
<script src="{{url('/')}}/vendor/animo.js/animo.js"></script>
<!-- SLIMSCROLL-->
<script src="{{url('/')}}/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<!-- SCREENFULL-->
<script src="{{url('/')}}/vendor/screenfull/dist/screenfull.js"></script>
<!-- LOCALIZE-->
<script src="{{url('/')}}/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>
<!-- RTL demo-->
<script src="{{url('/')}}/js/demo/demo-rtl.js"></script>
<!-- =============== PAGE VENDOR SCRIPTS ===============-->
<!-- JQUERY UI-->
<script src="{{url('/')}}/vendor/jquery-ui/jquery-ui.js"></script>
<script src="{{url('/')}}/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<!-- MOMENT JS-->
<script src="{{url('/')}}/vendor/moment/min/moment-with-locales.min.js"></script>
<!-- DEMO-->
<script src="{{url('/')}}/js/demo/demo-flot.js"></script>
<!-- FULLCALENDAR-->
<script src="{{url('/')}}/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{url('/')}}/vendor/fullcalendar/dist/gcal.js"></script>
<!-- FILESTYLE-->
<script src="{{url('/')}}/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>
<!-- TAGS INPUT-->
<script src="{{url('/')}}/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- SLIDER CTRL-->
<script src="{{url('/')}}/vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
<!-- INPUT MASK-->
<script src="{{url('/')}}/vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
<!-- WYSIWYG-->
<script src="{{url('/')}}/vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js"></script>
<script src="{{url('/')}}/vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js"></script>
<!-- DATETIMEPICKER-->
<script type="text/javascript" src="{{url('/')}}/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- COLORPICKER-->
<script type="text/javascript" src="{{url('/')}}/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
<!-- Demo-->
<script src="{{url('/')}}/js/demo/demo-forms.js"></script>
<!-- bootstrap dialog-->
<script src="{{url('/')}}/js/bootstrap-dialog.min.js"></script>
<!-- SELECT2-->
<script src="{{url('/')}}/vendor/select2/dist/js/select2.js"></script>
<!-- =============== APP SCRIPTS ===============-->
<script src="{{url('/')}}/js/app-modify.js"></script>
<script src="{{url('/')}}/js/vue.js"></script>
<script>
    function ShowAlert(text,type) {
        var bdType = {
            'warning': BootstrapDialog.TYPE_DANGER,
            'danger': BootstrapDialog.TYPE_DANGER,
            'success': BootstrapDialog.TYPE_SUCCESS,
            'default': BootstrapDialog.TYPE_DEFAULT
        };
        type = type || "warning";
        BootstrapDialog.alert({
            title: 'Information',
            message: text,
            type: bdType[type]
        });
    }

    Vue.http.interceptors.push((request, next) => {
        request.headers['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
    next();
    });
    Vue.http.options.emulateJSON = true;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Vue.directive('chosen', {
        twoWay: true,
        bind: function() {
            return this.vm.$nextTick((function(_this) {
                return function() {
                    return $(_this.el).chosen({
                        inherit_select_classes: false,
                        width: '100%'
                    }).change(function(ev) {
                        var i, len, option, ref, values;
                        if (_this.el.hasAttribute('multiple')) {
                            values = [];
                            ref = _this.el.selectedOptions;
                            for (i = 0, len = ref.length; i < len; i++) {
                                option = ref[i];
                                values.push(option.value);
                            }
                            return _this.set(values);
                        } else {
                            return _this.set(_this.el.value);
                        }
                    });
                };
            })(this));
        },
        update: function(nv, ov) {
            return $(this.el).trigger('chosen:updated');
        }
    });
</script>

@yield('scripts-jakban')

<script>
    const app = new Vue({
        el: 'body',
        methods:{
        }
    });
</script>
</body>

</html>
