<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
<style>

<?php //include(public_path().'/css/bootstrap.css');?>
body {
    background: white;
    font-family: Arial;
    font-size:11px;
}
.borderless td, .borderless th {
    border: none;
}
.margin-lr-10 {
    margin:0 10px 0 10px;
}
.padding-lr-15 {
    padding:0 15px 0 15px;
}
.pull-left {
    float: left!important;
}
.pull-right {
    float: right!important;
}
.col-xs-12 {
    width: 100%;
    display: block;
}
.col-xs-6 {
    width: 50%;
    float: left;
}
.col-xs-8 {
    width: 66.66666667%;
    float: left;
}
.col-xs-7 {
    width: 58.33333%;
    float: left;
}
.col-xs-3 {
    width: 25%;
    float: left;
}
.col-xs-4 {
    width: 33.33333333%;
    float: left;
}
.col-xs-10 {
    width: 83.333333333%;
    float: left;
}
.col-xs-2 {
    width: 16.666666667%;
    float: left;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.text-left {
    text-align: left;
}
table.table {
    border-collapse: collapse;
    border-spacing: 0;
}
table td {
    vertical-align: top;
    padding:5px 5px 5px 10px;
}
table.no-padding td {
    vertical-align: top;
    padding:0px;
}
.page-break {
    page-break-after: always;
    page-break-inside: avoid;
}
.clearfix:after {
    display: block;
    content: '';
    clear: both;
}
    .square {
        width: 8px;
        height: 8px;
        padding:5px;
        margin:3px;
        display: block;
        border:1px solid black;
    }
.square.fill {
    background: black;
}
.last-node:last-child {
    display: none;
}
    .cleafix:after {
        clear: both;
        content: '';
        display: block;
    }
</style>
</head>
{{--<body {!! Request::get('output') != 'pdf' ? 'onload="window.print()"' : '' !!}>--}}
<body>
<div class="col-xs-12">
    @yield('content-jakban')
</div>
<!-- JQUERY-->
<script src="/vendor/jquery/dist/jquery.js"></script>
@yield('scripts-jakban')
</body>
</html>
