<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="{{url('vendor/fontawesome/css/font-awesome.min.css')}}">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="{{url('/vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- ANIMATE.CSS-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/animate.css/animate.min.css">
    <!-- WHIRL (spinners)-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/whirl/dist/whirl.css">
    <!-- =============== PAGE VENDOR STYLES ===============-->
    <!-- TAGS INPUT-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <!-- SLIDER CTRL-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css">
    <!-- CHOSEN-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/chosen_v1.2.0/chosen.min.css">
    <!-- DATETIMEPICKER-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <!-- COLORPICKER-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css">
    <!-- SELECT2-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/select2/dist/css/select2.css">
    <link rel="stylesheet" href="{{url('/')}}/vendor/select2-bootstrap-theme/dist/select2-bootstrap.css">
    <!-- WEATHER ICONS-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/weather-icons/css/weather-icons.min.css">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="{{url('/')}}/css/app-modify.css" id="maincss">
    <link rel="stylesheet" href="{{url('/')}}/css/bootstrap-dialog.min.css" id="maincss">
    <!-- fav icon -->
    {{--<link rel="icon" type="image/x-icon" href="{{url('/')}}/img/logo-single.png">--}}
    <!-- FULLCALENDAR-->
    <link rel="stylesheet" href="{{url('/')}}/vendor/fullcalendar/dist/fullcalendar.css">

    <style>
        .table-upper-header th {
            text-transform: uppercase;
        }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
@yield('modal-dialog')
<div class="wrapper">
    <!-- top navbar-->
    <header class="topnavbar-wrapper">
        <!-- START Top Navbar-->
        <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{url('/')}}">Brand</a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
                <!-- START Left navbar-->

                <!-- END Left navbar-->
                <!-- START Right Navbar-->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Fullscreen (only desktops)-->
                    <li class="visible-lg">
                        <a href="#" data-toggle-fullscreen="">
                            <em class="fa fa-expand"></em>
                        </a>
                    </li>
                <!-- END Offsidebar menu-->
                </ul>
                <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
        </nav>
    </header>
    <!-- END Top Navbar-->

    <!-- Main section-->
    <section>
        <!-- Page content-->
        <div class="content-wrapper">
            @yield('content')
        </div>
    </section>
    <!-- End Section -->

    <!-- Page footer-->
    <footer>
        <span>&copy; 2017 - {{ config('app.name', 'Balai Teknik') }}</span>
    </footer>
    <!-- End Page Footer -->
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="{{url('/')}}/vendor/modernizr/modernizr.custom.js"></script>
<!-- MATCHMEDIA POLYFILL-->
<script src="{{url('/')}}/vendor/matchMedia/matchMedia.js"></script>
<!-- JQUERY-->
<script src="{{url('/')}}/vendor/jquery/dist/jquery.js"></script>
<!-- BOOTSTRAP-->
<script src="{{url('/')}}/vendor/bootstrap/dist/js/bootstrap.js"></script>
<!-- STORAGE API-->
<script src="{{url('/')}}/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
<!-- JQUERY EASING-->
<script src="{{url('/')}}/vendor/jquery.easing/js/jquery.easing.js"></script>
<!-- ANIMO-->
<script src="{{url('/')}}/vendor/animo.js/animo.js"></script>
<!-- SLIMSCROLL-->
<script src="{{url('/')}}/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<!-- SCREENFULL-->
<script src="{{url('/')}}/vendor/screenfull/dist/screenfull.js"></script>
<!-- LOCALIZE-->
<script src="{{url('/')}}/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>
<!-- RTL demo-->
<script src="{{url('/')}}/js/demo/demo-rtl.js"></script>
<!-- =============== PAGE VENDOR SCRIPTS ===============-->
<!-- JQUERY UI-->
<script src="{{url('/')}}/vendor/jquery-ui/jquery-ui.js"></script>
<script src="{{url('/')}}/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<!-- MOMENT JS-->
<script src="{{url('/')}}/vendor/moment/min/moment-with-locales.min.js"></script>
<!-- DEMO-->
<script src="{{url('/')}}/js/demo/demo-flot.js"></script>
<!-- FULLCALENDAR-->
<script src="{{url('/')}}/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{url('/')}}/vendor/fullcalendar/dist/gcal.js"></script>
<!-- FILESTYLE-->
<script src="{{url('/')}}/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>
<!-- TAGS INPUT-->
<script src="{{url('/')}}/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- SLIDER CTRL-->
<script src="{{url('/')}}/vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
<!-- INPUT MASK-->
<script src="{{url('/')}}/vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
<!-- WYSIWYG-->
<script src="{{url('/')}}/vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js"></script>
<script src="{{url('/')}}/vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js"></script>
<!-- DATETIMEPICKER-->
<script type="text/javascript" src="{{url('/')}}/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- COLORPICKER-->
<script type="text/javascript" src="{{url('/')}}/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
<!-- Demo-->
<script src="{{url('/')}}/js/demo/demo-forms.js"></script>
<!-- bootstrap dialog-->
<script src="{{url('/')}}/js/bootstrap-dialog.min.js"></script>
<!-- SELECT2-->
<script src="{{url('/')}}/vendor/select2/dist/js/select2.js"></script>
<!-- =============== APP SCRIPTS ===============-->
<script src="{{url('/')}}/js/app-modify.js"></script>
<script src="{{url('/')}}/js/vue.js"></script>
<script>
    function ShowAlert(text,type) {
        var bdType = {
            'warning': BootstrapDialog.TYPE_DANGER,
            'danger': BootstrapDialog.TYPE_DANGER,
            'success': BootstrapDialog.TYPE_SUCCESS,
            'default': BootstrapDialog.TYPE_DEFAULT
        };
        type = type || "warning";
        BootstrapDialog.alert({
            title: 'Information',
            message: text,
            type: bdType[type]
        });
    }

    Vue.http.interceptors.push((request, next) => {
        request.headers['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
    next();
    });
    Vue.http.options.emulateJSON = true;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Vue.directive('chosen', {
        twoWay: true,
        bind: function() {
            return this.vm.$nextTick((function(_this) {
                return function() {
                    return $(_this.el).chosen({
                        inherit_select_classes: false,
                        width: '100%'
                    }).change(function(ev) {
                        var i, len, option, ref, values;
                        if (_this.el.hasAttribute('multiple')) {
                            values = [];
                            ref = _this.el.selectedOptions;
                            for (i = 0, len = ref.length; i < len; i++) {
                                option = ref[i];
                                values.push(option.value);
                            }
                            return _this.set(values);
                        } else {
                            return _this.set(_this.el.value);
                        }
                    });
                };
            })(this));
        },
        update: function(nv, ov) {
            return $(this.el).trigger('chosen:updated');
        }
    });
</script>

@yield('scripts')

<script>
    const app = new Vue({
        el: 'body',
        methods:{
        }
    });
</script>
</body>

</html>
