<form id="formInput" @click="Submit()" method="POST" action="{{route('OfficialTask::Save')}}" enctype="multipart/form-data" class="form-horizontal">
    {!! csrf_field() !!}
    {{--<input type="hidden" name="_token" value="{{Request::get('token')}}" />--}}
    @if($mode=='edit')
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" v-model="formData.employee_id" value="{{$data->employee_id}}">
        <input type="hidden" v-model="formData.no_surat_perintah" value="{{$data->no_surat_perintah}}">
        <input type="hidden" v-model="formData.description" value="{{$data->description}}">
        <input type="hidden" v-model="formData.remarks" value="{{$data->remarks}}">
        <input type="hidden" v-model="formData.date_departure" value="{{date('d-m-Y',strtotime($data->date_departure))}}">
        <input type="hidden" v-model="formData.date_arrival" value="{{date('d-m-Y',strtotime($data->date_arrival))}}">
        <input type="hidden" v-model="formData.day_duration" value="{{$data->day_duration}}">
        <input type="hidden" v-model="formData.type_code" value="{{$data->type_code}}">
        <input type="hidden" v-model="formData.departure_place_id" value="{{$data->departure_place_id}}">
        <input type="hidden" v-model="formData.arrival_place_id" value="{{$data->arrival_place_id}}">
        <input type="hidden" v-model="formData.perjalanan_amount_type" value="{{$data->perjalanan_amount_type}}">
        <input type="hidden" v-model="formData.type_amount" value="{{$data->type_amount}}">
        <input type="hidden" v-model="formData.transportation_type" value="{{$data->transportation_type}}">
        <input type="hidden" v-model="formData.transportation_class" value="{{$data->transportation_class}}">
        <input type="hidden" v-model="formData.airplane_ticker_id" value="{{$data->airplane_ticker_id}}">
        <input type="hidden" v-model="formData.with_vehicle_rent" value="{{$data->with_vehicle_rent}}">
        <input type="hidden" v-model="formData.vehicle_rent_type" value="{{$data->vehicle_rent_type}}">
        <input type="hidden" v-model="formData.vehicle_rent_price_id" value="{{$data->vehicle_rent_price_id}}">
        <input type="hidden" v-model="formData.transportation_amount_type" value="{{$data->transportation_amount_type}}">
        <input type="hidden" v-model="formData.transportation_amount_type_other" value="{{$data->transportation_amount_type_other}}">
        <input type="hidden" v-model="formData.vehicle_amount_type" value="{{$data->vehicle_amount_type}}">
        <input type="hidden" v-model="formData.transportation_amount" value="{{$data->transportation_amount}}">
        <input type="hidden" v-model="formData.transportation_amount_qty" value="{{$data->transportation_amount_qty}}">
        <input type="hidden" v-model="formData.transportation_amount_2" value="{{$data->transportation_amount_2}}">
        <input type="hidden" v-model="formData.transportation_amount_qty_2" value="{{$data->transportation_amount_qty_2}}">
        <input type="hidden" v-model="formData.vehicle_amount" value="{{$data->vehicle_amount}}">
        <input type="hidden" v-model="formData.kode_akun_id" value="{{$data->kode_akun_id}}">

        {{--HOTEL--}}
        <input type="hidden" v-model="formData.with_hotel" value="{{$data->with_hotel}}">
        <input type="hidden" v-model="formData.hotel_ticket_id" value="{{$data->hotel_ticket_id}}">
        <input type="hidden" v-model="formData.hotel_day_duration" value="{{$data->hotel_day_duration}}">
        <input type="hidden" v-model="formData.hotel_amount_type" value="{{$data->hotel_amount_type}}">
        <input type="hidden" v-model="formData.hotel_amount" value="{{round($data->hotel_day_duration > 0 ? $data->hotel_amount/$data->hotel_day_duration : 0)}}">
        {{--/HOTEL--}}

        {{--TAXI--}}
        <input type="hidden" v-model="formData.with_taxi" value="{{$data->with_taxi}}">
        <input type="hidden" v-model="formData.taxi_qty_departure" value="{{$data->taxi_qty_departure}}">
        <input type="hidden" v-model="formData.taxi_qty_arrival" value="{{$data->taxi_qty_arrival}}">
        <input type="hidden" v-model="formData.taxi_ticket_departure" value="{{$data->taxi_ticket_departure}}">
        <input type="hidden" v-model="formData.taxi_ticket_arrival" value="{{$data->taxi_ticket_arrival}}">
        <input type="hidden" v-model="formData.taxi_amount_departure_type" value="{{$data->taxi_amount_departure_type}}">
        <input type="hidden" v-model="formData.taxi_amount_arrival_type" value="{{$data->taxi_amount_arrival_type}}">
        <input type="hidden" v-model="formData.taxi_amount_departure" value="{{round($data->taxi_qty_departure > 0 ? $data->taxi_amount_departure/$data->taxi_qty_departure : 0)}}">
        <input type="hidden" v-model="formData.taxi_amount_arrival" value="{{round($data->taxi_qty_arrival > 0  ? $data->taxi_amount_arrival/$data->taxi_qty_arrival : 0)}}">
        {{--/TAXI--}}

    @elseif($mode=='add')
        <input type="hidden" name="id" value="">
        <input type="hidden" v-model="formData.employee_id" value="">
        <input type="hidden" v-model="formData.no_surat_perintah" value="">
        <input type="hidden" v-model="formData.description" value="">
        <input type="hidden" v-model="formData.remarks" value="">
        <input type="hidden" v-model="formData.date_departure" value="">
        <input type="hidden" v-model="formData.date_arrival" value="">
        <input type="hidden" v-model="formData.day_duration" value="">
        <input type="hidden" v-model="formData.type_code" value="">
        {{--<input type="hidden" v-model="formData.departure_place_id" value="">--}}
        <input type="hidden" v-model="formData.arrival_place_id" value="">
        <input type="hidden" v-model="formData.transportation_amount_type" value="Default">
        <input type="hidden" v-model="formData.transportation_amount_type_other" value="PP">
        <input type="hidden" v-model="formData.transportation_class" value="">
        <input type="hidden" v-model="formData.airplane_ticker_id" value="">
        <input type="hidden" v-model="formData.with_taxi" value="N">
        <input type="hidden" v-model="formData.taxi_qty" value="1">
        <input type="hidden" v-model="formData.taxi_ticket_id" value="">
        <input type="hidden" v-model="formData.with_hotel" value="N">
        <input type="hidden" v-model="formData.hotel_ticket_id" value="">
        <input type="hidden" v-model="formData.with_vehicle_rent" value="N">
        <input type="hidden" v-model="formData.vehicle_rent_type" value="">
        <input type="hidden" v-model="formData.vehicle_rent_price_id" value="">
        <input type="hidden" v-model="formData.hotel_amount_type" value="">
        <input type="hidden" v-model="formData.taxi_amount_type" value="">
        <input type="hidden" v-model="formData.vehicle_amount_type" value="">
        <input type="hidden" v-model="formData.hotel_amount" value="">
        <input type="hidden" v-model="formData.transportation_amount" value="">
        <input type="hidden" v-model="formData.taxi_amount" value="">
        <input type="hidden" v-model="formData.vehicle_amount" value="">
    @endif
<!-- Isi Form Perjalanan Dinas -->
    <div class="form-group">
        <label for="dari" class="col-md-12">Kode (auto) * -
            <a @click.prevemt="pilih_kode_lain = !pilih_kode_lain"><i class="fa fa-search"></i></a>
        </label>
        <div class="col-md-12">
            {{--<select class="form-control" v-if="pilih_kode_lain" v-model="formData.code" id="code" name="code" required>--}}
                {{--<option value="0">Pilih</option>--}}
                {{--@foreach($data_kode as $code)--}}
                    {{--<option value="{{$code}}"--}}
                            {{--{{$mode == 'edit' && $data->employee_id == $employee->id ? 'selected' : ''}}--}}
                    {{-->{{$code}}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
            {{--v-if="!pilih_kode_lain"--}}
            <input class="form-control" disabled v-model="formData.code" required="required" value="{{$mode=='edit' ? $data->code : $code_pd}}" name="code" type="text" id="code">
        </div>
    </div>
    <div class="form-group">
        <label for="nama" class="col-md-12">Nama Pegawai *</label>
        <div class="col-md-12">
            <select class="form-control"
                    v-chosen="formData.employee_id"
                    v-model="formData.employee_id" id="employee_id" name="employee_id" required>
                <option value="0">Pilih</option>
                @foreach($data_user_employees as $employee)
                <option value="{{$employee->id}}" {{$mode == 'edit' && $data->employee_id == $employee->id ? 'selected' : ''}}>{{$employee->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="pangkat_jabatan" class="col-md-12">Pangkat - Jabatan *</label>
        <div class="col-md-12">
            <input class="form-control" readonly="readonly" v-model="formData.pangkat_jabatan" type="text" value="">
        </div>
    </div>

    <div class="form-group">
        <label for="dari" class="col-md-12">No Surat Perintah *</label>
        <div class="col-md-12">
            <input class="form-control" v-model="formData.no_surat_perintah" value="{{$mode=='edit' ? $data->no_surat_perintah : ""}}" name="no_surat_perintah" type="text" id="no_surat_perintah" required>
        </div>
    </div>

    <div class="form-group">
        <label for="maksud" class="col-md-12">Maksud Perjalanan Dinas *</label>
        <div class="col-md-12">
            <textarea class="form-control" v-model="formData.description" name="description" cols="30" rows="3" id="description" required></textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="tanggal_berangkat_kembali" class="col-md-12 l">Tanggal Berangkat &amp; Kembali *</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div id="datetimepicker1" class="input-group date">
                        <input type="text" v-model="formData.date_departure" name="date_departure" id="date_departure" class="form-control" placeholder="Berangkat" required>
                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="datetimepickersampai" class="input-group date">
                        <input type="text" v-model="formData.date_arrival" name="date_arrival" id="date_arrival"  class="form-control" placeholder="Kembali" required>
                        <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                   </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="lama" class="col-md-12 l">Lama Perjalanan Dinas *</label>
        <div class="col-md-12">
            <div class="input-group">
                <input type="text" v-model="calculatedDay" class="form-control" readonly>
                <input type="hidden" v-model="calculatedDay" name="day_duration" id="day_duration" class="form-control" value="1" placeholder="jumlah hari" aria-describedby="basic-addon2" required>
                <span class="input-group-addon" id="basic-addon2">Hari</span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="jenis_perjalanan" class="col-md-12">Jenis Perjalanan *</label>
        <div class="col-md-12">
            <select class="form-control" v-model="formData.type_code" name="type_code" id="type_code" required>
                <option value="" >Pilih</option>
                @foreach($data_pd_jenis as $jenis)
                    <option value="{{$jenis->code}}">{{$jenis->jenis}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="berangkat" class="col-md-12 ">Tempat Berangkat *</label>
        <div class="col-md-12">
            {{\App\Place::find(Setting::name('setting_tempat_asal'))->name}}
            <input type="hidden" :value="{{Setting::name('setting_tempat_asal')}}" v-model="formData.departure_place_id" name="departure_place_id" id="departure_place_id" >
            {{--<select class="form-control" v-model="formData.departure_place_id" name="departure_place_id" id="departure_place_id" required>--}}
                {{--<option value="">Pilih</option>--}}
                {{--@foreach($data_places as $place)--}}
                    {{--<option value="{{$place->id}}" {{$mode=='add'?(Setting::name('setting_tempat_asal')==$place->provinsi_id?'selected':''):($data->departure_place_id==$place->id)?'selected':''}}>{{$place->name}}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
        </div>
    </div>

    <div class="form-group">
        <label for="tujuan" class="col-md-12">Tempat Tujuan *</label>
        <div class="col-md-12">
            <select class="form-control tempat-tujuan" v-chosen="formData.arrival_place_id" v-model="formData.arrival_place_id" name="arrival_place_id" id="arrival_place_id" required>
                <option value="">Pilih</option>
{{--                <option value="@{{ arrival.id }}" v-for="arrival in dataArrivalList">@{{ arrival.name }}</option>--}}
                <template v-if="formData.type_code == 'PerjalananDinasDalamKota' || formData.type_code == 'PerjalananDinasDiklat' || formData.type_code == 'RapatDalamKotaFullboard' || formData.type_code == 'RapatDalamKotaFullDatHalfDay'">
                    @foreach($data_places->where('type','In') as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                </template>
                <template v-if="formData.type_code == 'PerjalananDinasLuarKota' || formData.type_code == 'RapatLuarKotaFullboard'">
                    @foreach($data_places->where('type','Out') as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                </template>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="alat_angkut" class="col-md-12">Jenis Harga Perjalanan Dinas *</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" v-model="formData.perjalanan_amount_type" name="perjalanan_amount_type" id="perjalanan_amount_type" required>
                        <option value="Default">Default</option>
                        <option value="Other">Lainnya / Kwitansi</option>
                    </select>
                </div>
            </div>
            <div class="row" v-if="formData.perjalanan_amount_type == 'Other'">
                <br>
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Rp. </span>
                        <input type="number" class="form-control" v-model="formData.type_amount" name="type_amount" id="type_amount" required>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="alat_angkut" class="col-md-12">Transportasi *</label>
        <div class="col-md-12">
            <div class="row">
               <div class="col-md-6">
                   <select class="form-control" v-model="formData.transportation_type" name="transportation_type" id="transportation_type" required>
                       <option value="">Pilih</option>
                       <option value="Car">Mobil</option>
                       <option value="Airplane">Pesawat</option>
                       <option value="Train">Kereta</option>
                       <option value="Bus">Bis</option>
                   </select>
               </div>
               <div class="col-md-6">
                   <select class="form-control" v-if="formData.transportation_type == 'Airplane' || formData.transportation_type == 'Train' || formData.transportation_type == 'Bus'" v-model="formData.transportation_class" name="transportation_class" id="transportation_class" required>
                       <option value="">Pilih Kelas</option>
                       <option value="Economy">Ekonomi</option>
                       <option value="Business">Bisnis</option>
                   </select>
               </div>
           </div>
            <div class="row" v-if="formData.transportation_type == 'Airplane'">
                <br>
                <div class="col-md-12">
                    <select class="form-control" v-model="formData.airplane_ticker_id" name="airplane_ticker_id" id="airplane_ticker_id" v-chosen="formData.airplane_ticker_id" required>
                        <option value="">Pilih Wilayah Tiket Pesawat</option>
                        @foreach($data_airplane_tickets as $ticket)
                            <option value="{{$ticket->id}}" {{$mode == 'edit' && $data->airplane_ticker_id == $ticket->id ? 'selected' : ''}}>{{$ticket->First()->nama}} - {{$ticket->Destination()->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="alat_angkut" class="col-md-12">Jenis Harga Transportasi *</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <select class="form-control" v-model="formData.transportation_amount_type" name="transportation_amount_type" id="transportation_amount_type" required>
                        <option value="Default" v-if="formData.transportation_type == 'Airplane'">Default</option>
                        <option value="Other">Lainnya / Kwitansi</option>
                    </select>
                </div>
                <div class="col-md-6" v-if="formData.transportation_amount_type == 'Other'">
                    <select class="form-control" v-model="formData.transportation_amount_type_other" name="transportation_amount_type_other" id="transportation_amount_type_other" required>
                        <option value="PP">PP</option>
                        <option value="Non-PP">Non-PP</option>
                    </select>
                </div>
            </div>
            <div class="row" v-if="formData.transportation_amount_type == 'Other'">
                <br>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">Rp. </span>
                                <input type="number" class="form-control" v-model="formData.transportation_amount" name="transportation_amount" id="transportation_amount" required>
                            </div>
                        </div>
                        <div class="col-md-4" v-if="formData.transportation_amount_type_other == 'Non-PP'">
                            <div class="input-group">
                                <input type="number" min="1" class="form-control" v-model="formData.transportation_amount_qty" name="transportation_amount_qty" id="transportation_amount_qty" required>
                                <span class="input-group-addon" id="basic-addon3">Kali</span>
                            </div>
                        </div>
                    </div>
                    <p class="help-block" v-if="formData.transportation_amount_type_other == 'Non-PP'">
                        Berangkat
                    </p>
                </div>
                <div class="col-md-6" v-if="formData.transportation_amount_type_other == 'Non-PP'">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">Rp. </span>
                                <input type="number" class="form-control" v-model="formData.transportation_amount_2" name="transportation_amount_2" id="transportation_amount_2" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="number" min="1" class="form-control" v-model="formData.transportation_amount_qty_2" name="transportation_amount_qty_2" id="transportation_amount_qty_2" required>
                                <span class="input-group-addon" id="basic-addon3">Kali</span>
                            </div>
                        </div>
                    </div>

                    <p class="help-block">
                        Pulang
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="taxi" class="col-md-12">Penggunaan Taxi</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <label class="radio-inline c-radio">
                        <input v-model="formData.with_taxi" name="with_taxi"  type="radio" value="Y">
                        <span class="fa fa-circle"></span>Iya</label>
                    </label>
                    <label class="radio-inline c-radio">
                        <input v-model="formData.with_taxi" name="with_taxi" type="radio" value="N">
                        <span class="fa fa-circle"></span>Tidak</label>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div v-if="formData.with_taxi == 'Y'">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"> Alokasi Taksi Tempat Keberangkatan</div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Tempat*</label>
                            <div class="col-md-12">
                                {{$data_taxi_tickets->where('provinsi_id',\App\Place::find(Setting::name('setting_tempat_asal'))->provinsi_id)->first()->Provinsi()->nama}}
                                <input type="hidden" value="{{$data_taxi_tickets->where('provinsi_id',\App\Place::find(Setting::name('setting_tempat_asal'))->provinsi_id)->first()->id}}" v-model="formData.taxi_ticket_departure" name="taxi_ticket_departure" id="taxi_ticket_departure" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Jenis Harga*</label>
                            <div class="col-md-12">
                                <select class="form-control" v-model="formData.taxi_amount_departure_type" name="taxi_amount_departure_type" id="taxi_amount_departure_type" required>
                                    <option value="Default">Default</option>
                                    <option value="Other">Lainnya / Kwitansi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" v-if="formData.taxi_amount_departure_type == 'Other'">
                        <div class="form-group">
                            <label class="col-md-12">Harga Taxi*</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon3">Rp. </span>
                                    <input type="number" class="form-control" v-model="formData.taxi_amount_departure" name="taxi_amount_departure" id="taxi_amount_departure" required>
                                </div>
                                <small class="help-block">Satuan</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Jumlah Taxi *</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" class="form-control" v-model="formData.taxi_qty_departure" name="taxi_qty_departure" id="taxi_qty_departure" required>
                                    <span class="input-group-addon" id="basic-addon3">Kali</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-12">Total Harga Taxi Keberangkatan</label>
                            <div class="col-md-12">
                                <p class="form-control-static">
                                    Rp. @{{ formData.taxi_qty_departure * formData.taxi_amount_departure }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"> Alokasi Taksi Tempat Tujuan</div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Tempat*</label>
                            <div class="col-md-12">
                                <select class="form-control" v-model="formData.taxi_ticket_arrival" name="taxi_ticket_arrival" id="taxi_ticket_arrival" required>
                                    <option value="" >Pilih Wilayah Tiket Taksi</option>
                                    @foreach($data_taxi_tickets as $ticket)
                                        <option value="{{$ticket->id}}">{{$ticket->Provinsi()->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Jenis Harga*</label>
                            <div class="col-md-12">
                                <select class="form-control" v-model="formData.taxi_amount_arrival_type" name="taxi_amount_arrival_type" id="taxi_amount_arrival_type" required>
                                    <option value="Default">Default</option>
                                    <option value="Other">Lainnya / Kwitansi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" v-if="formData.taxi_amount_arrival_type == 'Other'">
                        <div class="form-group">
                            <label class="col-md-12">Harga Taxi*</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon3">Rp. </span>
                                    <input type="number" class="form-control" v-model="formData.taxi_amount_arrival" name="taxi_amount_arrival" id="taxi_amount_arrival" required>
                                </div>
                                <small class="help-block">Satuan</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Jumlah Taxi *</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" class="form-control" v-model="formData.taxi_qty_arrival" name="taxi_qty_arrival" id="taxi_qty_arrival" required>
                                    <span class="input-group-addon" id="basic-addon3">Kali</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-12">Total Harga Taxi Ditempat Tujuan</label>
                            <div class="col-md-12">
                                <p class="form-control-static">
                                    Rp. @{{ formData.taxi_qty_arrival * formData.taxi_amount_arrival }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label for="Hotel" class="col-md-12">Hotel</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <label class="radio-inline c-radio">
                        <input v-model="formData.with_hotel" name="with_hotel"  type="radio" value="Y">
                        <span class="fa fa-circle"></span>Iya</label>
                    </label>
                    <label class="radio-inline c-radio">
                        <input v-model="formData.with_hotel" name="with_hotel" type="radio" value="N">
                        <span class="fa fa-circle"></span>Tidak</label>
                    </label>
                </div>
                <div class="col-md-8" v-if="formData.with_hotel == 'Y'">
                    <select class="form-control" v-model="formData.hotel_ticket_id" name="hotel_ticket_id" id="hotel_ticket_id" required>
                        <option value="">Pilih Wilayah Tiket Hotel</option>
                        @foreach($data_hotel_tickets as $ticket)
                            <option value="{{$ticket->id}}">{{$ticket->Provinsi()->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" v-if="formData.with_hotel == 'Y'">
       <div class="row">
           <div class="col-md-6">
               <label for="alat_angkut" class="col-md-12">Jenis Harga Hotel *</label>
               <div class="col-md-12">
                   <div class="row">
                       <div class="col-md-12">
                           <select class="form-control" v-model="formData.hotel_amount_type" name="hotel_amount_type" id="hotel_amount_type" required>
                               <option  value="Default">Default</option>
                               <option value="Other">Lainnya / Kwitansi</option>
                           </select>
                       </div>
                   </div>
                   <div class="row" v-if="formData.hotel_amount_type == 'Other'">
                       <br>
                       <div class="col-md-12">
                           <div class="input-group">
                               <span class="input-group-addon" id="basic-addon3">Rp. </span>
                               <input type="number" class="form-control" v-model="formData.hotel_amount" name="hotel_amount" id="hotel_amount" required>
                           </div>
                           <small class="help-block">Satuan</small>
                       </div>
                   </div>
               </div>
           </div>
           <div class="col-md-6">
               <div class="row">
                   <div class="col-md-12">
                       <label for="lama" class="col-md-12 ">Lama Penginapan Hotel*</label>
                       <div class="col-md-12">
                           <div class="input-group">
                               <input type="text" v-model="formData.hotel_day_duration" name="hotel_day_duration" value="{{$mode=='edit' ? $data->hotel_day_duration : 0}}" class="form-control" required>
                               <span class="input-group-addon" id="basic-addon2">Hari</span>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <label class="col-md-12">Total Harga Hotel</label>
                       <div class="col-md-12">
                           <p class="form-control-static">
                               Rp. @{{ formData.hotel_day_duration * formData.hotel_amount }}
                           </p>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <label class="col-md-12">In Cash</label>
                       <div class="col-md-12">
                           <input type="checkbox" value="Y" name="hotel_in_cash" {{$mode=='edit' ? ($data->hotel_in_cash == 'Y' ? 'checked' : '') : ''}}>
                       </div>
                   </div>
               </div>

           </div>

       </div>
    </div>

    <div class="form-group">
        <label for="sewakendaraan" class="col-md-12">Sewa Kendaraan</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <label class="radio-inline c-radio">
                        <input v-model="formData.with_vehicle_rent" name="with_vehicle_rent"  type="radio" value="Y">
                        <span class="fa fa-circle"></span>Iya</label>
                    </label>
                    <label class="radio-inline c-radio">
                        <input  v-model="formData.with_vehicle_rent" name="with_vehicle_rent" type="radio" value="N">
                        <span class="fa fa-circle"></span>Tidak</label>
                    </label>
                </div>
                <div class="col-md-8" v-if="formData.with_vehicle_rent == 'Y'">
                    <select class="form-control" v-model="formData.vehicle_rent_type" name="vehicle_rent_type" id="vehicle_rent_type" required>
                        <option  value="">Pilih</option>
                        <option value="Car">Roda 4</option>
                        <option value="Mid Bus">Bis Sedang</option>
                        <option value="Big Bus">Bis Besar</option>
                    </select>
                </div>
            </div>
            <div class="row" v-if="formData.with_vehicle_rent == 'Y'">
                <br>
                <div class="col-md-12">
                    <select class="form-control" v-model="formData.vehicle_rent_price_id" name="vehicle_rent_price_id" id="vehicle_rent_price_id" required>
                        <option value="" >Pilih Wilayah Rental</option>
                        @foreach($data_rent_tickets as $ticket)
                            <option value="{{$ticket->id}}">{{$ticket->Provinsi()->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" v-if="formData.with_vehicle_rent == 'Y'">
        <label for="alat_angkut" class="col-md-12">Jenis Harga Sewa Kendaraan *</label>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" v-model="formData.vehicle_amount_type" name="vehicle_amount_type" id="vehicle_amount_type" required>
                        <option value="Default">Default</option>
                        <option value="Other">Lainnya / Kwitansi</option>
                    </select>
                </div>
            </div>
            <div class="row" v-if="formData.vehicle_amount_type == 'Other'">
                <br>
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Rp. </span>
                        <input type="number" class="form-control" v-model="formData.vehicle_amount" name="vehicle_amount" id="vehicle_amount" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="form-group" v-if="formData.transportation_type == 'Car' || formData.transportation_type == 'Bus'">--}}
        {{--<label for="dari" class="col-md-12">No Surat Perintah</label>--}}
        {{--<div class="col-md-12">--}}
            {{--<input class="form-control" v-model="formData.no_surat_perintah" value="{{$mode=='edit' ? $data->no_surat_perintah : ""}}" name="no_surat_perintah" type="text" id="no_surat_perintah">--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="form-group">
        <label for="dari" class="col-md-12">Kegiatan / Sub Kegiatan</label>
        <div class="col-md-12">
            {{--<input class="form-control" v-model="formData.kegiatan" value="{{$mode=='edit' ? $data->kegiatan : ""}}" name="kegiatan" type="text" id="kegiatan">--}}
            <select class="form-control" v-chosen="formData.kode_akun_id" v-model="formData.kode_akun_id" name="kode_akun_id" id="kode_akun_id" required>
                <option value="">Pilih</option>
                @foreach($data_akuns as $akun)
                    <option value="{{$akun->id}}">{{$akun->code}} ({{$akun->type}})</option>
                @endforeach
            </select>
        </div>
    </div>
    {{--<div class="form-group">--}}
        {{--<label for="dari" class="col-md-12">Sub Kegiatan</label>--}}
        {{--<div class="col-md-12">--}}
            {{--<input class="form-control" v-model="formData.sub_kegiatan" value="{{$mode=='edit' ? $data->sub_kegiatan : ""}}" name="sub_kegiatan" type="text" id="sub_kegiatan">--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="form-group">
        <label for="dari" class="col-md-12">Tanggal Surat Perintah</label>
        <div class="col-md-12">
            <div class="input-group date">
                <input type="text" v-model="formData.tgl_surat_perintah" name="tgl_surat_perintah" value="{{$mode=='edit' ? date('d-m-Y',strtotime($data->tgl_surat_perintah)) : ""}}" id="tgl_surat_perintah" class="form-control">
                <span class="input-group-addon">
                  <span class="fa fa-calendar"></span>
               </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="keterangan" class="col-md-12">Keterangan Lain-lain</label>
        <div class="col-md-12">
            <textarea class="form-control" v-model="formData.remarks" name="remarks" cols="30" rows="3" id="remarks"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="dari" class="col-md-12">No Urut pada Report</label>
        <div class="col-md-4">
            <input class="form-control" v-model="formData.urutan_report" value="{{$mode=='edit' ? $data->urutan_report : ""}}" name="urutan_report" type="number" id="urutan_report">
        </div>
    </div>
    <!-- End Isi Form Perjalanan Dinas -->
    <button type="submit" class="btn btn-primary">Save</button>
</form>

<script>
new Vue({
    el: '#formInput',
    data: {
        formData: {
            code:'',
            employee_id: 0,
            arrival_place_id: 0,
            hotel_day_duration: 0,
            type_code: "",
            pangkat_jabatan: '',
            data_hierarchies:'',
        },
        dataArrivalList:[],
        editMode: false,
        pilih_kode_lain: false
    },
    watch: {
        'formData.code': function() {
            this.selectEmployee(this.formData.code);
        },
        'formData.employee_id': function() {
            this.loadEmployee(this.formData.employee_id);
        },
        'formData.type_code': function() {
            this.loadArrivalLists();
        },
        'formData.arrival_place_id': function() {
        }
    },
    methods:{
        Submit:function(e){
            if(!this.validate()) {
                alert("Lengkapi Semua Data");
                return;
            }
        },
        loadArrivalLists:function () {
            $('.tempat-tujuan').trigger('chosen:updated');
        },
        validate:function(){
            var ret = true;
            $('form input, form textarea, form select').on('invalid', function(){;
                ret = false;
            });
            return ret;
        },
        selectEmployee:function (code_pd) {
            var self = this;
            self.formData.employee_id = 0;
            if(code_pd != "") {
                // LOAD
                return self.$http.get('{{url('/')}}'+'/administrator/official-task/api?action=get_employee_by_pd_code&code='+code_pd).then(function (response){
                    var dataEmployee = response.json().data_employee_id;
                    console.log( dataEmployee);
                    self.formData.employee_id = dataEmployee;
                },function(response) {
//                    ShowAlert('Something an Error While Loading Data!','danger');
                });
            }
        },
        loadEmployee:function () {
            var self = this;
            self.formData.pangkat_jabatan = "";
            if(self.formData.employee_id > 0) {
                // LOAD POST
                return self.$http.get('{{url('/')}}'+'/administrator/pegawai/api?action=get_employee_by_id&id='+self.formData.employee_id).then(function (response){
                    self.formData.employee_data = response.json().data_employee;
                    console.log(  self.formData.employee_data);
                    var class_employee = self.formData.employee_data.class != null ? self.formData.employee_data.class.class : '-';
                    var position_employee = self.formData.employee_data.position != null ? self.formData.employee_data.position.position : '-';
                    self.formData.pangkat_jabatan = class_employee  + ' / ' + position_employee;
                },function(response) {
//                    ShowAlert('Something an Error While Loading Data!','danger');
                });
            }
        },
    },
    computed: {
        calculatedDay:function () {
            var startDate = moment(this.formData.date_departure,'DD-MM-YYYY');
            var endDate = moment(this.formData.date_arrival,'DD-MM-YYYY');
            var diff = endDate.diff(startDate,'days');
            var ret = diff + 1 || 0;
//            this.formData.hotel_day_duration = ret;
            return ret;
        }
    },
    ready:function() {
        $('.chosen-select').chosen();
        var self = this;
        $('.date').datetimepicker({
            format: 'DD-MM-YYYY',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash'
            }
        });
        $('#date_departure').datetimepicker({
            format: 'DD-MM-YYYY',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash'
            }
        });
        $('#date_arrival').datetimepicker({
            useCurrent: false,
            format: 'DD-MM-YYYY',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash'
            }
        });
        $("#date_departure").on("dp.change", function (e) {
            $('#date_arrival').data("DateTimePicker").minDate(e.date);
        });
        $("#date_arrival").on("dp.change", function (e) {
            $('#date_departure').data("DateTimePicker").maxDate(e.date);
        });
    }
});
</script>