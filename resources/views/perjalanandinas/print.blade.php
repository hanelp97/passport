@extends('layouts.app-jakban-print')

@section('content-jakban')
    <div class="col-md-12" style="padding:15px">
        {{-- SURAT PERINTAH PERJALANAN DINAS--}}
        @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
            <div class="SuratPerintahPerjalananDinas">
                <div class="row clearfix">
                    <div class="col-xs-6 pull-left">
                        <table class="" width="100%">
                            <tr>
                                <td width="25%">KEMENTERIAN</td>
                                <td width="2%">:</td>
                                <td width="70%">{{strtoupper(Setting::name('departemen'))}}</td>
                            </tr>
                            <tr>
                                <td width="25%">Unit Kerja</td>
                                <td width="2%">:</td>
                                <td width="70%">{{Setting::name('departemen_unit')}}</td>
                            </tr>
                            <tr>
                                <td width="25%">Satuan Kerja</td>
                                <td width="2%">:</td>
                                <td width="70%">{{Setting::name('satuan_kerja')}}</td>
                            </tr>
                            <tr>
                                <td width="25%">Lokasi Kerja</td>
                                <td width="2%">:</td>
                                <td width="70%">{{Setting::name('lokasi_kerja')}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-6 pull-right">
                        <div class="text-center">
                            SPPD No {{$pd->code}}
                        </div>
                        <br><br>
                        <div class="text-center" style="border:1px solid black;line-height: 1.3">
                            {{Setting::name('bagian_kerja')}} <br>
                            {{Setting::name('dipa_tahun')}} <br>
                            Nomor  {{Setting::name('dipa_no')}} <br>
                            Tanggal  {{Setting::name('dipa_tanggal')}} <br>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h4>
                            <strong>
                                <u>
                                    SURAT PERINTAH PERJALANAN DINAS
                                </u>
                            </strong>
                        </h4>
                    </div>
                </div>
                <br>
                <div class="row padding-lr-15">
                    <div class="col-xs-12 ">
                        <table width="100%" class="table" border="1" style="font-size: 9pt">
                            <tr>
                                <td width="5%" class="text-center">1</td>
                                <td width="35%">Penjabat Berwenang yang memberi perintah</td>
                                <td width="60%" colspan="2">
                                    Kepala Balai Teknik Perkeretaapian Wilayah Jakarta & Banten
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    2.
                                </td>
                                <td>
                                    Nama Pegawai Yang Diperintahkan
                                </td>
                                <td colspan="2">
                                    {{ucfirst(@$pd->Employee()->name)}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" rowspan="4">
                                    3.
                                </td>
                                <td>
                                    a. Pangkat & Golongan
                                </td>
                                <td colspan="2">
                                    {{@$pd->Employee()->Classification()->class}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b. Jabatan
                                </td>
                                <td colspan="2">
                                    {{@$pd->Employee()->Position()->position?:'-'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c. Gaji Pokok
                                </td>
                                <td colspan="2">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d. Tingkat
                                </td>
                                <td colspan="2">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    4.
                                </td>
                                <td>
                                    Maksud perjalanan dinas
                                </td>
                                <td colspan="2">
                                    {{$pd->description}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    5.
                                </td>
                                <td>
                                    Transportasi yang dipergunakan
                                </td>
                                <td colspan="2">
                                    @if($pd->transportation_type == 'Car')
                                        Mobil
                                    @elseif($pd->transportation_type == 'Airplane')
                                        Pesawat
                                    @elseif($pd->transportation_type == 'Train')
                                        Kereta
                                    @elseif($pd->transportation_type == 'Bus')
                                        Bis
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" rowspan="2">
                                    6.
                                </td>
                                <td>
                                    a. Tempat berangkat
                                </td>
                                <td colspan="2">
                                    {{$pd->DeparturePlace()->name}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b. Tempat tujuan
                                </td>
                                <td colspan="2">
                                    {{$pd->ArrivalPlace()->name}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" rowspan="3">
                                    7.
                                </td>
                                <td>
                                    a. Lamanya perjalanan dinas
                                </td>
                                <td colspan="2">
                                    {{$pd->day_duration}} hari
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b. Tanggal berangkat
                                </td>
                                <td colspan="2">
                                    {{date('d-M-Y',strtotime($pd->date_departure))}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c. Tanggal kembali
                                </td>
                                <td colspan="2">
                                    {{date('d-M-Y',strtotime($pd->date_arrival))}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">8.</td>
                                <td>
                                    Pangkat &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; Nama
                                </td>
                                <td width="100px">Umur</td>
                                <td width="300px">Hubungan</td>
                            </tr>
                            <tr>
                                <td class="text-center"></td>
                                <td>
                                    &nbsp;
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-center" rowspan="3">
                                    9.
                                </td>
                                <td>
                                    Pembebanan anggaran :
                                </td>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    a. DIPA Nomor/Tanggal
                                </td>
                                <td colspan="2">
                                    {{Setting::name('dipa_no')}} Tgl {{Setting::name('dipa_tanggal')}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b. Kegiatan / Sub Kegiatan / Mak
                                </td>
                                <td colspan="2">
                                    {{@$pd->kegiatan}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    10.
                                </td>
                                <td>
                                    Keterangan lain-lain
                                </td>
                                <td colspan="2">
                                    {{$pd->remarks}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row clearfix" style="font-size: 10pt;">
                    <div class="col-xs-8 pull-right">
                        <table class="no-padding">
                            <tr>
                                <td style="text-align: left;padding-left:130px">
                                    Dikeluarkan di
                                </td>
                                <td width="2%">:</td>
                                <td>
                                    &nbsp;
                                    Jakarta
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;padding-left:130px">
                                    Pada Tanggal
                                    &nbsp;
                                </td>
                                <td width="2%">:</td>
                                <td>
                                    &nbsp;
                                    {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td width="2%"></td>
                                <td>
                                    <div style="margin-left: 6px;margin-top:10px">
                                        Balai Teknik Perkeretaapian <br> Wilayah Jakarta dan Banten <br>
                                        Kuasa Pengguna Anggaran
                                        <br><br><br><br> <br><br>
                                        <strong><u>{{@ucfirst(\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->name)}}</u></strong><br>
                                        {{--               {{@\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->Classification()->class}}--}}
                                        NIP.  {{@\App\Helpers\main_helper::format_nip(\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->NIP)}}
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br><br>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-xs-12" style="font-size: 10pt;">
                        {{--tembusan--}}
                        <u>Tembusan Disampaikan Kepada: </u>
                        <ol>
                            <li>
                                Bendahara pengeluaran
                            </li>
                        </ol>
                    </div>
                </div>
                <br>
            </div>
            <br>
            <div class="page-break"></div>
            <br>
        @endforeach

        {{--new page--}}
        <div class="PerincianPerhitunganBiayaPerjalananDinas">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h4 style="font-size: 14px">
                        <strong>
                            <u>
                                PERINCIAN PERHITUNGAN BIAYA PERJALANAN DINAS
                            </u>
                        </strong>
                    </h4>
                </div>
            </div>
            <br>
            <div class="row padding-lr-15">
                <div class="col-xs-12">
                    <table width="100%" class="table" border="1">
                        <thead style="font-size: 13px;">
                        <tr>
                            <th width="50%" style="padding:5px">
                                Perincian Biaya
                            </th>
                            <th width="20%">
                                Jumlah
                            </th>
                            <th width="30%">
                                Keterangan
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total_amount = 0)
                        {{--#PERJALANAN_DINAS--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->count() > 0)
                            <tr>
                                <td>
                                    Biaya transport dari tempat berangkat ke tempat tujuan :
                                    <br>
                                    {{--<div class="pull-right">--}}
                                        {{--{{$pd->day_duration}}--}}
                                    {{--</div>--}}
                                    {{--Biaya tempat berangkat ke tempat tujuan--}}
                                    <br><br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
                                        Uang Harian {{@$pd->Type()->jenis}}
                                        <div>
                                            <small class="pull-left">
                                                {{--{{$pd->DeparturePlace()->name}}--}}
                                                {{-----}}
                                                {{$pd->ArrivalPlace()->name}}
                                            </small>
                                            <div class="pull-right">
                                                {{$pd->day_duration}} x
                                                @if($pd->perjalanan_amount_type == "Other")
                                                    Rp. {{number_format($pd->type_amount)}}
                                                @else
                                                    Rp. {{number_format(round($pd->type_amount/$pd->day_duration))}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="cleafix"></div>
                                    @endforeach
                                </td>
                                <td>
                                    <br>
                                    <br>
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div class="text-right">
                                            @if($pd->perjalanan_amount_type == "Other")
                                                @php($total_amount+=$pd->type_amount*$pd->day_duration)
                                                Rp. {{number_format($pd->type_amount*$pd->day_duration)}}
                                            @else
                                                @php($total_amount+=$pd->type_amount)
                                                Rp. {{number_format($pd->type_amount)}}
                                            @endif
                                        </div>
                                        <div>&nbsp;</div>
                                    @endforeach
                                </td>
                                <td></td>
                            </tr>
                        @endif
                        {{--                       @php($baseAmount = $hotelAmount + $data->taxi_amount + $data->vehicle_amount)--}}
                        {{--#HOTEL--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_hotel','Y')->count() > 0)
                            <tr>
                                <td>
                                    Hotel
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_hotel','Y')->get() as $pd)
                                        @php($hotelAmount = $pd->hotel_day_duration > 0 ? $pd->hotel_amount / $pd->hotel_day_duration : $pd->hotel_amount)
                                        <div class="pull-left">
                                            <small>
                                                {{$pd->ArrivalPlace()->name}}
                                            </small>
                                        </div>
                                        <div>
                                            <div class="pull-right">
                                                {{$pd->hotel_day_duration}} x
                                                Rp.
                                                {{number_format($hotelAmount)}}
                                                {{--{{number_format($pd->hotel_in_cash == 'N' ? $hotelAmount : round($hotelAmount * 30 / 100)) }}--}}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    @endforeach
                                </td>
                                <td>
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_hotel','Y')->get() as $pd)
                                        <div class="text-right">
                                            @php($total_amount+=$pd->hotel_in_cash == 'N' ? $pd->hotel_amount :  $pd->hotel_amount * 30 / 100)

                                            {{--Rp. {{number_format($pd->hotel_in_cash == 'N' ? $pd->hotel_amount : $pd->hotel_in_cash_amount)}}--}}
                                            Rp. {{number_format($pd->hotel_in_cash == 'N' ? $pd->hotel_amount :  $pd->hotel_amount * 30 / 100)}}
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_hotel','Y')->get() as $pd)
                                        @if($pd->hotel_in_cash == 'Y')
                                            <div class="text-left">
                                                <small>In Cash 30%</small>
                                            </div>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        {{--#TIKET--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)
                         ->where(function($query) {
                            $query->orWhere('transportation_type', 'Train');
                            $query->orWhere('transportation_type','Airplane');
                            $query->orWhere('transportation_type','Bus');
                        })->where('employee_id',$data->employee_id)->count() > 0)
                            <tr>
                                <td>
                                    Tiket
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div>
                                            @if($pd->transportation_type == 'Airplane' || $pd->transportation_type == 'Train' )
                                                <div>
                                                    <small class="pull-left">
                                                        {{--@if($pd->transportation_type == 'Airplane')--}}
                                                        {{--Pesawat--}}
                                                        {{--@elseif($pd->transportation_type == 'Train')--}}
                                                        {{--Kereta--}}
                                                        {{--@endif--}}
                                                    </small>
                                                    @if($pd->transportation_amount_type == 'Default')
                                                        <div class="pull-right">
                                                            Rp. {{number_format($pd->transportation_amount) }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>
                                                @if($pd->transportation_amount_type == 'Other')
                                                    @if($pd->transportation_amount_type_other == 'Non-PP')
                                                        <div class="pull-left">
                                                            <div>
                                                                <small>
                                                                    {{$pd->DeparturePlace()->name}}
                                                                </small>
                                                            </div>
                                                            <div>
                                                                <small>
                                                                    {{$pd->ArrivalPlace()->name}}
                                                                </small>
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                            <div>
                                                                {{$pd->transportation_amount_qty}} x
                                                                Rp. {{number_format($pd->transportation_amount) }}
                                                            </div>
                                                            <div>
                                                                {{$pd->transportation_amount_qty_2}} x
                                                                Rp. {{number_format($pd->transportation_amount_2) }}
                                                            </div>
                                                        </div>
                                                    @elseif($pd->transportation_amount_type_other == 'PP')
                                                        <div class="pull-left">
                                                            <div>
                                                                <small>
                                                                    {{$pd->DeparturePlace()->name}}
                                                                    -
                                                                    {{$pd->ArrivalPlace()->name}}
                                                                </small>
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                            <div>
                                                                Rp. {{number_format($pd->transportation_amount) }}
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="cleafix"></div>
                                            @endif
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div>
                                            @if($pd->transportation_amount_type == 'Default')
                                                @php($total_amount+=$pd->transportation_amount)
                                                <div class="text-right">
                                                    Rp. {{number_format($pd->transportation_amount)}}
                                                </div>
                                            @endif
                                            @if($pd->transportation_amount_type == 'Other')
                                                @if($pd->transportation_amount_type_other == 'Non-PP')
                                                    @php($total_amount+=($pd->transportation_amount * $pd->transportation_amount_qty) + ($pd->transportation_amount_2 * $pd->transportation_amount_qty_2))
                                                    <div class="pull-right">
                                                        <div>
                                                            Rp. {{number_format($pd->transportation_amount * $pd->transportation_amount_qty)}}
                                                        </div>
                                                        <div>
                                                            Rp. {{number_format($pd->transportation_amount_2 * $pd->transportation_amount_qty_2)}}
                                                        </div>
                                                    </div>
                                                @elseif($pd->transportation_amount_type_other == 'PP')
                                                    @php($total_amount+=($pd->transportation_amount))
                                                    <div class="pull-right">
                                                        <div>
                                                            Rp. {{number_format($pd->transportation_amount)}}
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    <br>
                                    {{--@foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)--}}
                                    {{--<br>--}}
                                    {{--@if($pd->transportation_amount_type == 'Default')--}}
                                    {{--<div class="text-right">--}}
                                    {{--</div>--}}
                                    {{--@endif--}}
                                    {{--<div>--}}
                                    {{--<small>--}}
                                    {{--{{$pd->DeparturePlace()->name}}--}}
                                    {{-----}}
                                    {{--{{$pd->ArrivalPlace()->name}}--}}
                                    {{--</small>--}}
                                    {{--</div>--}}
                                    {{--@endforeach--}}
                                </td>
                            </tr>
                        @endif
                        {{--#TRANSPORT--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)
                         ->where(function($query) {
                            $query->orWhere('transportation_type', 'Car');
                        })->where('employee_id',$data->employee_id)->count() > 0 ||
                        \App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->count() > 0)
                        <tr>
                            <td>
                                BBM & Tol
                                <br>
                                {{--CAR--}}
                                @if(\App\PerjalananDinas::where('code',$data->code)
                                     ->where(function($query) {
                                        $query->orWhere('transportation_type', 'Car');
                                    })->where('employee_id',$data->employee_id)->count() > 0)
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)
                                          ->where(function($query) {
                                              $query->orWhere('transportation_type', 'Car');
                                          })
                                        ->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div>
                                            <br>
                                            <small class="pull-left">
                                                &nbsp;
                                                {{--@if($pd->transportation_type == 'Car')--}}
                                                    {{--BBM & Tol (Mobil)--}}
                                                {{--@elseif($pd->transportation_type == 'Airplane')--}}
                                                    {{--Tiket (Pesawat)--}}
                                                {{--@elseif($pd->transportation_type == 'Train')--}}
                                                    {{--Tiket (Kereta)--}}
                                                {{--@elseif($pd->transportation_type == 'Bus')--}}
                                                    {{--Bis--}}
                                                {{--@endif--}}
                                            </small>
                                            @if($pd->transportation_amount_type == 'Default')
                                                <div class="pull-right">
                                                    Rp. {{number_format($pd->transportation_amount) }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                        @if($pd->transportation_amount_type == 'Other')
                                            @if($pd->transportation_amount_type_other == 'Non-PP')
                                                <div class="pull-left">
                                                    <div>
                                                        <small>
                                                            {{$pd->DeparturePlace()->name}}
                                                        </small>
                                                    </div>
                                                    <div>
                                                        <small>
                                                            {{$pd->ArrivalPlace()->name}}
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div>
                                                        {{$pd->transportation_amount_qty}} x
                                                        Rp. {{number_format($pd->transportation_amount) }}
                                                    </div>
                                                    <div>
                                                        {{$pd->transportation_amount_qty_2}} x
                                                        Rp. {{number_format($pd->transportation_amount_2) }}
                                                    </div>
                                                </div>
                                            @elseif($pd->transportation_amount_type_other == 'PP')
                                                <div class="pull-left">
                                                    <div>
                                                        <small>
                                                            {{$pd->DeparturePlace()->name}}
                                                            -
                                                            {{$pd->ArrivalPlace()->name}}
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div>
                                                        Rp. {{number_format($pd->transportation_amount) }}
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                        <div class="cleafix"></div>
                                    @endforeach
                                @endif

                                {{--TAXI--}}
                                @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->count() > 0)
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->get() as $pd)
                                        {{--TAXI DEPARUTE--}}
                                        <div class="pull-left">
                                            <small>
                                                {{$pd->DeparturePlace()->name}}
                                                -
                                                Bandara / Stasiun (PP)
                                            </small>
                                        </div>

                                        <div class="pull-right">
                                            {{$pd->taxi_qty_departure}} x
                                            Rp.
                                            {{number_format($pd->taxi_qty_departure > 1 ? $pd->taxi_amount_departure / $pd->taxi_qty_departure : $pd->taxi_amount_departure)}}
                                        </div>
                                        <div class="clearfix"></div>

                                        {{--TAXI ARRIVAL--}}
                                        <div class="pull-left">
                                            <small>
                                                {{$pd->ArrivalPlace()->name}}
                                                -
                                                 Bandara / Hotel / <br> Stasiun (PP)
                                            </small>
                                        </div>

                                        <div class="pull-right">
                                            {{$pd->taxi_qty_departure}} x
                                            Rp.
                                            {{number_format($pd->taxi_qty_arrival > 1 ? $pd->taxi_amount_arrival / $pd->taxi_qty_arrival : $pd->taxi_amount_arrival)}}
                                        </div>
                                        <div class="clearfix"></div>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <br>
                                {{--Car--}}
                                @foreach(\App\PerjalananDinas::where('code',$data->code)
                                ->where(function($query) {
                                    $query->orWhere('transportation_type', 'Car');
                                })
                                ->where('employee_id',$data->employee_id)->get() as $pd)
                                    <br>
                                    @if($pd->transportation_amount_type == 'Default')
                                        @php($total_amount+=$pd->transportation_amount)
                                        <div class="text-right">
                                            Rp. {{number_format($pd->transportation_amount)}}
                                        </div>
                                    @endif
                                    @if($pd->transportation_amount_type == 'Other')
                                        <br>
                                        @if($pd->transportation_amount_type_other == 'Non-PP')
                                            @php($total_amount+=($pd->transportation_amount * $pd->transportation_amount_qty) + ($pd->transportation_amount_2 * $pd->transportation_amount_qty_2))
                                            <div class="pull-right">
                                                <div>
                                                    Rp. {{number_format($pd->transportation_amount * $pd->transportation_amount_qty)}}
                                                </div>
                                                <div>
                                                    Rp. {{number_format($pd->transportation_amount_2 * $pd->transportation_amount_qty_2)}}
                                                </div>
                                            </div>
                                        @elseif($pd->transportation_amount_type_other == 'PP')
                                            @php($total_amount+=($pd->transportation_amount))
                                            <div class="pull-right">
                                                <div>
                                                    Rp. {{number_format($pd->transportation_amount)}}
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                    <div class="clearfix"></div>
                                @endforeach

                                {{--Taxi--}}
                                @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->count() > 0)
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)
                                        {{--TAXI DEPARTURE--}}
                                        <div class="text-right">
                                            @php($total_amount+=$pd->taxi_amount_departure)
                                            Rp. {{number_format($pd->taxi_amount_departure)}}
                                        </div>

                                        {{--TAXI ARRIVAL--}}
                                        <div class="text-right">
                                            @php($total_amount+=$pd->taxi_amount_arrival)
                                            Rp. {{number_format($pd->taxi_amount_arrival)}}
                                        </div>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <br>
                                {{--@foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->get() as $pd)--}}
                                {{--<br>--}}
                                {{--@if($pd->transportation_amount_type == 'Default')--}}
                                {{--<div class="text-right">--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--<div>--}}
                                {{--<small>--}}
                                {{--{{$pd->DeparturePlace()->name}}--}}
                                {{-----}}
                                {{--{{$pd->ArrivalPlace()->name}}--}}
                                {{--</small>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            </td>
                        </tr>
                        @endif
                        {{--#SEWAKENDARAN--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('with_vehicle_rent','Y')->where('employee_id',$data->employee_id)->count() > 0)
                            <tr>
                                <td>
                                    Sewa Kendaraan
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('with_vehicle_rent','Y')->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div class="text-right">
                                            Rp. {{number_format($pd->vehicle_amount) }}
                                        </div>
                                    @endforeach
                                </td>
                                <td style="vertical-align: bottom;">
                                    <br>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('with_vehicle_rent','Y')->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div class="text-right">
                                            @php($total_amount+=$pd->vehicle_amount)

                                            Rp. {{number_format($pd->vehicle_amount)}}
                                        </div>
                                    @endforeach
                                </td>
                                <td style="vertical-align: bottom;">
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('with_vehicle_rent','Y')->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div>
                                           <small>
                                               {{$pd->DeparturePlace()->name}}
                                               -
                                               {{$pd->ArrivalPlace()->name}}
                                           </small>
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td class="text-right">
                                <strong>
                                    Total Bayar
                                </strong>
                            </td>
                            <td>
                                <div class="text-right">
                                    {{--Rp. {{number_format(--}}
           {{--\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->sum('total_amount')--}}
       {{--)}}--}}
                                    Rp. {{number_format($total_amount)}}
                                </div>
                            </td>
                            <td>
                                {{--{{\App\Helpers\moneyFormat::terbilang((int)--}}
                                     {{--\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->sum('total_amount')--}}

                                {{--)}} rupiah--}}

                                {{\App\Helpers\moneyFormat::terbilang((int)$total_amount)}} rupiah
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <br>
                <div class="row clearfix" style="font-size: 10pt;">
                    <div class="col-xs-6 pull-left">
                        Telah dibayar sejumlah: <br>
                        Rp. {{number_format($total_amount)}}
                        <br><br>
                        <small>
                            {{\App\Helpers\moneyFormat::terbilang((int)$total_amount)}} rupiah
                        </small>
                    </div>
                    <div class="col-xs-6 pull-right">
                        Telah menerima uang sebesar: <br>
                        Rp. {{number_format($total_amount)}}
                        <br><br>
                        <small>
                            {{\App\Helpers\moneyFormat::terbilang((int)$total_amount)}} rupiah
                        </small>
                    </div>
                </div>
                <br><br><br><br>
                <div class="row clearfix" style="font-size: 10pt;">
                    <div class="col-xs-6 pull-left">
                        <div class="pull-right" style="margin-right: 100px;">
                            Jakarta &nbsp; {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}
                            <br><br>
                            Bendahara pengeluaran
                            <br><br><br><br>
                            <br><br><br><br>
                            <strong><u>{{@ucfirst(\App\User::find(Setting::name('bendahara_pengeluaran'))->name)}}</u></strong><br>
                            {{--                       {{@\App\User::find(Setting::name('bendahara_pengeluaran'))->Classification()->class}}--}}
                            {{@\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP!="" ? "NIP. " . @\App\Helpers\main_helper::format_nip(\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP) : ""}}
                        </div>
                    </div>
                    <div class="col-xs-6 pull-right">
                        <div class="pull-left" style="margin-left: 100px;">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            Yang berpergian
                            <br><br><br><br>
                            <br><br><br><br>
                            <strong><u>{{ucfirst($data->Employee()->name)}}</u></strong><br>
                            {{--                       {{@$data->Employee()->Classification()->class}}--}}
                            {{@$data->Employee()->NIP != "" ? "NIP." .  @\App\Helpers\main_helper::format_nip($data->Employee()->NIP) : ""}}
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="page-break"></div>
            <br><br>
        </div>

        {{--new page--}}
        <br>
        <div class="row">
            <div class="col-xs-12 text-center">
                <h4 style="font-size:16px">
                    <strong>
                        <u>
                            TANDA BUKTI PEMBAYARAN
                        </u>
                    </strong>
                </h4>
            </div>
        </div>
        <br>
        <br><br>
        <div class="row clearfix">
            <div class="col-xs-6 pull-right">
                <div class="text-center" style="border:1px solid black;line-height: 1.5;">
                    {{Setting::name('bagian_kerja')}} <br>
                    {{Setting::name('dipa_tahun')}} <br>
                    Nomor  {{Setting::name('dipa_no')}} <br>
                    Tanggal  {{Setting::name('dipa_tanggal')}} <br>
                </div>
            </div>
        </div>
        <br><br><br>
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <table width="100%" class="table" style="font-size: 10pt;">
                    <tr>
                        <td width="35%">Telah Terima Dari</td>
                        <td width="60%">
                            Kuasa Pengguna Anggaran Balai Teknik Perkeretaapian Kelas I Wilayah Jakarta & Banten
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sejumlah
                        </td>
                        <td>

                            <strong>#
                                {{\App\Helpers\moneyFormat::terbilang((int)
                                        \App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->sum('total_amount')

                                   )}} rupiah
                                #</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Untuk biaya
                        </td>
                        <td>
                            {{$data->description}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Jumlah
                        </td>
                        <td>
                            <strong>
                                Rp.
                                {{number_format(
                                \App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->sum('total_amount')
                              )}}
                            </strong>
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
        </div>
        <br>

        <div class="row clearfix"style="font-size: 10pt;" >
            <div class="col-xs-4 pull-left">
                <table>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                SETUJU DIBAYAR <br>
                Penjabat Pembuat Komitmen <br> Kegiatan Rutin
                <br><br><br><br><br><br><br><br>
                <strong><u>{{@ucfirst(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->name)}}</u></strong><br>
                {{--               {{@\App\User::find(Setting::name('kuasa_pengguna_anggaran'))->Classification()->class}}--}}
                {{@\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP!="" ? "NIP. " . @\App\Helpers\main_helper::format_nip(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP) : ""}}
            </div>
            <div class="col-xs-4 pull-left" style="margin-left: 40px;">
                <table>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                LUNAS DIBAYAR <br>
                Bendahara pengeluaran
                <br><br><br><br><br><br><br><br><br>
                <strong><u>{{@ucfirst(\App\User::find(Setting::name('bendahara_pengeluaran'))->name)}}</u></strong><br>
                {{--               {{@\App\User::find(Setting::name('bendahara_pengeluaran'))->Classification()->class}}--}}
                {{@\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP!="" ? "NIP. " . @\App\Helpers\main_helper::format_nip(\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP) : ""}}
            </div>
            <div class="col-xs-3 pull-right">
                <br><br>
                {{--               Jakarta &nbsp; {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}--}}
                <br>
                Penerima
                <br><br><br><br><br><br><br><br><br>
                <strong><u>{{ucfirst($data->Employee()->name)}}</u></strong><br>
                {{--               {{@$data->Employee()->Classification()->class}}--}}
                {{@$data->Employee()->NIP != "" ? "NIP." .  @\App\Helpers\main_helper::format_nip($data->Employee()->NIP) : ""}}
            </div>
        </div>

        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->count() > 0 ||
        \App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_vehicle_rent','Y')->count() > 0 ||
        \App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('transportation_type','Car')->count() > 0 )
            <br>
            <div class="page-break"></div>
            {{--new page--}}
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3>
                        <strong>
                            <u>
                                DAFTAR PENGELUARAN RIIL
                            </u>
                        </strong>
                    </h3>
                </div>
            </div>
            <br><br><br>
            <div class="row" style="font-size: 10pt;">
                <div class="col-xs-12">
                    Yang bertanda tangan dibawah ini:
                    <hr>
                    <table width="100%" class="table">
                        <tr>
                            <td width="35%">Nama</td>
                            <td width="60%">
                                {{$data->Employee()->name}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                NIP
                            </td>
                            <td>
                                {{@$data->Employee()->NIP != "" ? "" .  @\App\Helpers\main_helper::format_nip($data->Employee()->NIP) : ""}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Jabatan
                            </td>
                            <td>
                                {{@$data->Employee()->Position()->position?:'-'}}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    Berdasarkan Surat Perintah Pelaksanaan Tugas (SPPT) tanggal {{@date('d',strtotime($data->tgl_surat_perintah))}} {{@\App\Helpers\main_helper::indonesiaDate(date('n', strtotime($data->tgl_surat_perintah)),"month")}} {{@date('Y', strtotime($data->tgl_surat_perintah))}}
                    Nomor {{$data->no_surat_perintah}} dengan ini kami menyatakan dengan sesungguhnya bahwa: <br>
                    <br>
                    Biaya transport pegawai yang dibawah ini yang tidak dapat diperoleh bukti-bukti pengeluarannya, meliputi:
                    <br>
                    <br>
                    <table class="table" width="100%" border="1" style="font-size: 10pt;">
                        <thead>
                        <tr>
                            <th width="3%">No.</th>
                            <th>Uraian</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>
                        @php($no = 1)
                        <tr>
                            <td>{{$no}}</td>
                            <td>
                                Biaya Transportasi
                            </td>
                            <td></td>
                        </tr>
                        @php($total_amount = 0)
                        {{--Car--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('transportation_type','Car')->count() > 0 )
                            <tr>
                                <td>-</td>
                                <td>
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)
                                              ->where(function($query) {
                                                  $query->orWhere('transportation_type', 'Car');
                                              })
                                            ->where('employee_id',$data->employee_id)->get() as $pd)
                                        <div>
                                            <small class="pull-left">
                                                {{--@if($pd->transportation_type == 'Car')--}}
                                                {{--BBM & Tol (Mobil)--}}
                                                {{--@elseif($pd->transportation_type == 'Airplane')--}}
                                                {{--Tiket (Pesawat)--}}
                                                {{--@elseif($pd->transportation_type == 'Train')--}}
                                                {{--Tiket (Kereta)--}}
                                                {{--@elseif($pd->transportation_type == 'Bus')--}}
                                                {{--Bis--}}
                                                {{--@endif--}}
                                            </small>
                                            @if($pd->transportation_amount_type == 'Default')
                                                <div class="pull-right">
                                                    Rp. {{number_format($pd->transportation_amount) }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                        @if($pd->transportation_amount_type == 'Other')
                                            @if($pd->transportation_amount_type_other == 'Non-PP')
                                                <div class="pull-left">
                                                    <div>
                                                        <small>
                                                            {{$pd->DeparturePlace()->name}}
                                                        </small>
                                                    </div>
                                                    <div>
                                                        <small>
                                                            {{$pd->ArrivalPlace()->name}}
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div>
                                                        {{$pd->transportation_amount_qty}} x
                                                        {{number_format($pd->transportation_amount) }}
                                                    </div>
                                                    <div>
                                                        {{$pd->transportation_amount_qty_2}} x
                                                        {{number_format($pd->transportation_amount_2) }}
                                                    </div>
                                                </div>
                                            @elseif($pd->transportation_amount_type_other == 'PP')
                                                <div class="pull-left">
                                                    <div>
                                                        <small>
                                                            {{$pd->DeparturePlace()->name}}
                                                            -
                                                            {{$pd->ArrivalPlace()->name}} (PP)
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div>
                                                        Rp. {{number_format($pd->transportation_amount) }}
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                        <div class="cleafix"></div>
                                    @endforeach
                                </td>

                                <td>
                                    {{--Car--}}
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)
                                    ->where(function($query) {
                                        $query->orWhere('transportation_type', 'Car');
                                    })
                                    ->where('employee_id',$data->employee_id)->get() as $pd)
                                        @if($pd->transportation_amount_type == 'Default')
                                            @php($total_amount+=$pd->transportation_amount)
                                            <div class="text-left">
                                                Rp. {{number_format($pd->transportation_amount)}}
                                            </div>
                                        @endif
                                        @if($pd->transportation_amount_type == 'Other')
                                            @if($pd->transportation_amount_type_other == 'Non-PP')
                                                @php($total_amount+=($pd->transportation_amount * $pd->transportation_amount_qty) + ($pd->transportation_amount_2 * $pd->transportation_amount_qty_2))
                                                <div class="pull-right">
                                                    <div>
                                                        Rp. {{number_format($pd->transportation_amount * $pd->transportation_amount_qty)}}
                                                    </div>
                                                    <div>
                                                        Rp. {{number_format($pd->transportation_amount_2 * $pd->transportation_amount_qty_2)}}
                                                    </div>
                                                </div>
                                            @elseif($pd->transportation_amount_type_other == 'PP')
                                                @php($total_amount+=($pd->transportation_amount))
                                                <div class="pull-left">
                                                    <div>
                                                        Rp. {{number_format($pd->transportation_amount)}}
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                        <div class="clearfix"></div>
                                    @endforeach
                                </td>
                            </tr>
                            @php($no++)
                        @endif
                        {{--Vehicle Rent--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_vehicle_rent','Y')->count() > 0 )
                            <tr>
                                <td>{{$no}}</td>
                                <td>
                                    Biaya Sewa Kendaraan
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_vehicle_rent','Y')->get() as $pd)
                                        <br>
                                        <small>
                                            {{@$pd->VehicleRentFee() ? @$pd->VehicleRentFee()->Provinsi()->nama : '-'}}
                                        </small>
                                        <br>
                                    @endforeach
                                </td>
                                {{--<td style="vertical-align: bottom;">--}}
                                    {{--@foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_vehicle_rent','Y')->get() as $pd)--}}
                                        {{--<br>--}}
                                        {{--Rp. {{number_format($pd->vehicle_amount)}}--}}
                                        {{--<br>--}}
                                    {{--@endforeach--}}
                                {{--</td>--}}
                                <td style="vertical-align: bottom;">
                                    @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_vehicle_rent','Y')->get() as $pd)
                                        <br>
                                        Rp. {{number_format($pd->vehicle_amount)}}
                                        @php($total_amount+=$pd->vehicle_amount)
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                            @php($no++)
                        @endif
                        {{--Taxi--}}
                        @if(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->count() > 0 )
                            @foreach(\App\PerjalananDinas::where('code',$data->code)->where('employee_id',$data->employee_id)->where('with_taxi','Y')->get() as $pd)
                                {{--TAXI DEPARUTE--}}
                                <tr>
                                    <td>-</td>
                                    <td>
                                        <div class="pull-left">
                                          <small>
                                              {{$pd->DeparturePlace()->name}} -
                                              Bandara / Stasiun (PP)
                                          </small>
                                        </div>
                                        <div class="pull-right">
                                            {{$pd->taxi_qty_departure}} x
                                            Rp.
                                            {{number_format($pd->taxi_qty_departure > 1 ? $pd->taxi_amount_departure / $pd->taxi_qty_departure : $pd->taxi_amount_departure)}}
                                        </div>
                                        <div class="clearfix"></div>
                                    </td>
                                    <td>
                                        {{--TAXI DEPARTURE--}}
                                        <div class="text-left">
                                            @php($total_amount+=$pd->taxi_amount_departure)
                                            Rp. {{number_format($pd->taxi_amount_departure)}}
                                        </div>
                                    </td>
                                </tr>
                                {{--TAXI ARRIVAL--}}
                                <tr>
                                    <td>-</td>
                                    <td>
                                        {{--TAXI ARRIVAL--}}
                                        <div class="pull-left">
                                            <small>
                                                {{$pd->ArrivalPlace()->name}}
                                                -
                                                Bandara / Hotel / <br> Stasiun (PP)
                                            </small>
                                        </div>

                                        <div class="pull-right">
                                            {{$pd->taxi_qty_departure}} x
                                            Rp.
                                            {{number_format($pd->taxi_qty_arrival > 1 ? $pd->taxi_amount_arrival / $pd->taxi_qty_arrival : $pd->taxi_amount_arrival)}}
                                        </div>
                                        <div class="clearfix"></div>
                                    </td>
                                    <td>
                                        {{--TAXI ARRIVAL--}}
                                        <div class="text-left">
                                            @php($total_amount+=$pd->taxi_amount_arrival)
                                            Rp. {{number_format($pd->taxi_amount_arrival)}}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            @php($no++)
                        @endif
                        <tr>
                            <td colspan="2" class="text-right">
                                <b>Total</b>
                            </td>
                            <td>
                                Rp. {{number_format($total_amount)}}
                            </td>
                        </tr>
                    </table>
                    <br>
                    Jumlah uang tersebut pada angka diatas benar-benar di keluarkan untuk pelaksanaan perjalanan dinas dimaksud dan apabila di kemudian hari terdapat
                    kelebihan atas pembayaran, kami bersedia untuk menyetorkan kelebihan tersebut ke Kas Negara.
                    <br>
                    <br>
                    Demikian pernyataan ini kami buat sebenar-benarnya, untuk dipergunakan sebagaimana semestinya.
                    <br>
                    <br>
                    <br>
                </div>

            </div>
            <br>
            <div class="row clearfix" style="font-size: 10pt;">
                <div class="col-xs-4 pull-left">
                    <br>
                    <br>
                    Penjabat Pembuat Komitmen
                    <br><br><br><br>
                    <br><br><br><br>
                    <strong><u>{{@ucfirst(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->name)}}</u></strong><br>
                    {{--                   {{@\App\User::find(Setting::name('penjabat_pembuat_komitment'))->Classification()->class}}--}}
                    {{@\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP!="" ? "NIP. " . @\App\Helpers\main_helper::format_nip(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP) : ""}}
                </div>
                <div class="col-xs-4 pull-left">
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="col-xs-4 pull-right">
                    <br>
                    Jakarta, &nbsp; {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}
                    <br>
                    Yang Melakukan Perjalanan Dinas
                    <br><br><br><br>                   <br><br><br><br>
                    <strong><u>{{ucfirst($data->Employee()->name)}}</u></strong><br>
                    {{--                   {{@$data->Employee()->Classification()->class}}--}}
                    {{@$data->Employee()->NIP != "" ? "NIP." . @\App\Helpers\main_helper::format_nip($data->Employee()->NIP) : ""}}
                    <br>
                </div>
            </div>
        @endif
    </div>
@endsection