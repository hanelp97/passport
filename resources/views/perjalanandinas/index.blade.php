@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <h3>Biaya Perjalanan Dinas
            <small>Management</small>
        </h3>
        @if(Session::has('success_message'))
            <div class="alert alert-success text-center">
                {{Session::get('success_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @elseif(Session::has('error_message'))
            <div class="alert alert-danger text-center">
                {{Session::get('error_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @endif
        <div class="mb-lg clearfix">
            <div class="pull-left">
                <button @click.prevent="Create" class="btn btn-lg btn-info"><strong>Tambah Data Perjalanan Dinas</strong></button>
            </div>
            <div class="pull-right">
                <!-- <p class="mb0 mt-sm">20 Perjalanan Dinas</p> -->
                <a href="{{ route('SettingOfficial::Index') }}" title="Setting Data Perjalanan Dinas">
                    <button type="button" class="btn btn-sm btn-inverse">
                        <em class="fa fa-gear"></em> Setting Data Perjalanan Dinas
                    </button>
                </a>
            </div>
        </div>

        <!-- START panel tab-->
        <div role="tabpanel" class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-6 pull-left">
                        Page {{$data_perjalanan_dinas->currentPage()}} of {{$data_perjalanan_dinas->lastPage()}}
                    </div>
                    <div class="col-lg-6 pull-right text-right">
                        <div style="margin-top: -20px">
                            {{$data_perjalanan_dinas->appends([
                             'status'=>Request::get('status'),
                             'sortby'=>Request::get('sortby'),
                             'order'=>Request::get('order'),
                             'q'=>Request::get('q')
                         ])->render()}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Nav tabs-->
            <ul role="tablist" class="nav nav-tabs nav-justified">
                <li role="presentation" class="active">
                    <a href="#data" aria-controls="data" role="tab" data-toggle="tab">
                        <em class="fa fa-clock-o fa-fw"></em>Data Perjalanan Dinas</a>
                </li>
                <li role="presentation">
                    <a href="#daftar" aria-controls="daftar" role="tab" data-toggle="tab">
                        <em class="fa fa-money fa-fw"></em>Daftar Pembayaran Perjalanan Dinas</a>
                </li>
                <li role="presentation">
                    <a href="#daftar-nama" aria-controls="daftar-nama" role="tab" data-toggle="tab">
                        <em class="fa fa-user fa-fw"></em>Daftar Nama</a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content p0">
                <div id="data" role="tabpanel" class="tab-pane active">
                    <!-- START list group-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="well">
                                <form action="" method="get">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label for="" class="control-label">No Awal</label>
                                            <input type="text" name="no_awal" id="no_awalno" class="form-control" value="{{Request::get('no_awal')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="" class="control-label">No Akhir</label>
                                            <input type="text" name="no_akhir" id="no_akhir" class="form-control" value="{{Request::get('no_akhir')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="" class="control-label">Dari</label>
                                            <input type="text" name="date_departure" id="date_departure" class="form-control" value="{{Request::get('date_departure')}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="" class="control-label">Sampai</label>
                                            <input type="text" name="date_arrival" id="date_arrival" class="form-control" value="{{Request::get('date_arrival')}}">
                                        </div>
                                        <div class="col-md-2">
                                            <label for="" class="control-label">&nbsp;</label><br>
                                            <button class="btn btn-primary"><i class="fa fa-search"></i> &nbsp; Cari</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <div class="table-responsive">
                                <table id="datatable1" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama</th>
                                        <th>Maksud Perjalanan Dinas</th>
                                        <th>Tujuan</th>
                                        <th>Lama</th>
                                        <th>Total Biaya</th>
                                        <th>Print</th>
                                        {{--<th>Export PDF</th>--}}
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data_perjalanan_dinas->count() == 0)
                                        <tr>
                                            <td colspan="10">
                                                <div class="alert alert-warning text-center">
                                                    Data Empty
                                                </div>
                                            </td>
                                        </tr>
                                    @endif

                                    @foreach($data_perjalanan_dinas as $perjalanan_dinas)
                                        <tr>
                                            <td>{{$perjalanan_dinas->code}}</td>
                                            <td>{{date('d-M-Y',strtotime($perjalanan_dinas->date_departure))}} s/d {{date('d-M-Y',strtotime($perjalanan_dinas->date_arrival))}}</td>
                                            <td>{{@$perjalanan_dinas->Employee()->name}}</td>
                                            <td>{{$perjalanan_dinas->description}}</td>
                                            <td>{{@$perjalanan_dinas->ArrivalPlace()->name}}</td>
                                            <td>{{$perjalanan_dinas->day_duration}} hari</td>
                                            <td>Rp. {{number_format($perjalanan_dinas->total_amount)}}</td>
                                            <td>
                                                <a target="_blank" href="{{route('OfficialTask::Report',['id'=>$perjalanan_dinas->id])}}?type=surat_perintah" class="btn btn-sm btn-default"><em class="fa fa-print"></em></a>
                                            </td>
                                            {{--<td>--}}
                                                {{--<a target="_blank" href="{{route('OfficialTask::Report',['id'=>$perjalanan_dinas->id])}}?type=surat_perintah&output=pdf" class="btn btn-sm btn-default"><em class="fa fa-file-pdf-o"></em></a>--}}
                                            {{--</td>--}}
                                            <td class="text-center">
                                                <button type="button" @click.prevent="Edit({{$perjalanan_dinas->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                <button type="button" @click.prevent="Delete({{$perjalanan_dinas->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END list group-->
                    <!-- <div class="panel-footer text-right"><a href="#" class="btn btn-default btn-sm">View All Activity </a>
                    </div> -->
                </div>
                <div id="daftar" role="tabpanel" class="tab-pane">
                    {{--<div class="panel-footer text-right"><a href="#" class="btn btn-default btn-sm"><em class="fa fa-file-pdf-o"></em> Export Semua Daftar</a>--}}
                    {{--</div>--}}
                    <!-- START table responsive-->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Tujuan</th>
                                <th>No. Surat Perintah</th>
                                <th>Jumlah Dibayarkan</th>
                                <th>Print</th>
                                <th>Export PDF</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                            @php($data_pembayaran_dinas = \App\PerjalananDinas::get()->groupBy('no_surat_perintah','arrival_place_id','date_departure','date_arrival','day_duration'))
                            @foreach($data_pembayaran_dinas as $perjalanan_dinas)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{date('d-M-Y',strtotime($perjalanan_dinas->first()->date_departure))}} s/d {{date('d-M-Y',strtotime($perjalanan_dinas->first()->date_arrival))}}</td>
                                    <td>{{@$perjalanan_dinas->first()->ArrivalPlace()->name}}</td>
                                    <td>{{@$perjalanan_dinas->first()->no_surat_perintah}}</td>
                                    <td>Rp. {{number_format($perjalanan_dinas->sum('total_amount'))}}</td>
                                    <td><a href="{{route('OfficialTask::Report')}}?type=nominatif&date_departure={{$perjalanan_dinas->first()->date_departure}}&date_arrival={{$perjalanan_dinas->first()->date_arrival}}&departure_place_id={{$perjalanan_dinas->first()->departure_place_id}}&arrival_place_id={{$perjalanan_dinas->first()->arrival_place_id}}&day_duration={{$perjalanan_dinas->first()->day_duration}}&no_surat_perintah={{$perjalanan_dinas->first()->no_surat_perintah}}" target="_blank" class="btn btn-sm btn-default"><em class="fa fa-print"></em></a></td>
                                    <td><a href="{{route('OfficialTask::Report')}}?type=nominatif&output=pdf&date_departure={{$perjalanan_dinas->first()->date_departure}}&date_arrival={{$perjalanan_dinas->first()->date_arrival}}&departure_place_id={{$perjalanan_dinas->first()->departure_place_id}}&arrival_place_id={{$perjalanan_dinas->first()->arrival_place_id}}&day_duration={{$perjalanan_dinas->first()->day_duration}}" target="_blank" class="btn btn-sm btn-default"><em class="fa fa-file-pdf-o"></em></a></td>
                                </tr>
                                @php($no++)
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- END table responsive-->

                </div>
                <div id="daftar-nama" role="tabpanel" class="tab-pane">
                    {{--<div class="panel-footer text-right"><a href="#" class="btn btn-default btn-sm"><em class="fa fa-file-pdf-o"></em> Export Semua Daftar</a>--}}
                    {{--</div>--}}
                    <!-- START table responsive-->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jumlah Perjalanan Dinas</th>
                                <th>Total Seluruh Biaya Perjalanan Dinas</th>
                                {{--<th>Jumlah Perjalanan Dinas</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                            @php($data_employees = \App\User::with(['PerjalananDinas'=>function($q){
                                  $q->orderBy('total_amount','desc');
                            }])->get())
                            @foreach($data_employees as $employee)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$employee->name}}</td>
                                    <td>{{@$employee->PerjalananDinas() ? @$employee->PerjalananDinas()->count() : 0}} kali</td>
                                    <td>Rp. {{@$employee->PerjalananDinas() ? @number_format($employee->PerjalananDinas()->sum('total_amount')) : 0}},-</td>
                                </tr>
                                @php($no++)
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- END table responsive-->

                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-6">
                        Page {{$data_perjalanan_dinas->currentPage()}} of {{$data_perjalanan_dinas->lastPage()}}
                    </div>
                    <div class="col-lg-6 text-right">
                        {{$data_perjalanan_dinas->appends([
                             'status'=>Request::get('status'),
                             'sortby'=>Request::get('sortby'),
                             'order'=>Request::get('order'),
                             'q'=>Request::get('q')
                         ])->render()}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END panel tab-->
    </template>


@endsection

@section('scripts-jakban')
    <script type="text/javascript" src="{{url('vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <style>
        #form-folder .modal-dialog  {
            width: 1000px;
        }
    </style>
    <!-- CHOSEN-->
    <script src="{{url('vendor/chosen_v1.2.0/chosen.jquery.min.js')}}"></script>
    <script>
        $('.chosen-select').chosen();

        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                Create: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Perjalanan Dinas',
                        closable: false,
                        message:function () {
                            var url = '{{route('OfficialTask::Add')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Edit: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Perjalanan Dinas',
                        closable: false,
                        message:function () {
                            var url = '{{route('OfficialTask::Edit')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                Delete: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus data ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('OfficialTask::Delete')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            },
            ready:function(){
                $('#date_departure').datetimepicker({
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $('#date_arrival').datetimepicker({
                    useCurrent: false,
                    format: 'DD-MM-YYYY',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-crosshairs',
                        clear: 'fa fa-trash'
                    }
                });
                $("#date_departure").on("dp.change", function (e) {
                    $('#date_arrival').data("DateTimePicker").minDate(e.date);
                });
                $("#date_arrival").on("dp.change", function (e) {
                    $('#date_departure').data("DateTimePicker").maxDate(e.date);
                });
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
    <!-- DATETIMEPICKER-->
@endsection