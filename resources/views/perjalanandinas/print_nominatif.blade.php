@extends('layouts.app-jakban-print')

@section('content-jakban')
    <div class="row">
        <div class="col-xs-12 text-center" style="margin: 0 auto;line-height: 1.7">
            <h4>
                <strong>
                    {!! strtoupper("Daftar Nominatif perjalanan dinas dalam negeri <br> balai teknik perkeretaapian wilayah jakarta dan banten <br> tahun anggaran ").date('Y')!!}
                </strong>
            </h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table" border="1">
                <thead>
                <tr>
                    <th rowspan="2" width="4%">No</th>
                    <th rowspan="2" width="15%">Nama</th>
                    <th rowspan="2" width="10%">Pangkat/Golongan</th>
                    <th rowspan="2" width="8%">Lama Perjalanan Dinas</th>
                    <th rowspan="2" width="5%">Tujuan</th>
                    <th rowspan="2" width="10%">Tanggal</th>
                    <th  width="25%" colspan="4">Uraian (Rp.)</th>
                    <th rowspan="2" width="13%">Jumlah</th>
                    <th rowspan="2" width="15%">Keterangan</th>
                </tr>
                <tr>
                    <th width="5%">Uang Harian</th>
                    <th width="5%">Hotel</th>
                    <th width="5%">Tiket</th>
                    <th width="5%">Transport</th>
                </tr>
                </thead>
                <tbody>
                @php($no = 1)
                @php($tot_all = 0)
                @foreach($data as $d)
                    @php($tot = 0)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{@$d->Employee()->name?:'-'}}</td>
                        <td>{{@$d->Employee() ? @$d->Employee()->Classification()->class  : "-"}}</td>
                        <td>{{$d->day_duration}} hari</td>
                        <td>{{@$d->ArrivalPlace()->name}}</td>
                        <td>{{date('d-M-Y',strtotime($d->date_departure))}} s/d {{date('d-M-Y',strtotime($d->date_arrival))}}</td>
                        <td>

                            @if($d->perjalanan_amount_type == "Other")
                                @php($tot += $d->type_amount*$d->day_duration)
                                Rp. {{number_format(round($d->type_amount*$d->day_duration))}}
                            @else
                                @php($tot += $d->type_amount)
                                Rp. {{number_format($d->type_amount)}}
                            @endif
{{--                            {{$d->type_amount == 0 ? '-' : "Rp.".number_format($d->type_amount)}}--}}
                        </td>
                        <td>
                            @php($hotelAmount = $d->hotel_in_cash == 'N' ? $d->hotel_amount :  $d->hotel_amount * 30 / 100)
                            @php($tot += $hotelAmount)
                            {{$d->hotel_amount == 0 ? '-' : "Rp.".number_format($hotelAmount)}}
                        </td>
                        <td>
                            @php($ticket_total = 0)
                            @if($d->transportation_type != "Car")
                                @php($ticket_total = $d->transportation_amount + $d->transportation_amount_2)
                            @endif
                            @php($tot += $ticket_total)
                            {{$ticket_total == 0 ? '-' : "Rp.".number_format($ticket_total)}}
                        </td>
                        <td>
                            @php($transport_total = $d->taxi_total_amount + $d->vehicle_amount)
                            @if($d->transportation_type == "Car")
                                @php($transport_total += $d->transportation_amount + $d->transportation_amount_2)
                            @endif
                            @php($tot += $transport_total)
                            {{$transport_total == 0 ? '-' : "Rp.".number_format($transport_total)}}
                        </td>
                        <td>Rp. <span class="text-right">{{number_format($tot)}}</span></td>
                        @if($no == 1)
                        <td rowspan="{{$data->count()}}" style="vertical-align:middle;text-align: center">
                            {{$no == 1 ? $d->description : ''}}
                        </td>
                        @endif
                    </tr>
                    @php($no++)
                    @php($tot_all+=$tot)
                @endforeach
                <tr>
                    <td colspan="10" class="text-center">
                        <strong>
                            Total
                        </strong>
                    </td>
                    <td lass="text-left">
                        <strong>
                            Rp. {{number_format($tot_all)}}
                        </strong>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="row clearfix">
        <div class="col-xs-6 pull-left" style="margin-left:50px">
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            Bendahara Pengeluaran
            <br><br><br><br><br><br><br>
            <strong><u>{{ucfirst(\App\User::find(Setting::name('bendahara_pengeluaran'))->name)}}</u></strong><br>
            {{\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP}}
        </div>
        <div class="col-xs-5 pull-left" style="margin-left:150px">
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            Jakarta,
            &nbsp;
            {{--&nbsp;--}}
            {{--..... --}}
            {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}<br>
           Penjabat Pembuat Komitmen Kegiatan Rutin
            <br><br><br><br><br><br>
            <strong><u>{{ucfirst(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->name)}}</u></strong><br>
            {{\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP}}
        </div>
    </div>

    <div class="page-break"></div>

    <div class="row">
        <div class="col-xs-12 text-center" style="margin: 0 auto;line-height: 1.7">
            <h4>
                <strong>
                    {!! strtoupper("Daftar pembayaran perjalanan dinas dalam negeri <br> balai teknik perkeretaapian wilayah jakarta dan banten <br> tahun anggaran ").date('Y') !!}
                </strong>
            </h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table" border="1">
                <thead>
                <tr>
                    <th rowspan="2" width="5%">No</th>
                    <th rowspan="2" width="15%">Nama</th>
                    <th rowspan="2" width="10%">Pangkat/Golongan</th>
                    <th rowspan="2" width="10%">Jabatan</th>
                    <th width="20%" colspan="4">Uraian (Rp.)</th>
                    <th rowspan="2" width="15%">Jumlah Dibayarkan</th>
                    <th rowspan="2" width="15%">Keterangan</th>
                </tr>
                <tr>
                    <th width="5%">Uang Harian</th>
                    <th width="5%">Hotel</th>
                    <th width="5%">Tiket</th>
                    <th width="5%">Transport</th>
                </tr>
                </thead>
                <tbody>
                @php($no = 1)
                @php($tot_all = 0)
                @foreach($data as $d)
                    @php($tot = 0)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{@$d->Employee()->name?:'-'}}</td>
                        <td>{{@$d->Employee() ? @$d->Employee()->Classification()->class : "-"}}</td>
                        <td>{{@$d->Employee()->Position()->position?:'-'}}</td>
                        <td>
                            @if($d->perjalanan_amount_type == "Other")
                                @php($tot += $d->type_amount*$d->day_duration)
                                Rp. {{number_format(round($d->type_amount*$d->day_duration))}}
                            @else
                                @php($tot += $d->type_amount)
                                Rp. {{number_format($d->type_amount)}}
                            @endif
                            {{--                            {{$d->type_amount == 0 ? '-' : "Rp.".number_format($d->type_amount)}}--}}
                        </td>
                        <td>
                            @php($tot += $d->hotel_amount)

                            {{$d->hotel_amount == 0 ? '-' : "Rp.".number_format($d->hotel_amount)}}
                        </td>
                        <td>
                            @php($ticket_total = 0)
                            @if($d->transportation_type != "Car")
                                @php($ticket_total = $d->transportation_amount + $d->transportation_amount_2)
                            @endif
                            @php($tot += $ticket_total)
                            {{$ticket_total == 0 ? '-' : "Rp.".number_format($ticket_total)}}
                        </td>
                        <td>
                            @php($transport_total = $d->taxi_total_amount + $d->vehicle_amount)
                            @if($d->transportation_type == "Car")
                                @php($transport_total += $d->transportation_amount + $d->transportation_amount_2)
                            @endif
                            @php($tot += $transport_total)
                            {{$transport_total == 0 ? '-' : "Rp.".number_format($transport_total)}}
                        </td>
                        <td>Rp. <span class="text-right">{{number_format($tot)}}</span></td>
                        <td style="vertical-align:bottom;">
                            {{$no}} ..............................
                        </td>
                    </tr>
                    @php($no++)
                    @php($tot_all+=$tot)
                @endforeach
                <tr>
                    <td colspan="8" class="text-center">
                        <strong>
                            Total
                        </strong>
                    </td>
                    <td lass="text-left">
                        <strong>
                            Rp. {{number_format($tot_all)}}
                        </strong>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="row clearfix">
        <div class="col-xs-6 pull-left" style="margin-left:50px">
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            Bendahara Pengeluaran
            <br><br><br><br><br><br><br><br>
            <strong><u>{{ucfirst(\App\User::find(Setting::name('bendahara_pengeluaran'))->name)}}</u></strong><br>
            {{\App\User::find(Setting::name('bendahara_pengeluaran'))->NIP}}
        </div>
        <div class="col-xs-5 pull-left" style="margin-left:150px">
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            Jakarta,
            &nbsp;
            {{--&nbsp;--}}
            {{--..... --}}
            {{\App\Helpers\main_helper::indonesiaDate(date('n'),"month")." ".date('Y')}}<br>
            Penjabat Pembuat Komitmen Kegiatan Rutin
            <br><br><br><br><br><br><br>
            <strong><u>{{ucfirst(\App\User::find(Setting::name('penjabat_pembuat_komitment'))->name)}}</u></strong><br>
            {{\App\User::find(Setting::name('penjabat_pembuat_komitment'))->NIP}}
        </div>
    </div>
@endsection
@section('scripts-jakban')

@endsection