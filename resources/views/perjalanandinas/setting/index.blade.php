@extends('layouts.app-jakban')

@section('content-jakban')
    <data-manager></data-manager>
    <template id="dataManager">
        <h3>Pengaturan Perjalanan Dinas
            <!-- <small>You are loged in!</small> -->
        </h3>
        @if(Session::has('success_message'))
            <div class="alert alert-success text-center">
                {{Session::get('success_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        @elseif(Session::has('error_message'))
            <div class="alert alert-danger text-center">
                {{Session::get('error_message')}}
                <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
    @endif
        <!-- START panel tab-->
        <div role="tabpanel" class="panel">
            <!-- Nav tabs-->
            <ul role="tablist" class="nav nav-tabs nav-justified">
                <li role="presentation" class="active">
                    <a href="#pengaturan" aria-controls="data" role="tab" data-toggle="tab">
                        <!-- <em class="fa fa-clock-o fa-fw"></em> -->Pengaturan</a>
                </li>
                <li role="presentation">
                    <a href="#daftartempat" aria-controls="daftar" role="tab" data-toggle="tab">
                        <!-- <em class="fa fa-money fa-fw"></em> -->Daftar Tempat</a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content p0">
                <div id="pengaturan" role="tabpanel" class="tab-pane active">
                    <form method="POST" action="{{route('SettingOfficial::Save')}}" accept-charset="UTF-8" class="form-horizontal">
                    {{ csrf_field() }}
                    <!-- START row-->
                        <div class="row">
                            <div style="clear: both;">&nbsp;</div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="departemen" class="col-md-3 control-label">Departemen *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="departemen">
                                        <input type="text" name="departemen" value="{{Setting::name('departemen')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="unit" class="col-md-3 control-label">Unit *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="departemen_unit">
                                        <input type="text" name="departemen_unit" value="{{Setting::name('departemen_unit')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="satuankerja" class="col-md-3 control-label">Satuan Kerja *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="satuan_kerja">
                                        <input type="text" name="satuan_kerja" value="{{Setting::name('satuan_kerja')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="lokasi" class="col-md-3 control-label">Lokasi *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="lokasi_kerja">
                                        <input type="text" name="lokasi_kerja" value="{{Setting::name('lokasi_kerja')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="bagian" class="col-md-3 control-label">Bagian *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="bagian_kerja">
                                        <input type="text" name="bagian_kerja" value="{{Setting::name('bagian_kerja')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="dipa_ta" class="col-md-3 control-label">DIPA TA *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="dipa_tahun">
                                        <input type="text" name="dipa_tahun" value="{{Setting::name('dipa_tahun')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="dipa" class="col-md-3 control-label">DIPA Nomor *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="dipa_no">
                                        <input type="text" name="dipa_no" value="{{Setting::name('dipa_no')}}" class=" form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="dipa_tanggal" class="col-md-3 control-label">DIPA Tanggal *</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="settings[]" value="dipa_tanggal">
                                        <input type="text" name="dipa_tanggal" value="{{Setting::name('dipa_tanggal')}}" class=" form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="kuasa_pengguna_anggaran" class="col-md-3 control-label">Setting Tempat Asal *</label>
                                    <div class="col-md-8">
                                        <input type="hidden" name="settings[]" value="setting_tempat_asal">
                                        <input type="hidden" v-model="formData.setting_tempat_asal" value="{{Setting::name('setting_tempat_asal')}}">
                                        <select class="form-control" v-model="formData.setting_tempat_asal" id="setting_tempat_asal" name="setting_tempat_asal">
                                            @foreach($data_places->where('type','In') as $place)
                                                <option value="{{$place->id}}">{{$place->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="kuasa_pengguna_anggaran" class="col-md-3 control-label">Kuasa Pengguna Anggaran *</label>
                                    <div class="col-md-8">
                                        <input type="hidden" name="settings[]" value="kuasa_pengguna_anggaran">
                                        <input type="hidden" v-model="formData.kuasa_pengguna_anggaran" value="{{Setting::name('kuasa_pengguna_anggaran')}}">
                                        <select class="form-control" v-model="formData.kuasa_pengguna_anggaran" id="kuasa_pengguna_anggaran" name="kuasa_pengguna_anggaran">
                                            @foreach($data_user_employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}} - {{$employee->NIP}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bendahara_pengeluaran" class="col-md-3 control-label">Bendahara Pengeluaran *</label>
                                    <div class="col-md-8">
                                        <input type="hidden" name="settings[]" value="bendahara_pengeluaran">
                                        <input type="hidden" v-model="formData.bendahara_pengeluaran" value="{{Setting::name('bendahara_pengeluaran')}}">
                                        <select class="form-control" v-model="formData.bendahara_pengeluaran" id="bendahara_pengeluaran" name="bendahara_pengeluaran">
                                            @foreach($data_user_employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}} - {{$employee->NIP}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bendahara_pengeluaran" class="col-md-3 control-label">Penjabat Pembuat Komitmen *</label>
                                    <div class="col-md-8">
                                        <input type="hidden" name="settings[]" value="penjabat_pembuat_komitment">
                                        <input type="hidden" v-model="formData.penjabat_pembuat_komitment" value="{{Setting::name('penjabat_pembuat_komitment')}}">
                                        <select class="form-control" v-model="formData.penjabat_pembuat_komitment" id="penjabat_pembuat_komitment" name="penjabat_pembuat_komitment">
                                            @foreach($data_user_employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}} - {{$employee->NIP}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bendahara_pengeluaran" class="col-md-3 control-label">Kepala Balai *</label>
                                    <div class="col-md-8">
                                        <input type="hidden" name="settings[]" value="kepala_balai">
                                        <input type="hidden" v-model="formData.kepala_balai" value="{{Setting::name('kepala_balai')}}">
                                        <select class="form-control" v-model="formData.kepala_balai" id="kepala_balai" name="kepala_balai">
                                            @foreach($data_user_employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}} - {{$employee->NIP}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="bagian" class="col-md-3 control-label">Set Nomor Terakhir Perjalanan Dinas</label>
                                    <div class="col-md-9">
                                        <p>
                                            {{Setting::name('set_last_seq')}}
                                        </p>
                                        <input type="hidden" name="settings[]" value="set_last_seq">
                                        <input type="text" name="set_last_seq" value="" class=" form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="javascript:history.back(-1)"><button class="btn btn-lg btn-default">Cancel</button></a>
                            <button type="submit" class="btn btn-lg btn-info"><strong>Save Change</strong></button>
                        </div>
                    </form>
                </div>

                <div id="daftartempat" role="tabpanel" class="tab-pane">

                    <!-- START row-->
                    <div class="row">
                        <div style="clear: both;">&nbsp;</div>
                        <div class="col-lg-12">
                            <!-- START panel-->
                            <div class="panel panel-default">
                                <div class="panel-heading"> Tempat Tujuan
                                    <div class="panel-footer text-right">
                                        <button @click.prevent="CreatePlace" class="btn btn-lg btn-info"><strong>Tambah Tempat</strong></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <!-- START table responsive-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Provinsi</th>
                                                <th>Nama</th>
                                                <th>Jenis</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach($data_places as $place)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$place->Provinsi()->nama}}</td>
                                                    <td>{{$place->name}}</td>
                                                    <td>
                                                        @if($place->type == 'In')
                                                            Dalam Kota
                                                        @elseif($place->type == 'Out')
                                                            Luar Kota
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <button type="button" @click.prevent="EditPlace({{$place->id}})" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button>
                                                        <button type="button" @click.prevent="DeletePlace({{$place->id}})" class="btn btn-sm btn-danger"><em class="fa fa-eraser"></em></button>
                                                    </td>
                                                </tr>
                                                @php(
                                                    $no++
                                                )
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END table responsive-->
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            Page {{$data_places->currentPage()}} of {{$data_places->lastPage()}}
                                        </div>
                                        <div class="col-lg-6 text-right">
                                            {{$data_places->appends([
                                                 'status'=>Request::get('status'),
                                                 'sortby'=>Request::get('sortby'),
                                                 'order'=>Request::get('order'),
                                                 'q'=>Request::get('q')
                                             ])->render()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END row-->

                </div>
            </div>
        </div>
        <!-- END panel tab-->
    </template>
@endsection

@section('scripts-jakban')
    <script type="text/javascript" src="/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <style>
        .login-dialog .modal-dialog {
            width: 500px;
        }
    </style>
    <script>
        var dataManager = Vue.extend({
            template:'#dataManager',
            data: function() {
                return {
                    formData:{
                    }
                }
            },
            methods: {
                CreatePlace: function(){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Tambah Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('SettingOfficial::AddPlace')}}';
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                EditPlace: function(id){
                    var self = this;
                    var dialog = new BootstrapDialog({
                        id:'form-folder',
                        title:'Edit Data Jadwal',
                        closable: false,
                        message:function () {
                            var url = '{{route('SettingOfficial::EditPlace')}}?id='+id;
                            var form = $('<div class="clearfix"></div>').load(url);
                            return form;
                        },
                        buttons: [
                            {
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialogRef) {
                                    dialogRef.close();
                                }
                            },
                        ],
                    });
                    dialog.open();
                },
                DeletePlace: function(id) {
                    var self = this;
                    BootstrapDialog.confirm({
                        title: 'Delete Confirmation',
                        message: 'Yakin ingin menghapus jadwal ini',
                        type: BootstrapDialog.TYPE_WARNING,
                        callback: function (result) {
                            if(result) {
                                self.$http.post('{{route('SettingOfficial::DeletePlace')}}?id='+id).then(function (response){
                                    if(response.json().status_action=="false"){
                                        BootstrapDialog.alert('Something Error!');
                                    } else {
                                        location.reload();
                                    }
                                },function(response) {
                                    ShowAlert('Something Error!','danger');
                                });
                            }
                        }
                    });
                },
            }
        });
        Vue.component('data-manager',dataManager);
    </script>
    <!-- DATETIMEPICKER-->
@endsection