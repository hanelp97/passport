
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'JAKBAN') }}</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="{{url('/')}}/vendor/fontawesome/css/font-awesome.min.css">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="{{url('/')}}/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.css" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="{{url('/')}}/css/app-modify.css" id="maincss">

   <!-- fav icon -->
   <link rel="icon" type="image/x-icon" href="{{url('/')}}img/logo-single.png">
</head>

<body>
   <div class="wrapper">
      <div class="block-center mt-xl wd-xl">
         <!-- START panel-->
         <div class="panel panel-dark panel-flat">
            <div class="panel-heading text-center">
               <a href="#">
                  <img src="img/logo.png" alt="Image" class="block-center img-rounded">
               </a>
            </div>
            <div class="panel-body">
               <p class="text-center pv">SIGN IN TO CONTINUE.</p>
               {!! Form::open(['url'=>'login', 'class'=>'mb-lg', 'role'=>'form',"data-parsley-validate"=>"","novalidate"=>""]) !!}

                <div class="form-group has-feedback{{ $errors->has('username') ? ' has-error' : '' }}">
                    {!! Form::text('username', null, ['id'=>'exampleInputUsername1','class'=>'form-control','autocomplete'=>'off', 'autofocus', 'placeholder'=>'Username', 'required'=>'required']) !!}
                    <span class="fa fa-user form-control-feedback text-muted"></span>
                    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    {!! Form::password('password', ['id'=>'exampleInputPassword1','class'=>'form-control','required'=>'required','placeholder'=>'Password']) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="clearfix">
                     <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                           {!! Form::checkbox('remember')!!}
                           <span class="fa fa-check"></span>Remember Me</label>
                     </div>
                        <div class="pull-right"><a href="{{ url('/password/reset') }}" class="text-muted">Forgot your password?</a>
                     </div>
                </div>
                
                <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
                <a href="{{url('schedule')}}" target="_blank" class="btn btn-block btn-success mt-lg">Data Jadwal Lengkap</a>
                <a href="http://103.200.7.167/APK/BTPJB/jakban/v1" target="_blank" class="btn btn-block btn-info mt-lg"><i class="fa fa-play-circle"></i> Download APK</a>

                {!! Form::close() !!}

               <!-- <p class="pt-lg text-center">Need to Signup?</p><a href="{{ url('/register') }}" class="btn btn-block btn-default">Register Now</a> -->
            </div>
         </div>
         <!-- END panel-->
         <div class="p-lg text-center">
            <span>&copy;</span>
            <span>2017</span>
            <span>-</span>
            <!-- <span>Angle</span> -->
            <br>
            <span>{{ config('app.name', 'JAKBAN') }}</span>
         </div>
      </div>
   </div>
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="{{url('/')}}/vendor/modernizr/modernizr.custom.js"></script>
   <!-- JQUERY-->
   <script src="{{url('/')}}/vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP-->
   <script src="{{url('/')}}/vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API-->
   <script src="{{url('/')}}/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- PARSLEY-->
   <script src="{{url('/')}}/vendor/parsleyjs/dist/parsley.min.js"></script>
   <!-- =============== APP SCRIPTS ===============-->
   <script src="{{url('/')}}/js/app.js"></script>
</body>

</html>


