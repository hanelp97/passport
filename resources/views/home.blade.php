@extends('layouts.app-jakban')

@section('content-jakban')
    <h3>
        Dashboard
    </h3>
    <div class="row">

        <div class="col-md-12">
            <div class="col-lg-3 col-sm-6">
                <!-- START widget-->
                <div class="panel widget bg-green-dark">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center3 pv-lg">
                            <i class="fa fa-file fa-3x"></i>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <div class="h2 mt0">
                                {{\App\File::all()->count()}}
                            </div>
                            <div class="text-uppercase">Jumlah File</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- START widget-->
                <div class="panel widget bg-green-dark">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-green-dark pv-lg">
                            <i class="fa fa-database fa-3x"></i>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <div class="h2 mt0">
                                {{\App\Disposition::all()->count()}}
                            </div>
                            <div class="text-uppercase">Jumlah Disposisi</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- START widget-->
                <div class="panel widget bg-green-dark">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-green-dark pv-lg">
                            <i class="fa fa-calendar fa-3x"></i>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <div class="h2 mt0">
                                {{\App\Schedule::all()->count()}}
                            </div>
                            <div class="text-uppercase">Jumlah Jadwal</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- START widget-->
                <div class="panel widget bg-green-dark">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-green-dark pv-lg">
                            <i class="fa fa-database fa-3x"></i>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <div class="h2 mt0">
                                {{\App\PerjalananDinas::all()->count()}}
                            </div>
                            <div class="text-uppercase"> Perjalanan Dinas</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts-jakban')
    <!-- FILESTYLE-->
    <script src="{{url('/')}}/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>
    <!-- TAGS INPUT-->
    <script src="{{url('/')}}/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <!-- CHOSEN-->
    <script src="{{url('/')}}/vendor/chosen_v1.2.0/chosen.jquery.min.js"></script>
    <!-- SLIDER CTRL-->
    <script src="{{url('/')}}/vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
    <!-- INPUT MASK-->
    <script src="{{url('/')}}/vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
    <!-- WYSIWYG-->
    <script src="{{url('/')}}/vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js"></script>
    <script src="{{url('/')}}/vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js"></script>
    <!-- DATETIMEPICKER-->
    <script type="text/javascript" src="{{url('/')}}/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- COLORPICKER-->
    <script type="text/javascript" src="{{url('/')}}/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Demo-->
    <script src="{{url('/')}}/js/demo/demo-forms.js"></script>
    <!-- SELECT2-->
    <script src="{{url('/')}}/vendor/select2/dist/js/select2.js"></script>
@endsection