<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Admin";
		$adminRole->save();
		// Membuat role arsip
		$arsipRole = new Role();
		$arsipRole->name = "arsip";
		$arsipRole->display_name = "Bagian Kearsipan";
		$arsipRole->save();
		// Membuat role keuangan
		$keuanganRole = new Role();
		$keuanganRole->name = "keuangan";
		$keuanganRole->display_name = "Bagian Keuangan";
		$keuanganRole->save();

		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin Jakban';
		$admin->username = 'admin';
		$admin->email = 'admin@gmail.com';
		$admin->password = bcrypt('rahasia');
		$admin->save();
		$admin->attachRole($adminRole);
		// Membuat sample arsip
		$arsip = new User();
		$arsip->name = "Bagian Kearsipan";
		$arsip->username = 'arsip';
		$arsip->email = 'kearsipan@gmail.com';
		$arsip->password = bcrypt('rahasia');
		$arsip->save();
		$arsip->attachRole($arsipRole);
		// Membuat sample keuangan
		$keuangan = new User();
		$keuangan->name = "Bagian Keuangan";
		$keuangan->username = 'keuangan';
		$keuangan->email = 'keuangan@gmail.com';
		$keuangan->password = bcrypt('rahasia');
		$keuangan->save();
		$keuangan->attachRole($keuanganRole);
    }
}
