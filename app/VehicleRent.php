<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleRent extends Model
{
    protected $table = 'vehicle_rent';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Provinsi::class,'provinsi_id')->first();
    }
}
