<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeAkunKategori extends Model
{
    protected $table = 'kode_akun_kategori';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function KodeAkuns() {
        return $this->hasMany(KodeAkun::class,'kategori_id');
    }
}
