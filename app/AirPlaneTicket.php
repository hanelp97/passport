<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirPlaneTicket extends Model
{
    protected $table = 'airplane_ticket';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function First() {
        return $this->belongsTo(KabKota::class,'city_first_id')->first();
    }

    public function Destination() {
        return $this->belongsTo(KabKota::class,'city_destination_id')->first();
    }
}
