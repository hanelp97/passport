<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeAkun extends Model
{
    protected $table = 'kode_akun';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Kategori() {
        return $this->belongsTo(KodeAkunKategori::class,'kategori_id')->first();
    }

    public function PerjalananDinas() {
        return $this->hasMany(PerjalananDinas::class,'kode_akun_id');
    }
}
