<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerjalananDinasJenis extends Model
{
    protected $table = 'perjalanan_dinas_jenis';
    protected $primaryKey = "code";
    public $incrementing = false;

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
