<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiFee extends Model
{
    protected $table = 'taxi_fee';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Provinsi::class,'provinsi_id')->first();
    }
}
