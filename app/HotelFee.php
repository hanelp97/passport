<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelFee extends Model
{
    protected $table = 'hotel_fee';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Provinsi::class,'provinsi_id')->first();
    }
}
