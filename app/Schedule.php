<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule';
    protected $casts = [
        'forward_to_user_id' => 'array',
        'forward_to_forwarder' => 'array',
        'category_id' => 'array'
    ];

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Category() {
        return $this->belongsTo(ScheduleCategory::class,'category_id')->first();
    }

    public function File() {
        return $this->belongsTo(File::class,'file_id')->first();
    }
}
