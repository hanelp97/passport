<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerjalananDinas extends Model
{
    protected $table = 'perjalanan_dinas';

    protected $casts = [
        'total_amount' => 'float',
    ];

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Employee() {
        return $this->belongsTo(User ::class,'employee_id')->first();
    }

    public function KodeAkun() {
        return $this->belongsTo(KodeAkun::class,'kode_akun_id')->first();
    }

    public function Type() {
        return $this->belongsTo(PerjalananDinasJenis ::class,'type_code')->first();
    }

    public function DeparturePlace() {
        return $this->belongsTo(Place ::class,'departure_place_id')->first();
    }

    public function ArrivalPlace() {
        return $this->belongsTo(Place ::class,'arrival_place_id')->first();
    }

    public function TaxiProv() {
        return $this->belongsTo(Provinsi ::class,'taxi_provinsi_id')->first();
    }

    public function HotelProv() {
        return $this->belongsTo(Provinsi ::class,'hotel_provinsi_id')->first();
    }

    public function RentProv() {
        return $this->belongsTo(Provinsi ::class,'vehicle_rent_provinsi_id')->first();
    }
    public function TaxiFee() {
        return $this->belongsTo(TaxiFee ::class,'taxi_ticket_id')->first();
    }
    public function VehicleRentFee() {
        return $this->belongsTo(VehicleRent ::class,'vehicle_rent_price_id')->first();
    }
}
