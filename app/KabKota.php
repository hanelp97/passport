<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KabKota extends Model
{
    protected $table = 'kabupaten_kota';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Folder::class,'provinsi_id')->first();
    }
}
