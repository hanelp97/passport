<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiayaPerjalananDinas extends Model
{
    protected $table = 'biaya_perjalanan_dinas';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Provinsi::class,'provinsi_id')->first();
    }
}
