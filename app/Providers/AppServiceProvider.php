<?php

namespace App\Providers;

use App\KabKota;
use App\Provinsi;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // PROVINSI
        $master_provinsi = Provinsi::orderBy('nama','asc')->get();
        view()->share('master_provinsi', $master_provinsi);
        $master_kabkota = KabKota::orderBy('nama','asc')->get();
        view()->share('master_kabkota', $master_kabkota);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
