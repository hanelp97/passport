<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClass extends Model
{
    protected $table = 'user_class';
    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
