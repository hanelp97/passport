<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forwarder extends Model
{
    protected $table = 'forwarder';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
