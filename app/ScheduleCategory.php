<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleCategory extends Model
{
    protected $table = 'schedule_category';
    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
