<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passport extends Model
{
    protected $table = 'data_passport';

    public static function CreateOrUpdate($id)
    {
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
