<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPosition extends Model
{
    protected $table = 'user_position';
    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
