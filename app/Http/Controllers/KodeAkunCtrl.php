<?php

namespace App\Http\Controllers;

use App\KodeAkun;
use App\KodeAkunKategori;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KodeAkunCtrl extends Controller
{
    public function index(Request $request)
    {
        $data_kode_akuns = KodeAkun::select('*');

        $data =  [
            'data_kode_akuns' => $data_kode_akuns->paginate(10)
        ];
        return view('databasesettings.kode_akun.index',$data);
    }

    public function add_page(Request $request)
    {
        $data = [
            'mode' => 'add',
            'kategoris' => KodeAkunKategori::all(),
        ];
        return view('databasesettings.kode_akun.form',$data);
    }

    public function edit_page(Request $request)
    {
        $data = [
            'data' => KodeAkun::find($request->id),
            'mode' => 'edit',
            'kategoris' => KodeAkunKategori::all(),
        ];
        return view('databasesettings.kode_akun.form',$data);
    }

    public function save(Request $request)
    {
        if($request->kategori_id == 0 || $request->code == "" || $request->type == "" || $request->rate == "")
        {
            $request->session()->flash('error_message', 'Field *s Wajib Diisi!');
            return redirect()->route('KodeAkun::Index');
        }

        $kode_akun = KodeAkun::CreateOrUpdate($request->id);
        $kode_akun->kategori_id = $request->kategori_id;
        $kode_akun->code = $request->code;
        $kode_akun->type = $request->type;
        $kode_akun->rate = $request->rate;

        if($kode_akun->save())
        {
            $request->session()->flash('success_message', 'Data telah di' . (isset($request->id) ? 'edit' : 'buat') . ' !');
            return redirect()->back();
        }

        $request->session()->flash('error_message', 'Data telah di' . (isset($request->id) ? 'edit' : 'buat') . ' Error!');
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();

        try
        {
            $data = KodeAkun::find($request->id);

            if($data->PerjalananDinas()->count() > 0)
            {
                throw new \Exception("Tidak bisa menghapus kode akun!. Perjalanan dinas sudah ada didalam akun ini!");
            }

            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Data Deleted !');

            $data = [
                'status_action' => 'success',
            ];

            return response()->json($data);
        }
        catch(\Exception $e)
        {
            DB::rollBack();

            $data = [
                'message' => $e->getMessage(),
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
