<?php

namespace App\Http\Controllers;

use App\HotelFee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PenginapanController extends Controller
{
    public function index(Request $request)
    {
        $data_penginapans = HotelFee::select('*');
        if($request->unit){
            $data_penginapans->orwhere('unit','like','%'.$request->unit.'%');
        }
        $data =  [
            'data_penginapans' => $data_penginapans->paginate(10)
        ];
        return view('databasesettings.penginapan.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.penginapan.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => HotelFee::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.penginapan.form',$data);
    }
    public function save(Request $request) {
        if($request->provinsi_id == "" || $request->unit == "" || $request->eselon1 == "" || $request->eselon2 == "" || $request->eselon3 == "" || $request->eselon4 == "" || $request->golongan1_2 == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->route('Penginapan::Index');
        }

        $penginapan = HotelFee::CreateOrUpdate($request->id);
        $penginapan->provinsi_id = $request->provinsi_id;
        $penginapan->unit = $request->unit;
        $penginapan->eselon1 = $request->eselon1;
        $penginapan->eselon2 = $request->eselon2;
        $penginapan->eselon3 = $request->eselon3;
        $penginapan->eselon4 = $request->eselon4;
        $penginapan->golongan1_2 = $request->golongan1_2;

        if($penginapan->save()){
            $request->session()->flash('success_message', 'Data Penginapan ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Penginapan ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = HotelFee::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Penginapan Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
