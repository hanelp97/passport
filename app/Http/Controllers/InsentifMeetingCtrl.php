<?php

namespace App\Http\Controllers;

use App\InsentifMeeting;
use Illuminate\Http\Request;

class InsentifMeetingCtrl extends Controller
{
    public function index(Request $request)
    {
        $data_insentif_meetings = InsentifMeeting::select('*');
        if($request->unit){
            $data_insentif_meetings->orwhere('unit','like','%'.$request->unit.'%');
        }
        $data =  [
            'data_insentif_meetings' => $data_insentif_meetings->paginate(10)
        ];
        return view('databasesettings.insentif_meeting.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.insentif_meeting.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => InsentifMeeting::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.insentif_meeting.form',$data);
    }
    public function save(Request $request) {
        if($request->provinsi_id == "" || $request->unit == "" || $request->fullboard_other_city == "" || $request->fullboard_in_city == "" || $request->fullday_halfday_in_city == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->route('InsentifMeeting::Index');
        }

        $insentif_meeting = InsentifMeeting::CreateOrUpdate($request->id);
        $insentif_meeting->provinsi_id = $request->provinsi_id;
        $insentif_meeting->unit = $request->unit;
        $insentif_meeting->fullboard_other_city = $request->fullboard_other_city;
        $insentif_meeting->fullboard_in_city = $request->fullboard_in_city;
        $insentif_meeting->fullday_halfday_in_city = $request->fullday_halfday_in_city;

        if($insentif_meeting->save()){
            $request->session()->flash('success_message', 'Data Insentif Meeting ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Insentif Meeting ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = InsentifMeeting::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Insentif Meeting Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
