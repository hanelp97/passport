<?php

namespace App\Http\Controllers;

use App\PerjalananDinas;
use App\Place;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingperjalanandinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data_user_employees' => User::WhereRoleIs('pegawai')->get(),
            'data_places' => Place::paginate(20),
        ];
        return view('perjalanandinas.setting.index',$data);
    }

    public function save(Request $request)
    {
        foreach($request->settings as $setting)
        {
            if($request->get($setting) == setting($setting))
            {
                continue;
            }

            $settingModel = Setting::where('name',$setting)->first();

            if($setting=="set_last_seq")
            {
                $lastData = PerjalananDinas::latest()->first();
                $lastData->sequence = $request->get($setting);
                $lastData->save();
            }

            $settingModel->value = $request->get($setting);
            $settingModel->save();
        }
        $request->session()->flash('success_message','Sukses memperbaharui setting!!');
        return redirect()->back()->withInput();
    }
    //PLACE
    public function add_place(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('perjalanandinas.setting.tempat.form',$data);
    }
    public function edit_place(Request $request) {
        $data = [
            'data' => Place::find($request->id),
            'mode' => 'edit',
        ];
        return view('perjalanandinas.setting.tempat.form',$data);
    }
    public function save_place(Request $request) {
        if($request->provinsi_id == "" || $request->name == "" || $request->type == "") {
            $request->session()->flash('error_message', 'Field *s Wajib Diisi!');
            return redirect()->route('SettingOfficial::Index');
        }

        $tempat = Place::CreateOrUpdate($request->id);
        $tempat->provinsi_id = $request->provinsi_id;
        $tempat->name = $request->name;
        $tempat->type = $request->type;

        if($tempat->save()){
            $request->session()->flash('success_message', 'Data Tempat ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Tempat ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy_place(Request $request) {
        DB::beginTransaction();
        try {
            $data = Place::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Tempat Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
