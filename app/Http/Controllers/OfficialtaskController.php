<?php

namespace App\Http\Controllers;

use App\AirPlaneTicket;
use App\BiayaPerjalananDinas;
use App\Helpers\main_helper;
use App\HotelFee;
use App\InsentifMeeting;
use App\KodeAkun;
use App\PerjalananDinas;
use App\PerjalananDinasJenis;
use App\Place;
use App\TaxiFee;
use App\User;
use App\VehicleRent;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfficialtaskController extends Controller
{
    public function api(Request $request) {
        if($request->action == 'get_employee_by_pd_code') {
            $data_pd = PerjalananDinas::where('code',$request->code)->first();
            $data =  [
                'data_employee_id' => $data_pd->employee_id,
            ];
            return response()->json($data);
        }
    }

    public function index(Request $request)
    {
        $data_perjalanan_dinas = PerjalananDinas::select('*');
        $data_perjalanan_dinas->orderby('id','desc');

        if($request->date_departure && $request->date_arrival)
        {
            $data_perjalanan_dinas->whereBetween('date_departure',[date('Y-m-d',strtotime($request->date_departure)),date('Y-m-d',strtotime($request->date_arrival))]);
        }

        if(request()->no_awal && request()->no_akhir)
        {
            $data_perjalanan_dinas->whereBetween('code',[request()->no_awal, request()->no_akhir]);
        }
        $data =  [
            'data_perjalanan_dinas' => $data_perjalanan_dinas->paginate(10),
        ];
        return view('perjalanandinas.index',$data);
    }
    public function index_nofmatif (Request $request)
    {
        $data_perjalanan_dinas = PerjalananDinas::select('*');
        $data_perjalanan_dinas->orderby('id','desc');
        if($request->date_departure && $request->date_arrival)
        {
            $data_perjalanan_dinas->whereBetween('date_departure',[date('Y-m-d',strtotime($request->date_departure)),date('Y-m-d',strtotime($request->date_arrival))]);
        }
        $data =  [
            'data_perjalanan_dinas' => $data_perjalanan_dinas->paginate(10),
        ];
        return view('perjalanandinas.index',$data);
    }
    public function add(Request $request) {
        $data = [
            'mode' => 'add',
            'code_pd' => $this->generateCode(),
            'data_user_employees' => User::all(),
            'data_pd_jenis' => PerjalananDinasJenis::all(),
            'data_kode' => PerjalananDinas::all()->pluck('code'),
            'data_places' => Place::all(),
            'data_airplane_tickets' => AirPlaneTicket::all(),
            'data_taxi_tickets' => TaxiFee::all(),
            'data_hotel_tickets' => HotelFee::all(),
            'data_rent_tickets' => VehicleRent::all(),
            'data_akuns' => KodeAkun::all(),
        ];
        return view('perjalanandinas.form',$data);
    }
    public function edit(Request $request) {
        $data = [
            'mode' => 'edit',
            'data' => PerjalananDinas::find($request->id),
            'data_user_employees' => User::all(),
            'data_pd_jenis' => PerjalananDinasJenis::all(),
            'data_kode' => PerjalananDinas::all()->pluck('code'),
            'data_places' => Place::all(),
            'data_airplane_tickets' => AirPlaneTicket::all(),
            'data_taxi_tickets' => TaxiFee::all(),
            'data_hotel_tickets' => HotelFee::all(),
            'data_rent_tickets' => VehicleRent::all(),
            'data_akuns' => KodeAkun::all(),
        ];
        return view('perjalanandinas.form',$data);
    }
    public function save(Request $request) {

//        return response()->json($request);

        if($request->employee_id == 0 || $request->description == ""
            || $request->date_departure == "" || $request->date_arrival == "" || $request->day_duration == ""
            || $request->type_code == "" || $request->departure_place_id == 0 || $request->arrival_place_id == 0  || $request->transportation_type == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        $code = $this->generateCode();

        if($this->dataExits($code,"code",$request->id)) {
            $request->session()->flash('error_message','Kode Sudah ada!');
            return redirect()->back()->withInput();
        }

        $hotel_amount = 0;
        $hotel_in_cash_amount = 0;
        $transportation_amount = 0;
        $transportation_amount_qty = 1;
        $transportation_amount_2 = 0;
        $transportation_amount_qty_2 = 1;
        $taxi_amount_departure = 0;
        $taxi_amount_arrival = 0;
        $taxi_total_amount = 0;
        $vehicle_amount = 0;
        $type_amount = 0;
        $type_amount_db = 0;

        $employee = User::where('id',$request->employee_id)->first();
        $employeeClass = $employee ? @$employee->Classification()->type : "";

        if($request->perjalanan_amount_type == "Default")
        {
            // get price from db

            // rapat / perjalanan dinas dalam kota
            if($request->type_code=='PerjalananDinasDalamKota' || $request->type_code=='RapatDalamKotaFullboard' ||  $request->type_code=='RapatDalamKotaFullDatHalfDay')
            {
                $place = Place::find($request->departure_place_id);

                //perjalanan dinas dalam kota
                if($request->type_code=='PerjalananDinasDalamKota')
                {
                    $biayaPerjalananDinas = BiayaPerjalananDinas::where('provinsi_id',$place->provinsi_id)->first();

                    if($biayaPerjalananDinas)
                    {
                        $type_amount = $biayaPerjalananDinas->in_city;
                    }
                }

                if($request->type_code=='RapatDalamKotaFullboard' ||  $request->type_code=='RapatDalamKotaFullDatHalfDay')
                {
                    $insentifMeeting = InsentifMeeting::where('provinsi_id',$place->provinsi_id)->first();

                    if($insentifMeeting)
                    {
                        if($request->type_code=='RapatDalamKotaFullboard')
                        {
                            $type_amount = $insentifMeeting->fullboard_in_city;
                        }
                        else if($request->type_code=='RapatDalamKotaFullDatHalfDay')
                        {
                            $type_amount = $insentifMeeting->fullday_halfday_in_city;
                        }
                    }

                }
            }

            // rapat / perjalanan dinas luar kota
            if($request->type_code=='PerjalananDinasLuarKota' || $request->type_code=='RapatLuarKotaFullboard')
            {
                $place = Place::find($request->arrival_place_id);

                //perjalanan dinas luar kota
                if($request->type_code=='PerjalananDinasLuarKota')
                {
                    $biayaPerjalananDinas = BiayaPerjalananDinas::where('provinsi_id',$place->provinsi_id)->first();

                    if($biayaPerjalananDinas)
                    {
                        $type_amount = $biayaPerjalananDinas->out_city;
                    }
                }

                if($request->type_code=='RapatLuarKotaFullboard')
                {
                    $insentifMeeting = InsentifMeeting::where('provinsi_id',$place->provinsi_id)->first();

                    if($insentifMeeting)
                    {
                        $type_amount = $insentifMeeting->fullboard_other_city;
                    }

                }
            }


            //jenis diklat
            if($request->type_code=='PerjalananDinasDiklat')
            {
                $place = Place::find($request->arrival_place_id);
                $biayaPerjalananDinas = BiayaPerjalananDinas::where('provinsi_id',$place->provinsi_id)->first();

                if($biayaPerjalananDinas)
                {
                    $type_amount = $biayaPerjalananDinas->diklat;
                }
            }

            $type_amount*=$request->day_duration;
            $type_amount_db = $type_amount;
        }
        else if($request->perjalanan_amount_type == "Other")
        {
            $type_amount = $request->type_amount;
            $type_amount *= $request->day_duration;
            $type_amount_db = $request->type_amount;
        }


        // transportation amount
        if($request->transportation_amount_type == "Default"){
            // get price from db
            if($request->transportation_type == "Airplane"){
                $AirPlaneTicket = AirPlaneTicket::find($request->airplane_ticker_id);
                if($request->transportation_class == "Economy"){
                    $transportation_amount = $AirPlaneTicket->economy;
                } else if($request->transportation_class == "Business"){
                    $transportation_amount = $AirPlaneTicket->business;
                }
            }
        }
        else if($request->transportation_amount_type == "Other")
        {
            if($request->transportation_amount_type_other == "PP")
            {
                $transportation_amount = $request->transportation_amount;
            }
            else if ($request->transportation_amount_type_other == "Non-PP")
            {
                $transportation_amount = $request->transportation_amount;
                $transportation_amount_qty = $request->transportation_amount_qty;
                $transportation_amount_2 = $request->transportation_amount_2;
                $transportation_amount_qty_2 = $request->transportation_amount_qty_2;
            }
        }

        //taxi amount
        if($request->with_taxi == 'Y') {
            //departure
            if($request->taxi_amount_departure_type == "Default")
            {
                // get price from db
                $taxiTicket = TaxiFee::find($request->taxi_ticket_departure);
                $taxi_amount_departure = $taxiTicket->fee;
            }
            else if($request->taxi_amount_departure_type == "Other")
            {
                $taxi_amount_departure = $request->taxi_amount_departure;
            }

            $taxi_amount_departure*=$request->taxi_qty_departure;

            //arrival
            if($request->taxi_amount_arrival_type == "Default")
            {
                // get price from db
                $taxiTicket = TaxiFee::find($request->taxi_ticket_arrival);
                $taxi_amount_arrival = $taxiTicket->fee;
            }
            else if($request->taxi_amount_arrival_type == "Other")
            {
                $taxi_amount_arrival = $request->taxi_amount_arrival;
            }

            $taxi_amount_arrival*=$request->taxi_qty_arrival;

            $taxi_total_amount = $taxi_amount_departure + $taxi_amount_arrival;
        }

        //hotel amount
        if($request->with_hotel == 'Y')
        {
            if($request->hotel_amount_type == "Default")
            {
                // get price from db
                $hotelTicket = HotelFee::find($request->hotel_ticket_id);
                if($employeeClass == "E1") {
                    $hotel_amount = $hotelTicket->eselon1;
                } else if($employeeClass == "E2") {
                    $hotel_amount = $hotelTicket->eselon2;
                } else if($employeeClass == "E3" || $employeeClass == "G4") {
                    $hotel_amount = $hotelTicket->eselon3;
                } else if($employeeClass == "E4" || $employeeClass == "G3") {
                    $hotel_amount = $hotelTicket->eselon4;
                } else if($employeeClass == "G1" || $employeeClass == "G2") {
                    $hotel_amount = $hotelTicket->golongan1_2;
                }
            } else if($request->hotel_amount_type == "Other") {
                $hotel_amount = $request->hotel_amount;
            }
            $dayDuration = 1;
            if($request->hotel_day_duration > 0){
                $dayDuration = $request->hotel_day_duration;
            }
            $hotel_amount*=$dayDuration;
        }

        //hotel in charges
        if($request->hotel_in_cash == 'Y')
        {
            $hotel_in_cash_amount = ($hotel_amount * 30 / 100);
//            $hotel_amount = $hotel_in_cash_amount;
        }

        //vehicle rent amount
        if($request->with_vehicle_rent == 'Y')
        {
            if($request->vehicle_amount_type == "Default")
            {
                // get price from db
                $vehicleRent = VehicleRent::find($request->vehicle_rent_price_id);
                if($request->vehicle_rent_type == "Car") {
                    $vehicle_amount = $vehicleRent->car;
                } else if($request->vehicle_rent_type == "Mid Bus") {
                    $vehicle_amount = $vehicleRent->midle_bus;
                } else if($request->vehicle_rent_type == "Big Bus") {
                    $vehicle_amount = $vehicleRent->big_bus;
                }
            } else if($request->vehicle_amount_type == "Other") {
                $vehicle_amount = $request->vehicle_amount;
            }
        }

        $total_amount = (($transportation_amount * $transportation_amount_qty) + ($transportation_amount_2 * $transportation_amount_qty_2)) + ($request->hotel_in_cash == 'Y' ? $hotel_in_cash_amount : $hotel_amount) + $vehicle_amount + $taxi_total_amount + $type_amount;

        $pd = PerjalananDinas::CreateOrUpdate($request->id);
//        $pd->code = $pd->code != "" ? $pd->code : $code;
        $pd2 = PerjalananDinas::where('code',$request->code)->first();

        if($pd2 != null)
        {
            $pd->sequence = $pd2->sequence;
            $pd->employee_id = $pd2->employee_id;
            $pd->urutan_report = $pd2->urutan_report?:0;
            $pd->tgl_surat_perintah =  $pd2->tgl_surat_perintah;
        }
        else
        {
            $pd->sequence = $pd->sequence != "" ? $pd->sequence : ( $this->getLastData() && $this->getLastData()->sequence > 0 ?  $this->getLastData()->sequence+1 : 1);
            $pd->employee_id = $request->employee_id;
            $pd->urutan_report = $request->urutan_report?:0;
            $pd->tgl_surat_perintah = date('Y-m-d',strtotime($request->tgl_surat_perintah));
        }

        $pd->code = $request->code;
        $pd->no_surat_perintah = $request->no_surat_perintah;
        $kodeakun = KodeAkun::find($request->kode_akun_id);
        $pd->kegiatan = "{$kodeakun->code} ({$kodeakun->type})";
        $pd->kode_akun_id = $request->kode_akun_id;
//        $pd->sub_kegiatan = $request->sub_kegiatan;
        $pd->description = $request->description;
        $pd->remarks = $request->remarks;
        $pd->date_departure = date('Y-m-d',strtotime($request->date_departure));
        $pd->date_arrival = date('Y-m-d',strtotime($request->date_arrival));
        $pd->day_duration = $request->day_duration;
        $pd->type_code = $request->type_code;
        $pd->departure_place_id = $request->departure_place_id;
        $pd->arrival_place_id = $request->arrival_place_id;
        $pd->perjalanan_amount_type = $request->perjalanan_amount_type;

        //start transportation
        $pd->transportation_type = $request->transportation_type;
        $pd->transportation_amount_type_other = $request->transportation_amount_type_other;
        $pd->transportation_class = $request->transportation_class;
        $pd->airplane_ticker_id = $request->airplane_ticker_id;
        $pd->transportation_amount_type = $request->transportation_amount_type;
        $pd->transportation_amount = $transportation_amount;
        $pd->transportation_amount_qty = $transportation_amount_qty;
        $pd->transportation_amount_2 = $transportation_amount_2;
        $pd->transportation_amount_qty_2 = $transportation_amount_qty_2;
        //end trasportation

        //start taxi
        $pd->with_taxi = $request->with_taxi;
        $pd->taxi_qty_departure = $request->taxi_qty_departure;
        $pd->taxi_qty_arrival = $request->taxi_qty_arrival;
        $pd->taxi_ticket_departure = $request->taxi_ticket_departure;
        $pd->taxi_ticket_arrival = $request->taxi_ticket_arrival;
        $pd->taxi_amount_departure_type = $request->taxi_amount_departure_type;
        $pd->taxi_amount_arrival_type = $request->taxi_amount_arrival_type;
        $pd->taxi_amount_departure = $taxi_amount_departure;
        $pd->taxi_amount_arrival = $taxi_amount_arrival;
        $pd->taxi_total_amount = $taxi_total_amount;
        //end taxi

        //start hotel
        $pd->with_hotel = $request->with_hotel;
        $pd->hotel_ticket_id = $request->hotel_ticket_id;
        $pd->hotel_day_duration = $request->hotel_day_duration;
        $pd->hotel_amount_type = $request->hotel_amount_type;
        $pd->hotel_amount = $hotel_amount;
        $pd->hotel_in_cash = $request->hotel_in_cash ?  $request->hotel_in_cash : 'N';
        $pd->hotel_in_cash_amount = $hotel_in_cash_amount;
        //end hotel

        //start vehicle
        $pd->with_vehicle_rent = $request->with_vehicle_rent;
        $pd->vehicle_rent_type = $request->vehicle_rent_type;
        $pd->vehicle_rent_price_id = $request->vehicle_rent_price_id;
        $pd->vehicle_amount_type = $request->vehicle_amount_type;
        $pd->vehicle_amount = $vehicle_amount;
        //end vehicle

        $pd->type_amount = $type_amount_db;
        $pd->total_amount = $total_amount;

        if($pd->save()){
            $this->pushNotification($pd->id,[$pd->employee_id]);
            $request->session()->flash('success_message', 'Data Perjalanan ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Perjalanan ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
//        return response()->json($request); //temp

    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = PerjalananDinas::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }
    //push notification
    private function pushNotification($id,$forwarded_user_id)
    {
        define('URL_API','http://103.200.7.167:1701/jakban/mobile/push/dinas/'.$id);

        $headers = array
        (
            'Content-Type: application/json'
        );

        if(isset($forwarded_user_id))
        {
            try
            {
                foreach ($forwarded_user_id as $frwd)
                {
                    $data = DB::table('user_mobile')->where('id',$frwd)->first();
                    if( count($data) > 0){
                        $fields = [
                            [
                                'token'=> $data->token
                            ]
                        ];

                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, URL_API);
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $res = curl_exec($ch );
                        curl_close( $ch );

                    }
                }
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }
    public function report(Request $request, $id = 0) {
        if($request->type=='surat_perintah') {
            $perjalananDinas = PerjalananDinas::find($id);
            if(!$perjalananDinas->Employee()) {
                $request->session()->flash('error_message', 'Data Pegawai Tidak Ada!');
                return redirect()->back();
            }
            $data = [
                'data' => $perjalananDinas,
            ];
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('perjalanandinas.print', $data);
                return $pdf->download('perjalanandinas.pdf');
            }
            return view('perjalanandinas.print',$data);
        } else if($request->type=='nominatif') {
            $perjalananDinas = PerjalananDinas::select('*')->where([
                'date_departure' => $request->date_departure,
                'date_arrival' => $request->date_arrival,
//                    'departure_place_id' => $request->departure_place_id,
                'arrival_place_id' => $request->arrival_place_id,
                'day_duration' => $request->day_duration,
                'no_surat_perintah' => $request->no_surat_perintah,
            ]);

            $perjalananDinas->orderby('urutan_report','asc');

            if(count($perjalananDinas->get()) == 0){
                $request->session()->flash('error_message', 'Data kosong!');
                return redirect()->back();
            }
            $data = [
                'data' => $perjalananDinas->get(),
            ];
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('perjalanandinas.print_nominatif', $data)->setPaper('a4', 'landscape');
                return $pdf->download('perjalanandinas_print_nominatif.pdf');
            }
            return view('perjalanandinas.print_nominatif',$data);
        }
    }
    private function getLastData()
    {
        $data_perjalanan_dinas = PerjalananDinas::orderBy('sequence','desc');
        $data_perjalanan_dinas->whereYear('created_at','=',Carbon::now()->year);
//        $data_perjalanan_dinas->whereMonth('created_at','=',Carbon::now()->month);
        return $data_perjalanan_dinas->first();
    }
    private function generateCode($date = null, $format = "code")
    {
       return generate_code('$N/SPPD/$M/$Y',new PerjalananDinas(), $date,[
            'SEQ_COLUMN' => 'sequence',
            'DATE_COLUMN' => 'created_at',
            'OUTPUT' => $format,
        ]);
    }
    private function dataExits($key,$column="code",$id=0){
        $data = PerjalananDinas::select('*');
        if($id > 0) {
            $data->where($column,$key)->whereNotIn('id',[$id]);
        } else {
            $data->where($column,$key);
        }

        if(count($data->get())>0) {
            return true;
        }

        return false;
    }
}
