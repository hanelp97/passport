<?php

namespace App\Http\Controllers;

use App\AirPlaneTicket;
use App\KabKota;
use App\Provinsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TikepesawatController extends Controller
{
    public function index(Request $request)
    {
        $data_tikets = AirPlaneTicket::select('*');
        if($request->city_first_id){
            $data_tikets->where('city_first_id',$request->city_first_id);
        }
        if($request->city_destination_id){
            $data_tikets->where('city_destination_id',$request->city_destination_id);
        }
        $data =  [
            'data_tikets' => $data_tikets->paginate(10)
        ];
        return view('databasesettings.tiket_pesawat.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.tiket_pesawat.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => AirPlaneTicket::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.tiket_pesawat.form',$data);
    }
    public function save(Request $request) {
        if($request->city_first_id == $request->city_destination_id) {
            $request->session()->flash('error_message', 'Asal dan Tujuan tidak bisa sama!');
            return redirect()->route('TiketPesawat::Index');
        }

        if($request->city_first_id == "" || $request->city_destination_id == "" || $request->business == "" || $request->economy == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->route('TiketPesawat::Index');
        }

        $tiket = AirPlaneTicket::CreateOrUpdate($request->id);
        $tiket->city_first_id = $request->city_first_id;
        $tiket->city_destination_id = $request->city_destination_id;
        $tiket->business = $request->business;
        $tiket->economy = $request->economy;

        if($tiket->save()){
            $request->session()->flash('success_message', 'Tiket ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Tiket ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = AirPlaneTicket::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Ticket Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
