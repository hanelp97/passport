<?php

namespace App\Http\Controllers;

use App\BiayaPerjalananDinas;
use Illuminate\Http\Request;

class BiayaPerjalananDinasCtrl extends Controller
{
    public function index(Request $request)
    {
        $data_biaya_perjalanan_dinas = BiayaPerjalananDinas::select('*');
        if($request->unit){
            $data_biaya_perjalanan_dinas->orwhere('unit','like','%'.$request->unit.'%');
        }
        $data =  [
            'data_biaya_perjalanan_dinas' => $data_biaya_perjalanan_dinas->paginate(10)
        ];
        return view('databasesettings.biaya_perjalanan_dinas.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.biaya_perjalanan_dinas.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => BiayaPerjalananDinas::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.biaya_perjalanan_dinas.form',$data);
    }
    public function save(Request $request) {
        if($request->provinsi_id == "" || $request->unit == "" || $request->out_city == "" || $request->in_city == "" || $request->diklat == "") {
            $request->session()->flash('error_message', 'Field *s Wajib Diisi!');
            return redirect()->route('BiayaPerjalananDinas::Index');
        }

        $biaya_perjalanan_dinas = BiayaPerjalananDinas::CreateOrUpdate($request->id);
        $biaya_perjalanan_dinas->provinsi_id = $request->provinsi_id;
        $biaya_perjalanan_dinas->unit = $request->unit;
        $biaya_perjalanan_dinas->out_city = $request->out_city;
        $biaya_perjalanan_dinas->in_city = $request->in_city;
        $biaya_perjalanan_dinas->diklat = $request->diklat;

        if($biaya_perjalanan_dinas->save()){
            $request->session()->flash('success_message', 'Data Biaya Perjalanan Dinas ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Biaya Perjalanan Dinas ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = BiayaPerjalananDinas::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Biaya Perjalanan Dinas Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
