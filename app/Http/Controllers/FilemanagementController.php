<?php

namespace App\Http\Controllers;

use App\File;
use App\Folder;
use App\Helpers\main_helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class FilemanagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $data_folders = Folder::select('*');
        $data_files = File::select('*');
        $data_folder = null;
        if($request->folder > 0 || $request->folder != "") {
            $data_folders->where('parent_id',$request->folder);
            $data_files->where('folder_id',$request->folder);
            $data_folder = Folder::find($request->folder);
        } else {
            $data_folders->where('parent_id',0);
            $data_files->where('folder_id',0);
        }
        $data = [
            'data_folders' => $data_folders->get(),
            'data_folder' => $data_folder,
            'data_files' => $data_files->get(),
        ];
        return view('filemanagement.index',$data);
    }

    public function add_folder(Request $request) {
        $data = [
            'mode' => 'add'
        ];
        return view('filemanagement.partial.folder.form',$data);
    }

    public function edit_folder(Request $request) {
        $data = [
            'data_folder' => Folder::find($request->id),
            'mode' => 'edit'
        ];
        return view('filemanagement.partial.folder.form',$data);
    }

    public function save_folder(Request $request) {
        //check parent
        if($request->parent_id > 0) {
            $parent = Folder::find($request->parent_id);
            if(!$parent) {
                $request->session()->flash('error_message', 'Parent Folder Not Found!');
                return redirect()->route('FileManagement::Index');
            }
        }
        $folder = Folder::CreateOrUpdate($request->id);
        $folder->folder = $request->folder;
        if(!$request->id) {
            $folder->parent_id = $request->parent_id ? : 0;
        }
        if($folder->save()){
            $request->session()->flash('success_message', 'Folder ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Folder ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }

    public function delete_folder_page(Request $request) {
        $data = [
            'data_folder' => Folder::find($request->id),
            'mode' => 'edit'
        ];
        return view('filemanagement.partial.folder.delete',$data);
    }

    public function delete_folder(Request $request) {
        DB::beginTransaction();
        try {
            $data = Folder::find($request->id);
            $parent_id = $data->parent_id;
            \App\Helpers\folder::Delete($request->id);
            $request->session()->flash('success_message', 'Folder Deleted !');
            DB::commit();

            $data = [
                'status_action' => 'success',
                'parent_id' => $parent_id,
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }

    public function upload_page(Request $request) {
        $data = [
            'mode' => 'add'
        ];
        return view('filemanagement.partial.file.form',$data);
    }

    public function upload_file(Request $request) {
        $getFile = $request->file('file');
        $getOriginalFileName = $getFile->getClientOriginalName();
        $filename = pathinfo($getOriginalFileName, PATHINFO_FILENAME);
        $extension = pathinfo($getOriginalFileName, PATHINFO_EXTENSION);

        $files                   = new File();
        $files->folder_id        = $request->folder_id?:0;
        $files->permalink        = $this->create_permalink(main_helper::convert_to_permalink($filename));
        $files->ext              = $extension;
        $files->size             = $getFile->getClientSize();
        $newName = $files->permalink."_".date('Ymdhis');

        $upload = main_helper::upload($getFile,$newName);

        if($upload) {
            $files->file = $newName.'.'.$getFile->getClientOriginalExtension();
            if($files->save()) {
                $request->session()->flash('success_message', 'File Uploaded !');
                return redirect()->back();
            }
        }
        $request->session()->flash('error_message', 'File Upload Error!');
        return redirect()->back();
    }

    public function download_file($file) {
        if($file) {
            return main_helper::download_file($file);
        }
        return false;
    }

    public function delete_file(Request $request) {
        DB::beginTransaction();
        try {
            $data = File::find($request->id);
            $folder_id = $data->folder_id;
            main_helper::delete_file($data->file);
            $data->delete();
            $request->session()->flash('success_message', 'File Deleted !');
            DB::commit();

            $data = [
                'status_action' => 'success',
                'folder_id' => $folder_id,
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }

    private function create_permalink($text,$id=0){
        $i=1;
        $permalink = $text;
        while($this->permalink_exits($text,$id)){
            $permalink = $text."-".$i++;
            if(!$this->permalink_exits($permalink,$id)){
                break;
            }
        }
        return $permalink;
    }
    private function permalink_exits($text,$id = 0){
        $file = File::where('permalink',$text)->get();
        if($id > 0) {
            $file = File::where('permalink',$text)->whereNotIn('id',[$id])->get();
        }
        if(count($file)>0) {
            return true;
        }
        return false;
    }
}
