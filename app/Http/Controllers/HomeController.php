<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('passport.index');
    }
    public function schedule()
    {
        $dataSchedules = Schedule::orderBy('datetime_schedule_start','desc');
        $data = [
            'data_schedules' => $dataSchedules->paginate(30)
        ];
        return view('schedule',$data);
    }
}
