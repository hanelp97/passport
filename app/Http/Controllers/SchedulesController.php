<?php

namespace App\Http\Controllers;

use App\File;
use App\Folder;
use App\Forwarder;
use App\Helpers\main_helper;
use App\Schedule;
use App\ScheduleCategory;
use App\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data_schedules = Schedule::select('*');
        if($request->filter){
            $data_schedules->where(function ($q) use ($request) {
                if($request->get('datetime_schedule_start') != "") {
                    $q->orWhere('datetime_schedule_start',$request->get('datetime_schedule_start'));
                }
                if($request->get('datetime_schedule_end') != "") {
                    $q->orWhere('datetime_schedule_end',$request->get('datetime_schedule_end'));
                }

                if($request->get('month') != "") {
                    $q->orWhere(function ($q) use ($request) {
                        $q->whereMonth('datetime_schedule_start','=',$request->get('month'));
                    });
                    $q->orWhere(function ($q) use ($request) {
                        $q->whereMonth('datetime_schedule_end','=',$request->get('month'));
                    });
                }
            });
        }
//        $data_schedules->whereRaw("JSON_CONTAINS(forward_to_user_id, '[".Auth::user()->id."]' )");
        $data_schedules->orderby('id','desc');
        $data = [
            'data_schedules' => $data_schedules->paginate(10)
        ];
        return view('schedules.index',$data);
    }

    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
            'data_categories' => ScheduleCategory::all(),
            'data_folders' => Folder::all(),
//            'data_user_employees' => User::WhereRoleIs('pegawai')->get(),
            'data_user_employees' => User::all(),
            'data_forwarders' => Forwarder::all(),
        ];
        return view('schedules.form',$data);
    }

    public function edit_page(Request $request) {
        $data = [
            'mode' => 'edit',
            'data' => Schedule::find($request->id),
            'data_categories' => ScheduleCategory::all(),
            'data_folders' => Folder::all(),
//            'data_user_employees' => User::WhereRoleIs('pegawai')->get(),
            'data_user_employees' => User::all(),
            'data_forwarders' => Forwarder::all(),
        ];
        return view('schedules.form',$data);
    }

    public function save(Request $request) {
        if($request->from == "" || $request->title == ""
            || $request->place == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        $schedule = Schedule::CreateOrUpdate($request->id);
        $schedule->category_id = $request->category_id;
        $schedule->from = $request->from;
        $schedule->title = $request->title;
        $schedule->datetime_schedule_start = date('Y-m-d H:i:s',strtotime($request->date_schedule_start." ".$request->time_schedule_start));
//        $schedule->datetime_schedule_end = date('Y-m-d H:i:s',strtotime($request->date_schedule_end." ".$request->time_schedule_end));
        $schedule->datetime_schedule_end = date('Y-m-d H:i:s');
        $schedule->place = $request->place;
        $schedule->desc = $request->desc;
        $schedule->notes = $request->notes;
        $schedule->forward_to_user_id = array_map('intval', array_unique(array_merge(User::all()->pluck('id')->toArray(),[Auth::user()->id]), SORT_REGULAR));
        $schedule->forward_to_forwarder = array_map('intval', array_unique($request->forward_to_forwarder));
        $schedule->class = $request->class;
        $schedule->folder_id = $request->folder_id;
        $schedule->no_surat_perintah = $request->no_surat_perintah?$request->no_surat_perintah:"";
        $schedule->tgl_diterima = date('Y-m-d',strtotime($request->tgl_diterima));
        $schedule->no_agenda = $request->no_agenda;

        $getFile = $request->file('file');

        if($request->id && $getFile && $schedule->file_id > 0) {
            //delete previous file
            $this->delete_file($schedule->file_id);
        }

        if($getFile) {
            $getOriginalFileName = $getFile->getClientOriginalName();
            $filename = pathinfo($getOriginalFileName, PATHINFO_FILENAME);
            $extension = pathinfo($getOriginalFileName, PATHINFO_EXTENSION);

            $files                   = new File();
            $files->folder_id        = $request->folder_id;
            $files->permalink        = $this->create_permalink(main_helper::convert_to_permalink($filename));
            $files->ext              = $extension;
            $files->size             = $getFile->getClientSize();
            $newName = $files->permalink."_".date('Ymdhis');

            $upload = main_helper::upload($getFile,$newName);

            if($upload) {
                $files->file = $newName.'.'.$getFile->getClientOriginalExtension();
                if(!$files->save()) {
                    $request->session()->flash('error_message', 'File Upload Error !');
                    return redirect()->back();
                }
                $schedule->file_id = $files->id;
            }
        }

        if($schedule->save()){
            try
            {
                $this->pushNotification($schedule->id,User::all()->pluck('id'));
                //            $this->pushNotification($schedule->id,$schedule->forward_to_user_id);
            }
            catch (Exception $ex)
            {

            }
            $request->session()->flash('success_message', 'Data Schedule ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Schedule ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = Schedule::find($request->id);
            if($data->file_id > 0) {
                $this->delete_file($data->file_id);
            }
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }

    //push notification
    private function pushNotification($id,$forwarded_user_id)
    {
        define('URL_API','http://103.200.7.167:1701/jakban/mobile/push/meeting/'.$id);

        $headers = array
        (
            'Content-Type: application/json'
        );

        if(isset($forwarded_user_id))
        {
            try
            {
                foreach ($forwarded_user_id as $frwd)
                {
                    $data = DB::table('user_mobile')->where('id',$frwd)->first();
                    if( count($data) > 0){
                        $fields = [
                            [
                                'token'=> $data->token
                            ]
                        ];

                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, URL_API);
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $res = curl_exec($ch );
                        curl_close( $ch );

                    }
                }
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }

    //file
    private function create_permalink($text,$id=0){
        $i=1;
        $permalink = $text;
        while($this->permalink_exits($text,$id)){
            $permalink = $text."-".$i++;
            if(!$this->permalink_exits($permalink,$id)){
                break;
            }
        }
        return $permalink;
    }
    private function permalink_exits($text,$id = 0){
        $file = File::where('permalink',$text)->get();
        if($id > 0) {
            $file = File::where('permalink',$text)->whereNotIn('id',[$id])->get();
        }
        if(count($file)>0) {
            return true;
        }
        return false;
    }
    private function delete_file($id) {
        $data = File::find($id);
        main_helper::delete_file($data->file);
        $data->delete();
    }
    public function report(Request $request, $id = 0)
    {
        if ($request->type == 'unit') {
            $schedule = Schedule::find($id);
            $data = [
                'data' => $schedule,
                'data_forwarders' => Forwarder::all(),
            ];
            //        if($request->output == 'pdf'){
//            $pdf = PDF::loadView('dispositions.print', $data);
//            return $pdf->download('dispositions.pdf');
//
            return view('schedules.print', $data);

        } else if ($request->type == 'all') {
            $data_schedules = Schedule::select('*');
            if($request->filter){
                $data_schedules->where(function ($q) use ($request) {
                    if($request->get('datetime_schedule_start') != "") {
                        $q->orWhere('datetime_schedule_start',$request->get('datetime_schedule_start'));
                    }
                    if($request->get('datetime_schedule_end') != "") {
                        $q->orWhere('datetime_schedule_end',$request->get('datetime_schedule_end'));
                    }

                    if($request->get('month') != "") {
                        $q->orWhere(function ($q) use ($request) {
                            $q->whereMonth('datetime_schedule_start','=',$request->get('month'));
                        });
                        $q->orWhere(function ($q) use ($request) {
                            $q->whereMonth('datetime_schedule_end','=',$request->get('month'));
                        });
                    }
                });
            }
            $data_schedules->orderby('id','desc');
            $data = [
                'data_schedules' => $data_schedules->paginate(10)
            ];
            return view('schedules.print-all',$data);
        }
    }
}
