<?php

namespace App\Http\Controllers;

use App\File;
use App\Folder;
use App\Forwarder;
use App\Helpers\main_helper;
use App\Disposition;

use App\ScheduleCategory;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DispositionCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data_dispositions = Disposition::select('*');
        if($request->filter){
            $data_dispositions->where(function ($q) use ($request) {
                if($request->get('created_at') != "") {
                    $q->orWhere('created_at',$request->get('created_at'));
                }

                if($request->get('month') != "") {
                    $q->orWhere(function ($q) use ($request) {
                        $q->whereMonth('created_at','=',$request->get('month'));
                    });
                    $q->orWhere(function ($q) use ($request) {
                        $q->whereMonth('created_at','=',$request->get('month'));
                    });
                }
            });
        }
//        $data_dispositions->whereRaw("JSON_CONTAINS(forward_to_user_id, '[".Auth::user()->id."]' )");
        $data_dispositions->orderby('id','desc');

        $data = [
            'data_dispositions' => $data_dispositions->paginate(10)
        ];
        return view('dispositions.index',$data);
    }

    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
            'data_categories' => ScheduleCategory::all(),
            'data_folders' => Folder::all(),
//            'data_user_employees' => User::WhereRoleIs('pegawai')->get(),
            'data_user_employees' => User::all(),
            'data_forwarders' => Forwarder::all(),
        ];
        return view('dispositions.form',$data);
    }

    public function edit_page(Request $request) {
        $data = [
            'mode' => 'edit',
            'data' => Disposition::find($request->id),
            'data_categories' => ScheduleCategory::all(),
            'data_folders' => Folder::all(),
//            'data_user_employees' => User::WhereRoleIs('pegawai')->get(),
            'data_user_employees' => User::all(),
            'data_forwarders' => Forwarder::all(),
        ];
        return view('dispositions.form',$data);
    }

    public function save(Request $request) {
        if($request->from == "" || $request->title == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        $disposition = Disposition::CreateOrUpdate($request->id);
        $disposition->category_id = $request->category_id;
        $disposition->no_surat_perintah = $request->no_surat_perintah ? $request->no_surat_perintah : "";
        $disposition->tgl_diterima = date('Y-m-d',strtotime($request->tgl_diterima));
        $disposition->no_agenda = $request->no_agenda;
        $disposition->from = $request->from;
        $disposition->title = $request->title;
        $disposition->desc = $request->desc;
        $disposition->notes = $request->notes;
        $disposition->forward_to_user_id = array_map('intval', array_unique(array_merge(User::all()->pluck('id')->toArray(),[Auth::user()->id]), SORT_REGULAR));
        $disposition->forward_to_forwarder = array_map('intval', array_unique($request->forward_to_forwarder));
        $disposition->class = $request->class;
        $disposition->folder_id = $request->folder_id;

        $getFile = $request->file('file');

        if($request->id && $getFile && $disposition->file_id > 0) {
            //delete previous file
            $this->delete_file($disposition->file_id);
        }

        if($getFile) {
            $getOriginalFileName = $getFile->getClientOriginalName();
            $filename = pathinfo($getOriginalFileName, PATHINFO_FILENAME);
            $extension = pathinfo($getOriginalFileName, PATHINFO_EXTENSION);

            $files                   = new File();
            $files->folder_id        = $request->folder_id;
            $files->permalink        = $this->create_permalink(main_helper::convert_to_permalink($filename));
            $files->ext              = $extension;
            $files->size             = $getFile->getClientSize();
            $newName = $files->permalink."_".date('Ymdhis');

            $upload = main_helper::upload($getFile,$newName);

            if($upload) {
                $files->file = $newName.'.'.$getFile->getClientOriginalExtension();
                if(!$files->save()) {
                    $request->session()->flash('error_message', 'File Upload Error !');
                    return redirect()->back();
                }
                $disposition->file_id = $files->id;
            }
        }

        if($disposition->save()){
            $this->pushNotification($disposition->id,$disposition->forward_to_user_id);
            $request->session()->flash('success_message', 'Data Disposition ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Disposition ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = Disposition::find($request->id);
            if($data->file_id > 0) {
                $this->delete_file($data->file_id);
            }
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }

    //push notification
    private function pushNotification($id,$forwarded_user_id)
    {
        define('URL_API','http://103.200.7.167:1701/jakban/mobile/push/disposisi/'.$id);

        $headers = array
        (
            'Content-Type: application/json'
        );

        if(isset($forwarded_user_id))
        {
            try
            {
                foreach ($forwarded_user_id as $frwd)
                {
                    $data = DB::table('user_mobile')->where('id',$frwd)->first();
                    if( count($data) > 0){
                        $fields = [
                            [
                                'token'=> $data->token
                            ]
                        ];

                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, URL_API);
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $res = curl_exec($ch );
                        curl_close( $ch );

                    }
                }
                return true;
            }
            catch (\Exception $e)
            {
                return false;
            }
        }
    }

    //file
    private function create_permalink($text,$id=0){
        $i=1;
        $permalink = $text;
        while($this->permalink_exits($text,$id)){
            $permalink = $text."-".$i++;
            if(!$this->permalink_exits($permalink,$id)){
                break;
            }
        }
        return $permalink;
    }
    private function permalink_exits($text,$id = 0){
        $file = File::where('permalink',$text)->get();
        if($id > 0) {
            $file = File::where('permalink',$text)->whereNotIn('id',[$id])->get();
        }
        if(count($file)>0) {
            return true;
        }
        return false;
    }
    private function delete_file($id) {
        $data = File::find($id);
        main_helper::delete_file($data->file);
        $data->delete();
    }
    public function report(Request $request, $id = 0) {
        if ($request->type == 'unit') {
            $disposition = Disposition::find($id);
            $data = [
                'data' => $disposition,
                'data_forwarders' => Forwarder::all(),
            ];
            //        if($request->output == 'pdf'){
//            $pdf = PDF::loadView('dispositions.print', $data);
//            return $pdf->download('dispositions.pdf');
//
            return view('dispositions.print', $data);

        } else if ($request->type == 'all') {
            $data_dispositions = Disposition::select('*');
            if($request->filter){
                $data_dispositions->where(function ($q) use ($request) {
                    if($request->get('created_at') != "") {
                        $q->orWhere('created_at',$request->get('created_at'));
                    }

                    if($request->get('month') != "") {
                        $q->orWhere(function ($q) use ($request) {
                            $q->whereMonth('created_at','=',$request->get('month'));
                        });
                        $q->orWhere(function ($q) use ($request) {
                            $q->whereMonth('created_at','=',$request->get('month'));
                        });
                    }
                });
            }
            $data_dispositions->orderby('id','desc');
            $data = [
                'data_dispositions' => $data_dispositions->paginate(10)
            ];
            return view('dispositions.print-all',$data);
        }
    }
}
