<?php

namespace App\Http\Controllers;

use App\TaxiFee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaxiController extends Controller
{
    public function index(Request $request)
    {
        $data_taxi_tickets = TaxiFee::select('*');
        if($request->unit){
            $data_taxi_tickets->orwhere('unit','like','%'.$request->unit.'%');
        }
        $data =  [
            'data_taxi_tickets' => $data_taxi_tickets->paginate(10)
        ];
        return view('databasesettings.taxi.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.taxi.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => TaxiFee::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.taxi.form',$data);
    }
    public function save(Request $request) {
        if($request->provinsi_id == "" || $request->unit == "" || $request->fee == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->route('Taxi::Index');
        }

        $taxi = TaxiFee::CreateOrUpdate($request->id);
        $taxi->provinsi_id = $request->provinsi_id;
        $taxi->unit = $request->unit;
        $taxi->fee = $request->fee;

        if($taxi->save()){
            $request->session()->flash('success_message', 'Data Taxi ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Taxi ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = TaxiFee::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Taxi Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
