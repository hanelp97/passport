<?php

namespace App\Http\Controllers;

use App\KodeAkunKategori;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KodeAkunKategoriCtrl extends Controller
{
    public function index(Request $request)
    {
        $data_kode_akun_kategoris = KodeAkunKategori::select('*');

        $data =  [
            'data_kode_akun_kategoris' => $data_kode_akun_kategoris->paginate(10)
        ];

        return view('databasesettings.kode_akun_kategori.index',$data);
    }

    public function add_page(Request $request)
    {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.kode_akun_kategori.form',$data);
    }

    public function edit_page(Request $request)
    {
        $data = [
            'data' => KodeAkunKategori::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.kode_akun_kategori.form',$data);
    }

    public function save(Request $request)
    {
        if($request->name == "") {
            $request->session()->flash('error_message', 'Field *s Wajib Diisi!');
            return redirect()->route('KodeAkunKategori::Index');
        }

        $kode_akun_kategori = KodeAkunKategori::CreateOrUpdate($request->id);
        $kode_akun_kategori->name = $request->name;

        if($kode_akun_kategori->save())
        {
            $request->session()->flash('success_message', 'Data telah di' . (isset($request->id) ? 'edit' : 'buat') . ' !');
            return redirect()->back();
        }

        $request->session()->flash('error_message', 'Data telah di' . (isset($request->id) ? 'edit' : 'buat') . ' Error!');
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();

        try
        {
            $data = KodeAkunKategori::find($request->id);

            if($data->KodeAkuns()->count() > 0)
            {
                throw new \Exception("Tidak bisa menghapus kategori!. Kode akun sudah ada didalam kategori ini!");
            }

            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Data Deleted !');

            $data = [
                'status_action' => 'success',
            ];

            return response()->json($data);
        }
        catch(\Exception $e)
        {
            DB::rollBack();

            $data = [
                'message' => $e->getMessage(),
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
