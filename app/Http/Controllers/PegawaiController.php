<?php

namespace App\Http\Controllers;

use App\Helpers\main_helper;
use App\Role;
use App\User;
use App\UserClass;
use App\UserPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class PegawaiController extends Controller
{
    public function api(Request $request) {
        if($request->action == 'get_employee_by_id') {
            $data_employee = User::where('id',$request->id)->first();
            $data =  [
                'data_employee' => array_merge($data_employee->toArray() ,[
                    'position' => $data_employee->Position(),
                    'class' => $data_employee->Classification(),
                ]),
            ];
            return response()->json($data);
        }
    }
    public function index(Request $request)
    {
        $data_users = User::select('*');
        $data_positions = UserPosition::select('*');
        $data_classes = UserClass::select('*');
        if($request->keyword){
            $data_users->orwhere('name','like','%'.$request->keyword.'%');
            $data_users->orwhere('username','like','%'.$request->keyword.'%');
            $data_users->orwhere('email','like','%'.$request->keyword.'%');
            $data_users->orwhere('NIP','like','%'.$request->keyword.'%');
        }
        $data_users->orderBy('id','desc');
        $data =  [
            'data_users' => $data_users->paginate(10),
            'data_positions' => $data_positions->get(),
            'data_classes' => $data_classes->get(),
        ];
        return view('usersetting.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
            'data_roles' => Role::all(),
            'data_positions' => UserPosition::all(),
            'data_classes' => UserClass::all(),
        ];
        return view('usersetting.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => User::find($request->id),
            'data_roles' => Role::all(),
            'data_positions' => UserPosition::all(),
            'data_classes' => UserClass::all(),
            'mode' => 'edit',
        ];
        return view('usersetting.form',$data);
    }
    public function save(Request $request) {
        if($request->role_id == "" || $request->name == "" || $request->username == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        if($request->id == 0) {
            if($request->password  == ""){
                $request->session()->flash('error_message', 'Password wajib diisi!');
                return redirect()->back()->withInput();
            }
        }

        if($request->email) {
            if($this->user_exits($request->email,"email",$request->id)) {
                $request->session()->flash('error_message','Email Exits!');
                return redirect()->back()->withInput();
            }
            if($this->user_exits($request->username,"username",$request->id)) {
                $request->session()->flash('error_message','Username Exits!');
                return redirect()->back()->withInput();
            }
        }

        $user = User::CreateOrUpdate($request->id);
        $user->name = $request->name;
        $user->username = str_replace(" ","",$request->username);
        $user->email = $request->email?$request->email : null;
        $user->password = bcrypt($request->password);
        $user->NIP = $request->NIP;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->position_id = $request->position_id;
        $user->class_id = $request->class_id;
        $user->birth = date('Y-m-d',strtotime($request->birth));

        $getPhoto = $request->file('photo');

        if($getPhoto) {
            $getOriginalFileName = $getPhoto->getClientOriginalName();

            $newName = "photo_profile_".$user->username.date('Ymdhis');

            $upload = main_helper::upload($getPhoto,$newName);

            if($upload) {
                $user->photo = $newName.'.'.$getPhoto->getClientOriginalExtension();
            }
        }

        if($user->save()){
            $user->roles()->detach([$request->role_id, $user->id]);
            $user->syncRoles([$request->role_id, $user->id]);
            $request->session()->flash('success_message', 'Data User ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data User ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            if(\Auth::user()->id == $request->id) {
                throw new Exception("Tidak Bisa menghapus user yang sedang login!");
            }
            $data = User::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }

    public function add_jabatan_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('usersetting.jabatan.form',$data);
    }
    public function edit_jabatan_page(Request $request) {
        $data = [
            'data' => UserPosition::find($request->id),
            'mode' => 'edit',
        ];
        return view('usersetting.jabatan.form',$data);
    }
    public function save_jabatan(Request $request) {
        if($request->position == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        $p = UserPosition::CreateOrUpdate($request->id);
        $p->position = $request->position;
        if($p->save()){
            $request->session()->flash('success_message', 'Data Jabatan ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Jabatan ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy_jabatan(Request $request) {
        DB::beginTransaction();
        try {
            if(\Auth::user()->position_id == $request->id) {
                throw new Exception("Tidak Bisa menghapus jabatan yang penjabatnya sedang login!!");
            }
            $data = UserPosition::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }

    public function add_golongan_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('usersetting.golongan.form',$data);
    }
    public function edit_golongan_page(Request $request) {
        $data = [
            'data' => UserClass::find($request->id),
            'mode' => 'edit',
        ];
        return view('usersetting.golongan.form',$data);
    }
    public function save_golongan(Request $request) {
        if($request->class == "" || $request->type == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->back()->withInput();
        }

        $p = UserClass::CreateOrUpdate($request->id);
        $p->class = $request->class;
        $p->type = $request->type;
        if($p->save()){
            $request->session()->flash('success_message', 'Data Golongan ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Golongan ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy_golongan(Request $request) {
        DB::beginTransaction();
        try {
            if(\Auth::user()->class_id == $request->id) {
                throw new Exception("Tidak Bisa menghapus golongan yang penjabatnya sedang login!!");
            }
            $data = UserClass::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', ' Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(\Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false',
                'message' => $e->getMessage(),
            ];
            return response()->json($data);
        }
    }

    private function user_exits($key,$column="username",$id_user=0){
        $user = User::select('*');
        if($id_user > 0) {
            $user->where($column,$key)->whereNotIn('id',[$id_user]);
        } else {
            $user->where($column,$key);
        }

        if(count($user->get())>0) {
            return true;
        }

        return false;
    }
}
