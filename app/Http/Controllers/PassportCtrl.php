<?php

namespace App\Http\Controllers;

use App\Passport;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PassportCtrl extends Controller
{
    public function search()
    {
        if(request()->no_passport == null)
            return redirect('/');

        $data_passport = Passport::where('no_paspor', request()->no_passport)->first();

        if($data_passport == null)
        {
            request()->session()->flash('error','Nomor paspor "'.request()->no_passport.'" tidak ditemukan! <br> <br><a href="'.url('passport/add?no_passport='.request()->no_passport).'">Buat passport</a>');
            return redirect('/');
        }

        $data = [
            'data_passport' => $data_passport
        ];

        return view('passport.show', $data);
    }

    public function add ()
    {
        if(request()->no_passport == null)
            return redirect('/');

        $data_passport = Passport::where('no_paspor', request()->no_passport)->first();

        if($data_passport != null)
        {
            return redirect('/passport/search?no_passport='.request()->no_passport);
        }

        return view('passport.add');
    }

    public function edit ()
    {
        if(request()->no_passport == null)
            return redirect('/');

        $data_passport = Passport::where('no_paspor', request()->no_passport)->first();

        if($data_passport == null)
        {
            request()->session()->flash('error','Nomor paspor "'.request()->no_passport.'" tidak ditemukan!');
            return redirect('/');
        }

        $data = [
            'data_passport' => $data_passport
        ];

        return view('passport.edit', $data);
    }

    public function save()
    {
        if(request()->no_passport != null)
        {
            $passport = Passport::where('no_paspor',request()->no_passport)->first();

            if($passport == null)
                $passport = new Passport();

            $passport->no_paspor  = request()->no_passport;
            $passport->nik = request()->nik;
            $passport->nama_lengkap = request()->nama_lengkap;
            $passport->tempat_lahir = request()->tempat_lahir;
            $passport->tanggal_lahir = Carbon::parse(request()->tanggal_lahir)->toDateString();
            $passport->jenis_kelamin = request()->jenis_kelamin;
            $passport->status_pernikahan = request()->status_pernikahan;
            $passport->alamat_domisili_bahasa_inggris = request()->alamat_domisili_bahasa_inggris;
            $passport->alamat_domisili_bahasa_china = request()->alamat_domisili_bahasa_china;
            $passport->metode_pemungutan_suara = request()->metode_pemungutan_suara;

            if($passport->save())
            {
                request()->session()->flash('success','Passport "'.request()->no_passport.'" Berhasil disimpan!');
                return redirect('passport/search?no_passport='.$passport->no_paspor);
            }
        }
    }
}
