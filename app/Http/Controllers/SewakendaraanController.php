<?php

namespace App\Http\Controllers;

use App\VehicleRent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SewakendaraanController extends Controller
{
    public function index(Request $request)
    {
        $data_vehicle_rents = VehicleRent::select('*');
        if($request->unit){
            $data_vehicle_rents->orwhere('unit','like','%'.$request->unit.'%');
        }
        $data =  [
            'data_vehicle_rents' => $data_vehicle_rents->paginate(10)
        ];
        return view('databasesettings.vehicle_rent.index',$data);
    }
    public function add_page(Request $request) {
        $data = [
            'mode' => 'add',
        ];
        return view('databasesettings.vehicle_rent.form',$data);
    }
    public function edit_page(Request $request) {
        $data = [
            'data' => VehicleRent::find($request->id),
            'mode' => 'edit',
        ];
        return view('databasesettings.vehicle_rent.form',$data);
    }
    public function save(Request $request) {
        if($request->provinsi_id == "" || $request->unit == "" || $request->car == "" || $request->midle_bus == "" || $request->big_bus == "") {
            $request->session()->flash('error_message', 'Field * Wajib Diisi!');
            return redirect()->route('VehicleRent::Index');
        }

        $vehicle_rent = VehicleRent::CreateOrUpdate($request->id);
        $vehicle_rent->provinsi_id = $request->provinsi_id;
        $vehicle_rent->unit = $request->unit;
        $vehicle_rent->car = $request->car;
        $vehicle_rent->midle_bus = $request->midle_bus;
        $vehicle_rent->big_bus = $request->big_bus;

        if($vehicle_rent->save()){
            $request->session()->flash('success_message', 'Data Vehicle ' . (isset($request->id) ? 'Updated' : 'Created') . ' !');
            return redirect()->back();
        }
        $request->session()->flash('error_message', 'Data Vehicle ' . (isset($request->id) ? 'Update' : 'Create') . ' Error!');
        return redirect()->back();
    }
    public function destroy(Request $request) {
        DB::beginTransaction();
        try {
            $data = VehicleRent::find($request->id);
            $data->delete();
            DB::commit();
            $request->session()->flash('success_message', 'Vehicle Deleted !');
            $data = [
                'status_action' => 'success',
            ];
            return response()->json($data);
        } catch(Exception $e) {
            DB::rollBack();
            $data = [
                'status_action' => 'false'
            ];
            return response()->json($data);
        }
    }
}
