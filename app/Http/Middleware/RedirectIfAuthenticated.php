<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

//        if($request->is('administrator/filemanagement'))
//        {
//            if(!in_array(Auth::user()->roles->first()->name,['admin','arsip','pegawai']))
//            {
//                $request->session()->flash('error_message', 'Access Denied');
//                return redirect()->back();
//            }
//        }
//


        return $next($request);
    }
}
