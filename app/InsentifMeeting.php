<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsentifMeeting extends Model
{
    protected $table = 'insentif_meeting';

    public static function CreateOrUpdate($id){
        $obj = static::find($id);
        return $obj ?: new static;
    }

    public function Provinsi() {
        return $this->belongsTo(Provinsi::class,'provinsi_id')->first();
    }
}
