<?php
/**
 * Created by PhpStorm.
 * User: Hanel Prillian
 * Date: 2/5/2016
 * Time: 8:55 PM
 */

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class main_helper {

    public static function convert_to_permalink($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
    /*
    * Return Boolean
    */
    public static function upload($file,$newname){
        $extension = $file->getClientOriginalExtension();
        $path = "file/";
        $upload = $file->move(public_path()."/upload/$path", $newname.'.'.$extension);
        if($upload) {
            return true;
        }
        return false;
    }
    /*
     * Return Boolean
     */
    public static function delete_file($file){
        $path = "file/";
        $status = false;
        if(file_exists(public_path()."/upload/$path$file")) {
            if(unlink(public_path()."/upload/$path$file")) {
                $status = true;
            }
        }
        return $status;
    }
    /*
     * Return Response
     */
    public static function download_file($file) {
        $file= public_path(). "/upload/file/".$file;

        $headers = array(
            'Content-Type: application/octet-stream',
        );

        return response()->download($file);
    }

    public static function indonesiaDate($number,$mode = "date") {
        $hari = array ( 1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        if($mode == "date") {
            return $hari[$number];
        }
        if($mode == "month") {
            return $bulan[$number];
        }
    }
    public static function romanic_number($integer, $upcase = true)
    {
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
        $return = '';
        while($integer > 0)
        {
            foreach($table as $rom=>$arb)
            {
                if($integer >= $arb)
                {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }

        return $return;
    }
    public static function format_nip($number)
    {
//        $return = substr($number,0,8)." ".substr($number,8,6)." ".substr($number,15,1)." ".substr($number,16,3);
        $return = $number;

        return $return;
    }
}

