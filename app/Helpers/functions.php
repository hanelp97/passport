<?php
/**
 * Created by PhpStorm.
 * User: hanelprillian
 * Date: 9/22/2017
 * Time: 8:18 PM
 */

if(!function_exists('setting'))
{
    function setting($name)
    {
        $settings   = \App\Setting::ByName($name)->first();
        return $settings['setting_value'];
    }
}

if(!function_exists('calculate_amount'))
{
    function calculate_amount($id_perjalanan_dinas)
    {
//        $settings   = \App\Setting::ByName($name)->first();
//        return $settings['setting_value'];
    }
}

if(!function_exists('generate_code'))
{
    //    #================= USER GUIDE =======================================
    //    generate_code('bank_cash_book',new BankTransaction(),'2017-10-10',[
    //        'SEQ_COLUMN' => 'seq',
    //        'DATE_COLUMN' => 'transaction_date',
    //        'PREFIX' => [
    //            [
    //                'NAME' => 'BANK_CODE',
    //                'VALUE' => 'BCA01'
    //            ]
    //        ],
    //        'FILTER_BY' => ['settlement_module'=>'CB']
    //          'OUTPUT' => 'seq',
    //    ]);

    function generate_code($format, \Illuminate\Database\Eloquent\Model $model, $date, $setting = [] )
    {
        $running_code = null;
        $date = $date ? \Carbon\Carbon::parse($date) : \Carbon\Carbon::now();

        $p = [
            '$D'=>date('d',strtotime($date)),
            '$M'=>date('m',strtotime($date)),
            '$Y'=>date('Y',strtotime($date)),
            '$N'=>1
        ];

        $data = $model->select('*');

        if(isset($setting['PREFIX']))
        {
            foreach($setting['PREFIX'] as $key => $val)
            {
                $p['$'.$val['NAME']] = $val['VALUE'];
            }
        }

        if(isset($setting['FILTER_BY']))
        {
            $data->where($setting['FILTER_BY']);
        }

        $p['$Y'] = \Carbon\Carbon::create($p['$Y'])->format('Y');

        $reset_number = "MONTHLY";

        if(isset($setting['DATE_COLUMN']))
        {
            if($reset_number == "MONTHLY")
            {
                $data->whereMonth($setting['DATE_COLUMN'],$date->month);
            }
            else if($reset_number == "ANNUALLY")
            {
                $data->whereYear($setting['DATE_COLUMN'],$date->year);
            }
        }

        if(isset($setting['SEQ_COLUMN']))
        {
            $data->orderBy($setting['SEQ_COLUMN'],'desc');
        }
        else
        {
            $data->orderBy('seq','desc');
        }

        if($model->getKey() != null && $data->first())
        {
            $p['$N'] =  $data->first()->{$setting['SEQ_COLUMN']} + 1;

            if($model->getKey() == $data->first()->getKey())
            {
                $p['$N'] = $model->{$setting['SEQ_COLUMN']};
            }
        }
        else if($data->first())
        {
            $p['$N'] =  $data->first()->{$setting['SEQ_COLUMN']} + 1;
        }

        if(isset($setting['OUTPUT']))
        {
            if($setting['OUTPUT'] == 'number')
            {
                $running_code =  $p['$N'];
            }
            else if($setting['OUTPUT'] == 'code')
            {
                $p['$N'] = str_pad($p['$N'],3,"0",STR_PAD_LEFT);
                $running_code = strtr($format,$p);
            }
        }
        else
        {
            $p['$N'] = str_pad($p['$N'],3,"0",STR_PAD_LEFT);
            $running_code = strtr($format,$p);
        }

        return $running_code;
    }
}