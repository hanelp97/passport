<?php
/**
 * Created by PhpStorm.
 * User: Hanel Prillian
 * Date: 4/4/2016
 * Time: 4:56 PM
 */

namespace App\Helpers;

use App\Setting;
class settings
{
    /*
     * Return Value
     */
    public static function name($name) {
        $settings   = Setting::ByName($name)->first();
        return $settings['value'];
    }
}