<?php
/**
 * Created by PhpStorm.
 * User: Hanel Prillian
 * Date: 2/5/2016
 * Time: 8:55 PM
 */

namespace App\Helpers;

use App\File;
use Illuminate\Support\Facades\DB;

class folder {

    public static function Breadcrumbs($parent = 0) {
        $folder_model = \App\Folder::select('*');
        $html = "";
        foreach($folder_model->where('id',$parent)->get() as $folder) {
            $html.= self::Breadcrumbs($folder->parent_id);
            $html.='<li><a href="'.route('FileManagement::Index').'?folder='.$folder->id.'">'.$folder->folder.'</a></li>';
        }
        return $html;
    }

    public static function Delete($id = 0) {
        DB::beginTransaction();
        try {
            $folder = \App\Folder::find($id);
            if($folder->Files()->count() > 0 ) {
                foreach ($folder->Files()->get() as $file) {
                    $f = File::find($file->id);
                    $deleteFile = main_helper::delete_file($f->file);
                    if($deleteFile) {
                        $f->delete();
                    }
                }
            }
            $folder->delete();

            $folders = \App\Folder::select('*');
            foreach ($folders->where('id','!=',$id)->get() as $f) {
                if($f->parent_id == $id){
                    self::Delete($f->id);
                }
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
}

