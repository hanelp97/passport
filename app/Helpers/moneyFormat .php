<?php
/**
 * Created by PhpStorm.
 * User: Hanel Prillian
 * Date: 4/4/2016
 * Time: 4:56 PM
 */

namespace App\Helpers;

use App\Setting;
class moneyFormat
{
    public static function rupiah ($angka) {
        $rupiah = number_format($angka ,2, ',' , '.' );
        return $rupiah;
    }

    public static function terbilang ($angka) {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($angka < 12)
            return " " . $abil[$angka];
        elseif ($angka < 20)
            return self::terbilang($angka - 10) . "belas";
        elseif ($angka < 100)
            return self::terbilang($angka / 10) . " puluh" . self::terbilang($angka % 10);
        elseif ($angka < 200)
            return " seratus" . self::terbilang($angka - 100);
        elseif ($angka < 1000)
            return self::terbilang($angka / 100) . " ratus" . self::terbilang($angka % 100);
        elseif ($angka < 2000)
            return " seribu" . self::terbilang($angka - 1000);
        elseif ($angka < 1000000)
            return self::terbilang($angka / 1000) . " ribu" . self::terbilang($angka % 1000);
        elseif ($angka < 1000000000)
            return self::terbilang($angka / 1000000) . " juta" . self::terbilang($angka % 1000000);
    }
}